# Star Wars

README / 02 January 2024

-----

## Introduction

Star Wars is a simple RESTful API service managing Star Wars characters.

## Technical information

Name | Value
--- | ---
Main programming language | C#
Framework used | ASP.NET Core 8.0

## License

Licensed under MIT. Read full license [here](https://gitlab.com/Prologh/star-wars/blob/master/LICENSE).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)

### Acknowledgements

Project icon based on one made by [Freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/).
