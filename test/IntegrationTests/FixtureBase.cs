﻿using Flurl.Http;
using Flurl.Http.Newtonsoft;
using Microsoft.AspNetCore.Mvc.Testing;
using StarWars.IntegrationTests.ApiClients;
using System.Diagnostics;
using System.Net.Http;

namespace StarWars.IntegrationTests;

/// <summary>
/// Provides base class fixture for testing purposes.
/// </summary>
public abstract class FixtureBase : IClassFixture<StarWarsApiFactory>, IDisposable
{
    private readonly WebApplicationFactoryClientOptions _httpsClientOptions;
    private IServiceScope _scope;
    private bool _isDisposed;

    /// <summary>
    /// Initializes a new instance of the <see cref="FixtureBase"/>
    /// class with factory specified.
    /// </summary>
    /// <param name="factory">
    /// The Star Wars Web API factory.
    /// </param>
    protected FixtureBase(StarWarsApiFactory factory)
    {
        _httpsClientOptions = new WebApplicationFactoryClientOptions
        {
            BaseAddress = new Uri("https://localhost:443"),
        };

        Factory = factory;
    }

    /// <summary>
    /// Gets the Web API factory.
    /// </summary>
    protected StarWarsApiFactory Factory { get; }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Gets <see cref="HttpClient"/> configured for HTTP usage. Its
    /// base address is set to http://localhost.
    /// </summary>
    protected HttpClient CreateHttpClient() => Factory.CreateClient();

    /// <summary>
    /// Gets <see cref="HttpClient"/> configured for HTTPS usage. Its
    /// base address is set to https://localhost:443.
    /// </summary>
    protected HttpClient CreateHttpsClient() => Factory.CreateClient(_httpsClientOptions);

    /// <summary>
    /// Gets <see cref="FlurlClient"/> configured for HTTP usage. Its
    /// base address is set to http://localhost.
    /// </summary>
    protected FlurlClient CreateFlurlHttpClient()
    {
        var client = new FlurlClient(CreateHttpClient());

        ApplyDefaultFlurlSettings(client);

        return client;
    }

    /// <summary>
    /// Gets <see cref="FlurlClient"/> configured for HTTPS usage. Its
    /// base address is set to https://localhost:443.
    /// </summary>
    protected FlurlClient CreateFlurlHttpsClient()
    {
        var client = new FlurlClient(CreateHttpsClient());

        ApplyDefaultFlurlSettings(client);

        return client;
    }

    protected CharactersApiClient CreateCharactersApiClient()
    {
        var flurlClient = CreateFlurlHttpsClient();

        return new CharactersApiClient(flurlClient);
    }

    protected EpisodesApiClient CreateEpisodesApiClient()
    {
        var flurlClient = CreateFlurlHttpsClient();

        return new EpisodesApiClient(flurlClient);
    }

    protected PlanetsApiClient CreatePlanetsApiClient()
    {
        var flurlClient = CreateFlurlHttpsClient();

        return new PlanetsApiClient(flurlClient);
    }

    protected WebhooksApiClient CreateWebhooksApiClient()
    {
        var flurlClient = CreateFlurlHttpsClient();

        return new WebhooksApiClient(flurlClient);
    }

    protected T CreateScopedService<T>()
    {
        var scope = CreateScope();

        return scope.ServiceProvider.GetRequiredService<T>();
    }

    private IServiceScope CreateScope()
    {
        _scope ??= Factory.Services.CreateScope();

        return _scope;
    }

    private void ApplyDefaultFlurlSettings(FlurlClient client)
    {
        var timeout = Debugger.IsAttached ? TimeSpan.FromMinutes(10) : TimeSpan.FromSeconds(5);

        client.AllowAnyHttpStatus();
        client.WithTimeout(timeout);
        client.Settings.Redirects.Enabled = false;
        client.Settings.JsonSerializer = new NewtonsoftJsonSerializer();
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_isDisposed)
        {
            if (disposing)
            {
                _scope?.Dispose();
            }

            _isDisposed = true;
        }
    }
}
