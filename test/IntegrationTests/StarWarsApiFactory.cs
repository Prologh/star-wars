﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using StarWars.Api;
using StarWars.Persistance;
using StarWars.Persistance.Extensions;
using StarWars.Persistance.Seeding;

namespace StarWars.IntegrationTests;

/// <summary>
/// Factory for bootstrapping Star Wars Web API in memory for tests.
/// </summary>
public class StarWarsApiFactory : WebApplicationFactory<Startup>
{
    /// <summary>
    /// Gives a fixture an opportunity to configure the application
    /// before it gets built.
    /// </summary>
    /// <param name="builder">
    /// The <see cref="IWebHostBuilder"/> for current application.
    /// </param>
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureAppConfiguration((context, config) =>
        {
            config.Sources.Add(new JsonConfigurationSource
            {
                Path = "appsettings.Tests.json",
                Optional = false,
                ReloadOnChange = true,
                FileProvider = new PhysicalFileProvider(Environment.CurrentDirectory)
            });
        });

        builder.ConfigureTestServices(services =>
        {
            // Clean already registered DbContext options;
            services.RemoveDbContextOptions<StarWarsDbContext>();

            // Create seperate service provider.
            var internalServiceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Add a database context using an in-memory database for testing.
            services.AddDbContext<StarWarsDbContext>(options =>
            {
                options.UseInMemoryDatabase("InMemory");
                options.UseInternalServiceProvider(internalServiceProvider);
            });

            var serviceProvider = services.BuildServiceProvider();

            // Create a scope to obtain a reference to the database contexts
            using var scope = serviceProvider.CreateScope();
            var scopedServices = scope.ServiceProvider;
            var databaseInitializer = scopedServices.GetRequiredService<DatabaseInitializer>();

            try
            {
                databaseInitializer.Initialize(ensureCreated: true);
            }
            catch (Exception ex)
            {
                var logger = scopedServices.GetRequiredService<ILogger<StarWarsApiFactory>>();
                logger.LogError(ex, "An error occurred while seeding the database.");
            }
        });
    }
}
