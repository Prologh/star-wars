﻿global using Ardalis.GuardClauses;
global using FluentAssertions;
global using Microsoft.Extensions.DependencyInjection;
global using StarWars.IntegrationTests.Assertions;
global using System;
global using System.Linq;
global using System.Threading.Tasks;
global using Xunit;
