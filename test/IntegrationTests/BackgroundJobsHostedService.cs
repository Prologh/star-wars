﻿using Hangfire;
using Microsoft.Extensions.Hosting;
using System.Threading;

namespace StarWars.IntegrationTests;

public class BackgroundJobsHostedService : IHostedService
{
    public Task StartAsync(CancellationToken cancellationToken) => Task.CompletedTask;

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        // Wait 5 seconds till all Hangfire jobs complete. After that trigger cancellation via Dispose().
        using var cancellationSource = new CancellationTokenSource(TimeSpan.FromSeconds(5));
        var monitoringApi = JobStorage.Current.GetMonitoringApi();
        var jobsLeft = true;

        while (!cancellationSource.IsCancellationRequested && jobsLeft)
        {
            try
            {
                var statistics = monitoringApi.GetStatistics();
                jobsLeft = statistics.Processing > 0 || statistics.Enqueued > 0;
            }
            catch (ObjectDisposedException)
            {
                break;
            }

            await Task.Delay(30, default);
        }
    }
}
