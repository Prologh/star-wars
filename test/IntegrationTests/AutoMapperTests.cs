﻿using AutoMapper;

namespace StarWars.IntegrationTests;

public class AutoMapperTests : FixtureBase
{
    public AutoMapperTests(StarWarsApiFactory factory) : base(factory)
    {
    }

    [Fact]
    public void Should_Not_ThrowException()
    {
        // Arrange
        var mapper = CreateScopedService<IMapper>();

        // Act
        Action act = mapper.ConfigurationProvider.AssertConfigurationIsValid;

        // Assert
        act.Should().NotThrow<AutoMapperConfigurationException>();
    }
}
