﻿using Flurl.Http;
using Newtonsoft.Json;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Application.Common;
using StarWars.IntegrationTests.ApiClients.Models;
using System.Net.Http;
using System.Net.Mime;
using System.Text;

namespace StarWars.IntegrationTests.ApiClients.Abstractions;

public abstract class ApiClient
{
    private readonly IFlurlClient _flurlClient;

    protected ApiClient(IFlurlClient flurlClient)
    {
        _flurlClient = Guard.Against.Null(flurlClient);
    }

    protected async Task<ApiResponse<TResponse>> SendRequestFor<TResponse>(
        IFlurlRequest request,
        HttpMethod httpMethod)
    {
        Guard.Against.Null(request);
        Guard.Against.Null(httpMethod);

        request.Client = _flurlClient;

        // TODO: Handle failure and timeouts.
        using var response = await request.SendAsync(httpMethod, content: null);
        var responseData = await response.GetJsonAsync<TResponse>();

        return new ApiResponse<TResponse>(response.StatusCode, response.Headers, responseData);
    }

    protected async Task<ApiResponse<TResponse>> SendRequestFor<TResponse, TPayload>(
        IFlurlRequest request,
        HttpMethod httpMethod,
        TPayload payload)
    {
        Guard.Against.Null(request);
        Guard.Against.Null(httpMethod);

        request.Client = _flurlClient;

        var json = JsonConvert.SerializeObject(payload);
        var httpContent = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);

        // TODO: Handle failure and timeouts.
        using var response = await request.SendAsync(httpMethod, httpContent);
        var responseData = await response.GetJsonAsync<TResponse>();
        return new ApiResponse<TResponse>(response.StatusCode, response.Headers, responseData);
    }

    protected Task<ApiResponse<Envelope<PagedOutputModel<TResponse>>>> SendRequestForPagedEnvelope<TResponse>(
        IFlurlRequest request,
        HttpMethod httpMethod)
    {
        return SendRequestFor<Envelope<PagedOutputModel<TResponse>>>(request, httpMethod);
    }

    protected Task<ApiResponse<Envelope<PagedOutputModel<TResponse>>>> SendRequestForPagedEnvelope<TResponse, TPayload>(
        IFlurlRequest request,
        HttpMethod httpMethod,
        TPayload payload)
    {
        return SendRequestFor<Envelope<PagedOutputModel<TResponse>>, TPayload>(request, httpMethod, payload);
    }

    protected Task<ApiResponse<Envelope<TResponse>>> SendRequestForEnvelope<TResponse>(
        IFlurlRequest request,
        HttpMethod httpMethod)
    {
        return SendRequestFor<Envelope<TResponse>>(request, httpMethod);
    }

    protected Task<ApiResponse<Envelope<TResponse>>> SendRequestForEnvelope<TResponse, TPayload>(
        IFlurlRequest request,
        HttpMethod httpMethod,
        TPayload payload)
    {
        return SendRequestFor<Envelope<TResponse>, TPayload>(request, httpMethod, payload);
    }

    protected Task<ApiResponse<EmptyEnvelope>> SendRequestForEmptyEnvelope(
        IFlurlRequest request,
        HttpMethod httpMethod)
    {
        return SendRequestFor<EmptyEnvelope>(request, httpMethod);
    }

    protected Task<ApiResponse<EmptyEnvelope>> SendRequestForEmptyEnvelope<TPayload>(
        IFlurlRequest request,
        HttpMethod httpMethod,
        TPayload payload)
    {
        return SendRequestFor<EmptyEnvelope, TPayload>(request, httpMethod, payload);
    }
}
