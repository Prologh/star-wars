﻿using Flurl.Util;

namespace StarWars.IntegrationTests.ApiClients.Models;

public class ApiResponse<T>
{
    public ApiResponse(
        int statusCode,
        IReadOnlyNameValueList<string> headers,
        T responseData)
    {
        StatusCode = statusCode;
        Headers = headers;
        ResponseData = responseData;
    }

    public T ResponseData { get; }

    public bool HasSucceeded => StatusCode >= 200 && StatusCode <= 299;

    public bool HasFailed => StatusCode >= 400 && StatusCode <= 599;

    public int StatusCode { get; }

    public IReadOnlyNameValueList<string> Headers { get; }
}
