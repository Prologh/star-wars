﻿using Flurl.Http;
using StarWars.Api.InputModels.Characters;
using StarWars.Api.InputModels.Pagination;
using StarWars.Api.OutputModels.Characters;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Application.Common;
using StarWars.IntegrationTests.ApiClients.Abstractions;
using StarWars.IntegrationTests.ApiClients.Models;
using StarWars.IntegrationTests.UrlBuilders;
using System.Net.Http;

namespace StarWars.IntegrationTests.ApiClients;

public class CharactersApiClient : ApiClient
{
    private readonly CharactersControllerUrlBuilder _urlBuilder;

    public CharactersApiClient(IFlurlClient flurlClient) : base(flurlClient)
    {
        _urlBuilder = new();
    }

    public Task<ApiResponse<Envelope<PagedOutputModel<CharacterOutputModel>>>> GetPageAsync(int pageNumber, int pageSize)
    {
        var pagedInputModel = new PagedInputModel
        {
            PageNumber = pageNumber,
            PageSize = pageSize,
        };
        var url = _urlBuilder.GetPage(pagedInputModel);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForPagedEnvelope<CharacterOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<Envelope<CharacterOutputModel>>> GetByNameAsync(string name)
    {
        var url = _urlBuilder.GetByName(name);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForEnvelope<CharacterOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<EmptyEnvelope>> CreateAsync(CreateCharacterInputModel payload)
    {
        var url = _urlBuilder.Create();
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Post;

        return SendRequestForEmptyEnvelope(request, httpMethod, payload);
    }

    public Task<ApiResponse<EmptyEnvelope>> UpdateAsync(string name, UpdateCharacterInputModel payload)
    {
        var url = _urlBuilder.Update(name);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Put;

        return SendRequestForEmptyEnvelope(request, httpMethod, payload);
    }

    public Task<ApiResponse<EmptyEnvelope>> DeleteAsync(string name)
    {
        var url = _urlBuilder.Delete(name);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Delete;

        return SendRequestForEmptyEnvelope(request, httpMethod);
    }
}
