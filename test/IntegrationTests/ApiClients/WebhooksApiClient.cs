﻿using Flurl.Http;
using StarWars.Api.InputModels.Pagination;
using StarWars.Api.InputModels.Webhooks.InputModels;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Api.OutputModels.Webhooks;
using StarWars.Application.Common;
using StarWars.IntegrationTests.ApiClients.Abstractions;
using StarWars.IntegrationTests.ApiClients.Models;
using StarWars.IntegrationTests.UrlBuilders;
using System.Net.Http;

namespace StarWars.IntegrationTests.ApiClients;

public class WebhooksApiClient : ApiClient
{
    private readonly WebhooksControllerUrlBuilder _urlBuilder;

    public WebhooksApiClient(IFlurlClient flurlClient) : base(flurlClient)
    {
        _urlBuilder = new();
    }

    public Task<ApiResponse<Envelope<PagedOutputModel<WebhookOutputModel>>>> GetWebhooksPage(int pageNumber, int pageSize)
    {
        var pagedInputModel = new PagedInputModel
        {
            PageNumber = pageNumber,
            PageSize = pageSize,
        };
        var url = _urlBuilder.GetWebhooks(pagedInputModel);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForPagedEnvelope<WebhookOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<Envelope<PagedOutputModel<WebhookOutputModel>>>> GetWebhookCallsPage(int pageNumber, int pageSize, Guid externalId)
    {
        var pagedInputModel = new PagedInputModel
        {
            PageNumber = pageNumber,
            PageSize = pageSize,
        };
        var url = _urlBuilder.GetWebhookCallsPage(pagedInputModel, externalId);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForPagedEnvelope<WebhookOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<Envelope<WebhookOutputModel>>> GetById(Guid externalId)
    {
        var url = _urlBuilder.GetById(externalId);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForEnvelope<WebhookOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<Envelope<WebhookCallOutputModel>>> GetCallById(Guid webhookExternalId, Guid callExternalId)
    {
        var url = _urlBuilder.GetCallById(webhookExternalId, callExternalId);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForEnvelope<WebhookCallOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<EmptyEnvelope>> CreateAsync(CreateWebhookInputModel payload)
    {
        var url = _urlBuilder.Create();
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Post;

        return SendRequestForEmptyEnvelope(request, httpMethod, payload);
    }

    public Task<ApiResponse<EmptyEnvelope>> DeleteAsync(Guid externalId)
    {
        var url = _urlBuilder.Delete(externalId);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Delete;

        return SendRequestForEmptyEnvelope(request, httpMethod);
    }
}
