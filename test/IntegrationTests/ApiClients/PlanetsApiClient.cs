﻿using Flurl.Http;
using StarWars.Api.InputModels.Pagination;
using StarWars.Api.InputModels.Planets;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Api.OutputModels.Planets;
using StarWars.Application.Common;
using StarWars.IntegrationTests.ApiClients.Abstractions;
using StarWars.IntegrationTests.ApiClients.Models;
using StarWars.IntegrationTests.UrlBuilders;
using System.Net.Http;

namespace StarWars.IntegrationTests.ApiClients;

public class PlanetsApiClient : ApiClient
{
    private readonly PlanetsControllerUrlBuilder _urlBuilder;

    public PlanetsApiClient(IFlurlClient flurlClient) : base(flurlClient)
    {
        _urlBuilder = new();
    }

    public Task<ApiResponse<Envelope<PagedOutputModel<PlanetOutputModel>>>> GetPlanetsPage(int pageNumber, int pageSize)
    {
        var pagedInputModel = new PagedInputModel
        {
            PageNumber = pageNumber,
            PageSize = pageSize,
        };
        var url = _urlBuilder.GetPlanets(pagedInputModel);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForPagedEnvelope<PlanetOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<Envelope<PagedOutputModel<PlanetCharacterOutputModel>>>> GetPlanetCharactersPage(int pageNumber, int pageSize, string name)
    {
        var pagedInputModel = new PagedInputModel
        {
            PageNumber = pageNumber,
            PageSize = pageSize,
        };
        var url = _urlBuilder.GetPlanetCharactersPage(pagedInputModel, name);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForPagedEnvelope<PlanetCharacterOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<Envelope<PlanetOutputModel>>> GetByName(string name)
    {
        var url = _urlBuilder.GetByName(name);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForEnvelope<PlanetOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<EmptyEnvelope>> CreateAsync(CreatePlanetInputModel payload)
    {
        var url = _urlBuilder.Create();
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Post;

        return SendRequestForEmptyEnvelope(request, httpMethod, payload);
    }

    public Task<ApiResponse<EmptyEnvelope>> DeleteAsync(string name)
    {
        var url = _urlBuilder.Delete(name);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Delete;

        return SendRequestForEmptyEnvelope(request, httpMethod);
    }
}
