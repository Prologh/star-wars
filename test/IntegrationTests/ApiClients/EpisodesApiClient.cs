﻿using Flurl.Http;
using StarWars.Api.InputModels.Episodes;
using StarWars.Api.InputModels.Pagination;
using StarWars.Api.OutputModels.Episodes;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Application.Common;
using StarWars.IntegrationTests.ApiClients.Abstractions;
using StarWars.IntegrationTests.ApiClients.Models;
using StarWars.IntegrationTests.UrlBuilders;
using System.Net.Http;

namespace StarWars.IntegrationTests.ApiClients;

public class EpisodesApiClient : ApiClient
{
    private readonly EpisodesControllerUrlBuilder _urlBuilder;

    public EpisodesApiClient(IFlurlClient flurlClient) : base(flurlClient)
    {
        _urlBuilder = new();
    }

    public Task<ApiResponse<Envelope<PagedOutputModel<EpisodeOutputModel>>>> GetEpisodesPage(int pageNumber, int pageSize)
    {
        var pagedInputModel = new PagedInputModel
        {
            PageNumber = pageNumber,
            PageSize = pageSize,
        };
        var url = _urlBuilder.GetEpisodes(pagedInputModel);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForPagedEnvelope<EpisodeOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<Envelope<PagedOutputModel<EpisodeOutputModel>>>> GetEpisodeCharactersPage(int pageNumber, int pageSize, int partNumber)
    {
        var pagedInputModel = new PagedInputModel
        {
            PageNumber = pageNumber,
            PageSize = pageSize,
        };
        var url = _urlBuilder.GetEpisodeCharactersPage(pagedInputModel, partNumber);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForPagedEnvelope<EpisodeOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<Envelope<EpisodeOutputModel>>> GetByPartNumber(int partNumber)
    {
        var url = _urlBuilder.GetByPartNumber(partNumber);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Get;

        return SendRequestForEnvelope<EpisodeOutputModel>(request, httpMethod);
    }

    public Task<ApiResponse<EmptyEnvelope>> CreateAsync(CreateEpisodeInputModel payload)
    {
        var url = _urlBuilder.Create();
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Post;

        return SendRequestForEmptyEnvelope(request, httpMethod, payload);
    }

    public Task<ApiResponse<EmptyEnvelope>> UpdateAsync(int partNumber, UpdateEpisodeInputModel payload)
    {
        var url = _urlBuilder.Update(partNumber);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Put;

        return SendRequestForEmptyEnvelope(request, httpMethod, payload);
    }

    public Task<ApiResponse<EmptyEnvelope>> DeleteAsync(int partNumber)
    {
        var url = _urlBuilder.Delete(partNumber);
        var request = new FlurlRequest(url);
        var httpMethod = HttpMethod.Delete;

        return SendRequestForEmptyEnvelope(request, httpMethod);
    }
}
