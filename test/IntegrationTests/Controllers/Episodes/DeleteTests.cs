﻿using Microsoft.AspNetCore.Http;
using StarWars.Application.Episodes.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Episodes;

namespace StarWars.IntegrationTests.Controllers.Episodes;

public class DeleteTests : FixtureBase
{
    public DeleteTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Delete_Episode_ReturnsNotFound_IfNoEpisodeFound()
    {
        // Arrange
        const int nonExistingEpisodePartNumber = 2423525;
        var client = CreateEpisodesApiClient();

        // Act
        var apiResponse = await client.DeleteAsync(nonExistingEpisodePartNumber);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }

    [Fact]
    public async Task Delete_Episode_ReturnsNoContent_IfValid()
    {
        // Arrange
        var episodeRepository = CreateScopedService<IRepository<EpisodeEntity>>();
        var specification = new EpisodeSpecification();
        var episode = await episodeRepository.FirstAsync(specification, default);
        var client = CreateEpisodesApiClient();

        // Act
        var apiResponse = await client.DeleteAsync(episode.PartNumber);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status204NoContent);
    }
}
