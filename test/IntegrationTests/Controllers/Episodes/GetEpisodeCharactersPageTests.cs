﻿namespace StarWars.IntegrationTests.Controllers.Episodes;

public class GetEpisodeCharactersPageTests : FixtureBase
{
    public GetEpisodeCharactersPageTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_EpisodeCharacters_ReturnsFirstPageOfCharacters()
    {
        // Arrange
        var client = CreateEpisodesApiClient();

        // Act
        var apiResponse = await client.GetEpisodeCharactersPage(pageSize: 50, pageNumber: 1, partNumber: 4);

        // Assert
        apiResponse.Should().BeSuccessful();
        var response = apiResponse.ResponseData;
        response.Errors.Should().BeNullOrEmpty();
        response.Data.Items.Should().NotBeNullOrEmpty();
    }
}
