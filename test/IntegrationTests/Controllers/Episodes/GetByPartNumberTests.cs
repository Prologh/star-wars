﻿using Microsoft.AspNetCore.Http;

namespace StarWars.IntegrationTests.Controllers.Episodes;

public class GetByIdTests : FixtureBase
{
    public GetByIdTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_EpisodeByPartNumber_ReturnsEpisode_IfFound()
    {
        // Arrange
        const int partNumber = 1;
        var client = CreateEpisodesApiClient();

        // Act
        var apiResponse = await client.GetByPartNumber(partNumber);

        // Assert
        apiResponse.Should().BeSuccessful();
        apiResponse.ResponseData.Should().NotBeNull();
        apiResponse.ResponseData.Errors.Should().BeNullOrEmpty();
        apiResponse.ResponseData.Data.PartNumber.Should().Be(partNumber);
    }

    [Fact]
    public async Task Get_EpisodeByPartNumber_ReturnsNotFound_IfNoEpisodeFound()
    {
        // Arrange
        const int partNumber = 43245235;
        var client = CreateEpisodesApiClient();

        // Act
        var apiResponse = await client.GetByPartNumber(partNumber);

        // Assert
        apiResponse.Should().BeFailure();
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }
}
