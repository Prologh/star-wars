﻿namespace StarWars.IntegrationTests.Controllers.Episodes;

public class GetEpisodesPageTests : FixtureBase
{
    public GetEpisodesPageTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_Episodes_ReturnsFirstPageOfEpisodes()
    {
        // Arrange
        var client = CreateEpisodesApiClient();

        // Act
        var apiResponse = await client.GetEpisodesPage(pageSize: 50, pageNumber: 1);

        // Assert
        apiResponse.Should().BeSuccessful();
        var response = apiResponse.ResponseData;
        response.Errors.Should().BeNullOrEmpty();
        response.Data.Items.Should().NotBeNullOrEmpty();
    }
}
