﻿using Microsoft.AspNetCore.Http;
using StarWars.Api.InputModels.Episodes;
using StarWars.Application.Episodes.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain;
using StarWars.Domain.Episodes;

namespace StarWars.IntegrationTests.Controllers.Episodes;

public class UpdateTests : FixtureBase
{
    public UpdateTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Put_Episode_ReturnsBadRequest_IfInvalid()
    {
        // Arrange
        const int existingEpisodePartNumber = 1;
        var episodeNameLength = Constants.Validation.MaxEpisodeNameLength + 1;
        var tooLongEpisodeName = new string(Enumerable.Repeat('#', episodeNameLength).ToArray());
        var body = new UpdateEpisodeInputModel
        {
            Name = tooLongEpisodeName,
        };
        var client = CreateEpisodesApiClient();

        // Act
        var apiResponse = await client.UpdateAsync(existingEpisodePartNumber, body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Put_Episode_ReturnsNotFound_IfNoEpisodeFound()
    {
        // Arrange
        const int nonExistingEpisodePartNumber = 7;
        var body = new UpdateEpisodeInputModel
        {
            Name = "foo",
        };
        var client = CreateEpisodesApiClient();

        // Act
        var apiResponse = await client.UpdateAsync(nonExistingEpisodePartNumber, body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }

    [Fact]
    public async Task Put_Episode_ReturnsNoContent_IfValid()
    {
        // Arrange
        var client = CreateEpisodesApiClient();
        var episodeRepository = CreateScopedService<IRepository<EpisodeEntity>>();
        var specification = new EpisodeSpecification();
        var episode = await episodeRepository.FirstAsync(specification, default);
        var body = new UpdateEpisodeInputModel
        {
            Name = "foo",
        };

        // Act
        var apiResponse = await client.UpdateAsync(episode.PartNumber, body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status204NoContent);
    }
}
