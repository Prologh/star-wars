﻿using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using StarWars.Api.InputModels.Episodes;

namespace StarWars.IntegrationTests.Controllers.Episodes;

public class CreateNewTests : FixtureBase
{
    public CreateNewTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Post_Episode_ReturnsBadRequest_ForInvalidModel()
    {
        // Arrange
        var client = CreateEpisodesApiClient();
        var body = new CreateEpisodeInputModel();

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Post_Episode_ReturnsBadRequest_ForEpisodeWithAlreadyTakenPartNumber()
    {
        // Arrange
        var client = CreateEpisodesApiClient();
        var body = new CreateEpisodeInputModel
        {
            Name = "Foo",
            PartNumber = 1,
        };

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Post_Episode_CreatesNewEpisode_ForValidModel()
    {
        // Arrange
        var client = CreateEpisodesApiClient();
        var body = new CreateEpisodeInputModel
        {
            Name = "Obi Wan",
            PartNumber = 7,
        };

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status201Created);
        apiResponse.ResponseData.Should().BeNull();
        apiResponse.Headers.Should().Contain(x => x.Name == HeaderNames.Location);

        // Arrange
        var createdEpisodeUri = apiResponse.Headers.FirstOrDefault(HeaderNames.Location);

        // Act
        // Check if new episode is available at returned uri
        var createdEpisodeResponse = await client.GetByPartNumber(int.Parse(createdEpisodeUri.Split('/').Last()));

        // Assert
        createdEpisodeResponse.Should().BeSuccessful();
        createdEpisodeResponse.ResponseData.Data.PartNumber.Should().Be(body.PartNumber);
    }
}
