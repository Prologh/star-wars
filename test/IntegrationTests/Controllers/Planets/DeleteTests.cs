﻿using Microsoft.AspNetCore.Http;
using StarWars.Application.Planets.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Planets;

namespace StarWars.IntegrationTests.Controllers.Planets;

public class DeleteTests : FixtureBase
{
    public DeleteTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Delete_Planet_ReturnsNotFound_IfNoPlanetFound()
    {
        // Arrange
        const string nonExistingPlanetName = "Foo Bar 2";
        var client = CreatePlanetsApiClient();

        // Act
        var apiResponse = await client.DeleteAsync(nonExistingPlanetName);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }

    [Fact]
    public async Task Delete_Planet_ReturnsNoContent_IfValid()
    {
        // Arrange
        var planetRepository = CreateScopedService<IRepository<PlanetEntity>>();
        var specification = new PlanetSpecification();
        var planet = await planetRepository.FirstAsync(specification, default);
        var client = CreatePlanetsApiClient();

        // Act
        var apiResponse = await client.DeleteAsync(planet.Name);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status204NoContent);
    }
}
