﻿using Microsoft.AspNetCore.Http;

namespace StarWars.IntegrationTests.Controllers.Planets;

public class GetByIdTests : FixtureBase
{
    public GetByIdTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_PlanetByPartNumber_ReturnsPlanet_IfFound()
    {
        // Arrange
        const string name = "Tatooine";
        var client = CreatePlanetsApiClient();

        // Act
        var apiResponse = await client.GetByName(name);

        // Assert
        apiResponse.Should().BeSuccessful();
        apiResponse.ResponseData.Should().NotBeNull();
        apiResponse.ResponseData.Errors.Should().BeNullOrEmpty();
        apiResponse.ResponseData.Data.Name.Should().Be(name);
    }

    [Fact]
    public async Task Get_PlanetByPartNumber_ReturnsNotFound_IfNoPlanetFound()
    {
        // Arrange
        const string name = "Foo Bar 2";
        var client = CreatePlanetsApiClient();

        // Act
        var apiResponse = await client.GetByName(name);

        // Assert
        apiResponse.Should().BeFailure();
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }
}
