﻿using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using StarWars.Api.InputModels.Planets;

namespace StarWars.IntegrationTests.Controllers.Planets;

public class CreateNewTests : FixtureBase
{
    public CreateNewTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Post_Planet_ReturnsBadRequest_ForInvalidModel()
    {
        // Arrange
        var client = CreatePlanetsApiClient();
        var body = new CreatePlanetInputModel();

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Post_Planet_ReturnsBadRequest_ForPlanetWithAlreadyTakenName()
    {
        // Arrange
        var client = CreatePlanetsApiClient();
        var body = new CreatePlanetInputModel
        {
            Name = "Tatooine",
        };

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Post_Planet_CreatesNewPlanet_ForValidModel()
    {
        // Arrange
        var client = CreatePlanetsApiClient();
        var body = new CreatePlanetInputModel
        {
            Name = "Foo Bar 2",
        };

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status201Created);
        apiResponse.ResponseData.Should().BeNull();
        apiResponse.Headers.Should().Contain(x => x.Name == HeaderNames.Location);

        // Arrange
        var createdPlanetUri = apiResponse.Headers.FirstOrDefault(HeaderNames.Location);

        // Act
        // Check if new planet is available at returned uri
        var createdPlanetResponse = await client.GetByName(createdPlanetUri.Split('/').Last());

        // Assert
        createdPlanetResponse.Should().BeSuccessful();
        createdPlanetResponse.ResponseData.Data.Name.Should().Be(body.Name);
    }
}
