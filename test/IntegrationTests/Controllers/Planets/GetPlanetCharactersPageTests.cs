﻿namespace StarWars.IntegrationTests.Controllers.Planets;

public class GetPlanetCharactersPageTests : FixtureBase
{
    public GetPlanetCharactersPageTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_PlanetCharacters_ReturnsFirstPageOfCharacters()
    {
        // Arrange
        var client = CreatePlanetsApiClient();

        // Act
        var apiResponse = await client.GetPlanetCharactersPage(pageNumber: 1, pageSize: 50, name: "Tatooine");

        // Assert
        apiResponse.Should().BeSuccessful();
        var response = apiResponse.ResponseData;
        response.Errors.Should().BeNullOrEmpty();
        response.Data.Items.Should().NotBeNullOrEmpty();
    }
}
