﻿namespace StarWars.IntegrationTests.Controllers.Planets;

public class GetPlanetsPageTests : FixtureBase
{
    public GetPlanetsPageTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_Planets_ReturnsFirstPageOfPlanets()
    {
        // Arrange
        var client = CreatePlanetsApiClient();

        // Act
        var apiResponse = await client.GetPlanetsPage(pageSize: 50, pageNumber: 1);

        // Assert
        apiResponse.Should().BeSuccessful();
        var response = apiResponse.ResponseData;
        response.Errors.Should().BeNullOrEmpty();
        response.Data.Items.Should().NotBeNullOrEmpty();
    }
}
