﻿using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Specifications;
using StarWars.Domain.Webhooks;

namespace StarWars.IntegrationTests.Controllers.Webhooks;

public class GetWebhookCallsPageTests : FixtureBase
{
    public GetWebhookCallsPageTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_WebhookCalls_ReturnsFirstPageOfCharacters()
    {
        // Arrange
        var webhookRepository = CreateScopedService<IRepository<WebhookEntity>>();
        var specification = new WebhookSpecification();
        var webhook = await webhookRepository.FirstAsync(specification, default);
        var client = CreateWebhooksApiClient();

        // Act
        var apiResponse = await client.GetWebhookCallsPage(pageSize: 50, pageNumber: 1, externalId: webhook.ExternalId);

        // Assert
        apiResponse.Should().BeSuccessful();
        var response = apiResponse.ResponseData;
        response.Errors.Should().BeNullOrEmpty();
        response.Data.Items.Should().NotBeNullOrEmpty();
    }
}
