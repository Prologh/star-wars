﻿using Microsoft.AspNetCore.Http;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Specifications;
using StarWars.Domain.Webhooks;

namespace StarWars.IntegrationTests.Controllers.Webhooks;

public class GetByIdTests : FixtureBase
{
    public GetByIdTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_WebhookById_ReturnsWebhook_IfFound()
    {
        // Arrange
        var webhookRepository = CreateScopedService<IRepository<WebhookEntity>>();
        var specification = new WebhookSpecification();
        var webhook = await webhookRepository.FirstAsync(specification, default);
        var client = CreateWebhooksApiClient();

        // Act
        var apiResponse = await client.GetById(webhook.ExternalId);

        // Assert
        apiResponse.Should().BeSuccessful();
        apiResponse.ResponseData.Should().NotBeNull();
        apiResponse.ResponseData.Errors.Should().BeNullOrEmpty();
        apiResponse.ResponseData.Data.ExternalId.Should().Be(webhook.ExternalId);
    }

    [Fact]
    public async Task Get_WebhookById_ReturnsNotFound_IfNoWebhookFound()
    {
        // Arrange
        var externalId = Guid.NewGuid();
        var client = CreateWebhooksApiClient();

        // Act
        var apiResponse = await client.GetById(externalId);

        // Assert
        apiResponse.Should().BeFailure();
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }
}
