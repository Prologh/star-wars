﻿using Microsoft.AspNetCore.Http;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Specifications;
using StarWars.Domain.Webhooks;

namespace StarWars.IntegrationTests.Controllers.Webhooks;

public class GetCallByIdTests : FixtureBase
{
    public GetCallByIdTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_WebhookCallById_ReturnsWebhookCall_WhenFound()
    {
        // Arrange
        var webhookCallRepository = CreateScopedService<IRepository<WebhookCallEntity>>();
        var specification = new WebhookCallSpecification().WithWebhook();
        var call = await webhookCallRepository.FirstAsync(specification, default);
        var client = CreateWebhooksApiClient();

        // Act
        var apiResponse = await client.GetCallById(call.Webhook.ExternalId, call.ExternalId);

        // Assert
        apiResponse.Should().BeSuccessful();
        apiResponse.ResponseData.Should().NotBeNull();
        apiResponse.ResponseData.Errors.Should().BeNullOrEmpty();
        apiResponse.ResponseData.Data.ExternalId.Should().Be(call.ExternalId);
    }

    [Fact]
    public async Task Get_WebhookCallById_ReturnsNotFound_WhenNoWebhookCallFound()
    {
        // Arrange
        var externalId = Guid.NewGuid();
        var callId = Guid.NewGuid();
        var client = CreateWebhooksApiClient();

        // Act
        var apiResponse = await client.GetCallById(externalId, callId);

        // Assert
        apiResponse.Should().BeFailure();
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }
}
