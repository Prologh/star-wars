﻿using Microsoft.AspNetCore.Http;
using StarWars.Application.Webhooks.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Webhooks;

namespace StarWars.IntegrationTests.Controllers.Webhooks;

public class DeleteTests : FixtureBase
{
    public DeleteTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Delete_Webhook_ReturnsNotFound_IfNoWebhookFound()
    {
        // Arrange
        var nonExistingWebhookId = Guid.NewGuid();
        var client = CreateWebhooksApiClient();

        // Act
        var apiResponse = await client.DeleteAsync(nonExistingWebhookId);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }

    [Fact]
    public async Task Delete_Webhook_ReturnsNoContent_IfValid()
    {
        // Arrange
        var episodeRepository = CreateScopedService<IRepository<WebhookEntity>>();
        var specification = new WebhookSpecification();
        var episode = await episodeRepository.FirstAsync(specification, default);
        var client = CreateWebhooksApiClient();

        // Act
        var apiResponse = await client.DeleteAsync(episode.ExternalId);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status204NoContent);
    }
}
