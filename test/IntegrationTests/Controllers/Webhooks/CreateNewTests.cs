﻿using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using StarWars.Api.InputModels.Webhooks.InputModels;
using StarWars.Domain.Webhooks;

namespace StarWars.IntegrationTests.Controllers.Webhooks;

public class CreateNewTests : FixtureBase
{
    public CreateNewTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Post_Webhook_ReturnsBadRequest_ForInvalidModel()
    {
        // Arrange
        var client = CreateWebhooksApiClient();
        var body = new CreateWebhookInputModel();

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Post_Webhook_ReturnsBadRequest_ForWebhookForTheSameTargetUrl()
    {
        // Arrange
        var client = CreateWebhooksApiClient();
        var body = new CreateWebhookInputModel
        {
            TargetUrl = "https://localhost/",
            Topic = WebhookTopic.CharacterCreated,
        };

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Post_Webhook_CreatesNewWebhook_ForValidModel()
    {
        // Arrange
        var client = CreateWebhooksApiClient();
        var body = new CreateWebhookInputModel
        {
            TargetUrl = "https://localhost/",
            Topic = WebhookTopic.EpisodeUpdated,
        };

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status201Created);
        apiResponse.ResponseData.Should().BeNull();
        apiResponse.Headers.Should().Contain(x => x.Name == HeaderNames.Location);

        // Arrange
        var createdWebhookUri = apiResponse.Headers.FirstOrDefault(HeaderNames.Location);

        // Act
        // Check if new webhook is available at returned uri
        var createdWebhookResponse = await client.GetById(new Guid(createdWebhookUri.Split('/').Last()));

        // Assert
        createdWebhookResponse.Should().BeSuccessful();
        createdWebhookResponse.ResponseData.Data.Topic.Should().Be(body.Topic);
    }
}
