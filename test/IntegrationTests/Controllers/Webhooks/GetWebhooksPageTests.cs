﻿namespace StarWars.IntegrationTests.Controllers.Webhooks;

public class GetWebhooksPageTests : FixtureBase
{
    public GetWebhooksPageTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_Webhooks_ReturnsFirstPageOfWebhooks()
    {
        // Arrange
        var client = CreateWebhooksApiClient();

        // Act
        var apiResponse = await client.GetWebhooksPage(pageSize: 50, pageNumber: 1);

        // Assert
        apiResponse.Should().BeSuccessful();
        var response = apiResponse.ResponseData;
        response.Errors.Should().BeNullOrEmpty();
        response.Data.Items.Should().NotBeNullOrEmpty();
    }
}
