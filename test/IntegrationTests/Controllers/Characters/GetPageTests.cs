﻿namespace StarWars.IntegrationTests.Controllers.Characters;

public class GetPageTests : FixtureBase
{
    public GetPageTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_Characters_ReturnsFirstPageOfCharacters()
    {
        // Arrange
        var client = CreateCharactersApiClient();

        // Act
        var apiResponse = await client.GetPageAsync(pageSize: 50, pageNumber: 1);

        // Assert
        apiResponse.Should().BeSuccessful();
        var response = apiResponse.ResponseData;
        response.Errors.Should().BeNullOrEmpty();
        response.Data.Items.Should().NotBeNullOrEmpty();
    }
}
