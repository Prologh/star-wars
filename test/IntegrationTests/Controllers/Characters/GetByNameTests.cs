﻿using Microsoft.AspNetCore.Http;

namespace StarWars.IntegrationTests.Controllers.Characters;

public class GetByNameTests : FixtureBase
{
    public GetByNameTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_CharacterByName_ReturnsCharacter_IfFound()
    {
        // Arrange
        const string characterName = "C-3PO";
        var client = CreateCharactersApiClient();

        // Act
        var apiResponse = await client.GetByNameAsync(characterName);

        // Assert
        apiResponse.Should().BeSuccessful();
        apiResponse.ResponseData.Should().NotBeNull();
        apiResponse.ResponseData.Errors.Should().BeNullOrEmpty();
        apiResponse.ResponseData.Data.Name.Should().Be(characterName);
    }

    [Fact]
    public async Task Get_CharacterByName_ReturnsNotFound_IfNoCharacterFound()
    {
        // Arrange
        const string characterName = "Harry Potter";
        var client = CreateCharactersApiClient();

        // Act
        var apiResponse = await client.GetByNameAsync(characterName);

        // Assert
        apiResponse.Should().BeFailure();
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }
}
