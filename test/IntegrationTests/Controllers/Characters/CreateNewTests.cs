﻿using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using StarWars.Api.InputModels.Characters;

namespace StarWars.IntegrationTests.Controllers.Characters;

public class CreateNewTests : FixtureBase
{
    public CreateNewTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Post_Character_ReturnsBadRequest_ForInvalidModel()
    {
        // Arrange
        var client = CreateCharactersApiClient();
        var body = new CreateCharacterInputModel();

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Post_Character_ReturnsBadRequest_ForCharacterWithAlreadyTakenName()
    {
        // Arrange
        var client = CreateCharactersApiClient();
        var body = new CreateCharacterInputModel
        {
            Name = "Luke Skywalker",
        };

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Post_Character_CreatesNewCharacter_ForValidModel()
    {
        // Arrange
        var client = CreateCharactersApiClient();
        var body = new CreateCharacterInputModel { Name = "Obi Wan" };

        // Act
        var apiResponse = await client.CreateAsync(body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status201Created);
        apiResponse.ResponseData.Should().BeNull();
        apiResponse.Headers.Should().Contain(x => x.Name == HeaderNames.Location);

        // Arrange
        var createdCharacterUri = apiResponse.Headers.FirstOrDefault(HeaderNames.Location);

        // Act
        // Check if new character is available at returned uri
        var createdCharacterResponse = await client.GetByNameAsync(createdCharacterUri.Split('/').Last());

        // Assert
        createdCharacterResponse.Should().BeSuccessful();
        createdCharacterResponse.ResponseData.Data.Name.Should().Be(body.Name);
    }
}
