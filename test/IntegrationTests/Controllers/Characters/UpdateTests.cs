﻿using Microsoft.AspNetCore.Http;
using StarWars.Api.InputModels.Characters;
using StarWars.Application.Characters.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain;
using StarWars.Domain.Characters;

namespace StarWars.IntegrationTests.Controllers.Characters;

public class UpdateTests : FixtureBase
{
    public UpdateTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Put_Character_ReturnsBadRequest_IfInvalid()
    {
        // Arrange
        const string existingCharacterName = "Chewbacca";
        var planetNameLength = Constants.Validation.MaxPlanetNameLength + 1;
        var planetName = new string(Enumerable.Repeat('#', planetNameLength).ToArray());
        var body = new UpdateCharacterInputModel
        {
            Planet = planetName,
        };
        var client = CreateCharactersApiClient();

        // Act
        var apiResponse = await client.UpdateAsync(existingCharacterName, body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
    }

    [Fact]
    public async Task Put_Character_ReturnsNotFound_IfNoCharacterFound()
    {
        // Arrange
        const string nonExistingCharacterName = "Chewbaca 2";
        var body = new UpdateCharacterInputModel();
        var client = CreateCharactersApiClient();

        // Act
        var apiResponse = await client.UpdateAsync(nonExistingCharacterName, body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }

    [Fact]
    public async Task Put_Character_ReturnsNoContent_IfValid()
    {
        // Arrange
        var client = CreateCharactersApiClient();
        var characterRepository = CreateScopedService<IRepository<CharacterEntity>>();
        var specification = new CharacterSpecification();
        var character = await characterRepository.FirstAsync(specification, default);
        var body = new UpdateCharacterInputModel();

        // Act
        var apiResponse = await client.UpdateAsync(character.Name, body);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status204NoContent);
    }
}
