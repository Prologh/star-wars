﻿using Microsoft.AspNetCore.Http;
using StarWars.Application.Characters.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Characters;

namespace StarWars.IntegrationTests.Controllers.Characters;

public class DeleteTests : FixtureBase
{
    public DeleteTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Delete_Character_ReturnsNotFound_IfNoCharacterFound()
    {
        // Arrange
        const string nonExistingCharacterName = "Yoda";
        var client = CreateCharactersApiClient();

        // Act
        var apiResponse = await client.DeleteAsync(nonExistingCharacterName);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status404NotFound);
    }

    [Fact]
    public async Task Delete_Character_ReturnsNoContent_IfValid()
    {
        // Arrange
        var characterRepository = CreateScopedService<IRepository<CharacterEntity>>();
        var specification = new CharacterSpecification();
        var character = await characterRepository.FirstAsync(specification, default);
        var client = CreateCharactersApiClient();

        // Act
        var apiResponse = await client.DeleteAsync(character.Name);

        // Assert
        apiResponse.StatusCode.Should().Be(StatusCodes.Status204NoContent);
    }
}
