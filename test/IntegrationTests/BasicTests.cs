﻿using Flurl.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using System.Net.Mime;

namespace StarWars.IntegrationTests;

public class BasicTests : FixtureBase
{
    public BasicTests(StarWarsApiFactory factory) : base(factory)
    {

    }

    [Fact]
    public async Task Get_EndpointBlockNonHttpsRequests()
    {
        // Arrange
        var client = CreateFlurlHttpClient();

        // Act
        var response = await client
            .Request()
            .GetAsync();

        // Assert
        response.StatusCode.Should().Be(StatusCodes.Status403Forbidden);
    }

    [Fact]
    public async Task Get_EndpointReturnCorrectCharset()
    {
        // Arrange
        const string utf8Charset = "charset=utf-8";
        var client = CreateFlurlHttpsClient();

        // Act
        var response = await client
            .Request()
            .AppendPathSegment("api")
            .AppendPathSegment("characters")
            .GetAsync();

        // Assert
        response.Headers.FirstOrDefault(HeaderNames.ContentType).Should().Contain(utf8Charset);
    }

    [Fact]
    public async Task Get_EndpointReturnCorrectContentType()
    {
        // Arrange
        var client = CreateFlurlHttpsClient();

        // Act
        var response = await client
            .Request()
            .AppendPathSegment("api")
            .AppendPathSegment("characters")
            .GetAsync();

        // Assert
        response.Headers.FirstOrDefault(HeaderNames.ContentType).Should().Contain(MediaTypeNames.Application.Json);
    }

    [Fact]
    public async Task Get_EndpointReturnSuccessCode()
    {
        // Arrange
        var client = CreateFlurlHttpsClient();

        // Act
        var response = await client
            .Request()
            .AppendPathSegment("api")
            .AppendPathSegment("characters")
            .GetAsync();

        // Assert
        response.StatusCode.Should().Be(StatusCodes.Status200OK);
    }
}
