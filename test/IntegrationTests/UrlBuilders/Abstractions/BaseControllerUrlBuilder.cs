﻿using Flurl;
using StarWars.Api.Controllers.Abstractions;

namespace StarWars.IntegrationTests.UrlBuilders.Abstractions;

public abstract class BaseControllerUrlBuilder<TController>
    where TController : ControllerBaseWithResults
{
    protected const string BaseUrl = "https://localhost/api";

    private readonly string _controllerPathSegment;

    protected BaseControllerUrlBuilder()
    {
        _controllerPathSegment = typeof(TController).Name.Replace("Controller", string.Empty);
    }

    protected Url GetBaseUrl()
    {
        return BaseUrl.AppendPathSegment(_controllerPathSegment);
    }
}
