﻿using Flurl;
using StarWars.Api.Controllers;
using StarWars.Api.InputModels.Pagination;
using StarWars.IntegrationTests.UrlBuilders.Abstractions;

namespace StarWars.IntegrationTests.UrlBuilders;

public class WebhooksControllerUrlBuilder : BaseControllerUrlBuilder<WebhooksController>
{
    public Url GetWebhooks(PagedInputModel pagedInputModel)
    {
        return GetBaseUrl()
            .SetQueryParam(nameof(PagedInputModel.PageNumber), pagedInputModel.PageNumber)
            .SetQueryParam(nameof(PagedInputModel.PageSize), pagedInputModel.PageSize);
    }

    public Url GetById(Guid externalId) => GetBaseUrl().AppendPathSegment(externalId);

    public Url GetWebhookCallsPage(PagedInputModel pagedInputModel, Guid externalId)
    {
        return GetBaseUrl()
            .AppendPathSegment(externalId)
            .AppendPathSegment("calls")
            .SetQueryParam(nameof(PagedInputModel.PageNumber), pagedInputModel.PageNumber)
            .SetQueryParam(nameof(PagedInputModel.PageSize), pagedInputModel.PageSize);
    }

    public Url GetCallById(Guid webhookExternalId, Guid callExternalId)
    {
        return GetBaseUrl()
            .AppendPathSegment(webhookExternalId)
            .AppendPathSegment("calls")
            .AppendPathSegment(callExternalId);
    }

    public Url Create() => GetBaseUrl();

    public Url Delete(Guid externalId) => GetBaseUrl().AppendPathSegment(externalId);
}
