﻿using Flurl;
using StarWars.Api.Controllers;
using StarWars.Api.InputModels.Pagination;
using StarWars.IntegrationTests.UrlBuilders.Abstractions;

namespace StarWars.IntegrationTests.UrlBuilders;

public class EpisodesControllerUrlBuilder : BaseControllerUrlBuilder<EpisodesController>
{
    public Url GetEpisodes(PagedInputModel pagedInputModel)
    {
        return GetBaseUrl()
            .SetQueryParam(nameof(PagedInputModel.PageNumber), pagedInputModel.PageNumber)
            .SetQueryParam(nameof(PagedInputModel.PageSize), pagedInputModel.PageSize);
    }

    public Url GetByPartNumber(int partNumber) => GetBaseUrl().AppendPathSegment(partNumber);

    public Url GetEpisodeCharactersPage(PagedInputModel pagedInputModel, int partNumber)
    {
        return GetBaseUrl()
            .AppendPathSegment(partNumber)
            .AppendPathSegment("characters")
            .SetQueryParam(nameof(PagedInputModel.PageNumber), pagedInputModel.PageNumber)
            .SetQueryParam(nameof(PagedInputModel.PageSize), pagedInputModel.PageSize);
    }

    public Url Create() => GetBaseUrl();

    public Url Update(int partNumber) => GetBaseUrl().AppendPathSegment(partNumber);

    public Url Delete(int partNumber) => GetBaseUrl().AppendPathSegment(partNumber);
}
