﻿using Flurl;
using StarWars.Api.Controllers;
using StarWars.Api.InputModels.Pagination;
using StarWars.IntegrationTests.UrlBuilders.Abstractions;

namespace StarWars.IntegrationTests.UrlBuilders;

public class CharactersControllerUrlBuilder : BaseControllerUrlBuilder<CharactersController>
{
    public Url GetPage(PagedInputModel pagedInputModel)
    {
        return GetBaseUrl()
            .SetQueryParam(nameof(PagedInputModel.PageNumber), pagedInputModel.PageNumber)
            .SetQueryParam(nameof(PagedInputModel.PageSize), pagedInputModel.PageSize);
    }

    public Url GetByName(string name) => GetBaseUrl().AppendPathSegment(name);

    public Url Create() => GetBaseUrl();

    public Url Update(string name) => GetBaseUrl().AppendPathSegment(name);

    public Url Delete(string name) => GetBaseUrl().AppendPathSegment(name);
}
