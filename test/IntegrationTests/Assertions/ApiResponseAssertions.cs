﻿using StarWars.Application.Common;
using StarWars.IntegrationTests.ApiClients.Models;

namespace StarWars.IntegrationTests.Assertions;

public class ApiResponseAssertions<T> : ApiResponseAssertions<T, ApiResponse<Envelope<T>>, ApiResponseAssertions<T>>
{
    public ApiResponseAssertions(ApiResponse<Envelope<T>> subject) : base(subject)
    {
    }
}
