﻿using StarWars.Application.Common;
using StarWars.IntegrationTests.ApiClients.Models;

namespace StarWars.IntegrationTests.Assertions;

public static class AssertionExtensions
{
    public static ApiResponseAssertions<T> Should<T>(this ApiResponse<Envelope<T>> apiResponse)
    {
        return new ApiResponseAssertions<T>(apiResponse);
    }
}
