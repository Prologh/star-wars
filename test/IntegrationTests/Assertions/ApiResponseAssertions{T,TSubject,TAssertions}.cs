﻿using FluentAssertions.Execution;
using FluentAssertions.Primitives;
using Newtonsoft.Json;
using StarWars.Application.Common;
using StarWars.IntegrationTests.ApiClients.Models;

namespace StarWars.IntegrationTests.Assertions;

public class ApiResponseAssertions<T, TSubject, TAssertions> : ReferenceTypeAssertions<TSubject, TAssertions>
    where TSubject : ApiResponse<Envelope<T>>
    where TAssertions : ReferenceTypeAssertions<TSubject, TAssertions>
{
    public ApiResponseAssertions(TSubject subject) : base(subject)
    {
    }

    protected override string Identifier => "apiResponse";

    public AndConstraint<ApiResponseAssertions<T, TSubject, TAssertions>> BeSuccessful(
        string because = "",
        params object[] becauseArgs)
    {
        Execute.Assertion
            .BecauseOf(because, becauseArgs)
            .ForCondition(Subject.HasSucceeded)
            .FailWith(
                "Expected API response to be successful{reason}, but found {0} response code with errors:\n{1}",
                Subject.StatusCode,
                JsonConvert.SerializeObject(Subject.ResponseData?.Errors));

        return new AndConstraint<ApiResponseAssertions<T, TSubject, TAssertions>>(this);
    }

    public AndConstraint<ApiResponseAssertions<T, TSubject, TAssertions>> BeFailure(
        string because = "",
        params object[] becauseArgs)
    {
        Execute.Assertion
            .BecauseOf(because, becauseArgs)
            .ForCondition(Subject.HasFailed)
            .FailWith(
                "Expected API response to be failure{reason}, but found {0} response code with data:\n{1}",
                Subject.StatusCode,
                Subject.ResponseData != null ? JsonConvert.SerializeObject(Subject.ResponseData.Data) : "null");

        return new AndConstraint<ApiResponseAssertions<T, TSubject, TAssertions>>(this);
    }
}
