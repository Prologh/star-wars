﻿using StarWars.Infrastructure.Converters;

namespace StarWars.UnitTests.Infrastructure.Converters;

public class SimplePascalCaseToKebabCaseConverterTests
{
    [Theory]
    [InlineData("s", "s")]
    [InlineData("S", "s")]
    [InlineData("Foo", "foo")]
    [InlineData("123", "123")]
    [InlineData("Foo123", "foo-123")]
    [InlineData("123Foo", "123-foo")]
    [InlineData("FOO", "foo")]
    [InlineData("foO", "fo-o")]
    [InlineData("foo bar", "foo bar")]
    [InlineData("FooBar", "foo-bar")]
    public void Should_ConvertInput_ToKebabCase(string input, string expectedResult)
    {
        // Arrange
        var converter = new SimplePascalCaseToKebabCaseConverter();

        // Act
        var result = converter.Convert(input);

        // Assert
        result.Should().Be(expectedResult);
    }
}
