﻿using Microsoft.Extensions.Options;
using StarWars.Api.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace StarWars.UnitTests.Extensions.DependencyInjection;

public class SwaggerGeneratorServiceCollectionExtensionsTests
{
    [Fact]
    public void ExtensionMethodShouldAddSwaggerGeneratorWithOptions()
    {
        // Arrange
        var services = new ServiceCollection();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionOptions = serviceProvider.GetService<IOptions<SwaggerGenOptions>>()?.Value;
        services.AddSwaggerGenWithDefaultOptions();
        serviceProvider = services.BuildServiceProvider();
        var postExtensionOptions = serviceProvider.GetService<IOptions<SwaggerGenOptions>>()?.Value;

        // Assert
        preExtensionOptions
            .Should()
            .BeNull("Because no Swagger Generator options has been registered yet.");
        postExtensionOptions
            .Should()
            .NotBeNull("Because extension method should already add " +
                    "Swagger Generator options to the service collection.");
    }
}
