﻿using AutoMapper;
using StarWars.Api.Extensions.DependencyInjection;

namespace StarWars.UnitTests.Extensions.DependencyInjection;

public class AutoMapperServiceCollectionExtensionsTests
{
    [Fact]
    public void ExtensionMethodAddsAutoMapperWithProfiles()
    {
        // Arrange
        var services = new ServiceCollection();

        // Act
        var serviceProvider = services.BuildServiceProvider();
        var preExtensionMapper = serviceProvider.GetService<IMapper>();
        services.AddAutoMapperWithProfiles();
        serviceProvider = services.BuildServiceProvider();
        var postExtensionMapper = serviceProvider.GetService<IMapper>();

        // Assert
        preExtensionMapper
            .Should()
            .BeNull("Because AutoMapper has not been registered yet.");
        postExtensionMapper
            .Should()
            .NotBeNull("Because extension method should already add " +
                    "AutoMapper to the service collection.");
        postExtensionMapper.ConfigurationProvider
            .Should()
            .BeAssignableTo<MapperConfiguration>().Subject
            .As<AutoMapper.Internal.IGlobalConfiguration>()
            .Profiles
            .Should()
            .NotBeEmpty("Because extension method should add mapping profiles.");
    }
}
