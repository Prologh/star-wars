﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StarWars.Api.Migrations;

public partial class InitialCreate : Migration
{
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "Character",
            columns: table => new
            {
                Name = table.Column<string>(maxLength: 50, nullable: false),
                Episodes = table.Column<string>(nullable: true),
                Friends = table.Column<string>(nullable: true),
                Planet = table.Column<string>(maxLength: 50, nullable: true)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_Character", x => x.Name);
            });
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropTable(
            name: "Character");
    }
}
