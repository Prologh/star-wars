﻿namespace StarWars.Domain;

/// <summary>
/// Contains all kind of constant values used accross the application.
/// </summary>
public static class Constants
{
    /// <summary>
    /// The code base project name.
    /// </summary>
    public const string ProjectName = "Star Wars";

    /// <summary>
    /// Contains pagination-related constants.
    /// </summary>
    public static class Pagination
    {
        public const int MinPageNumber = 1;

        public const int MaxPageNumber = 1000;

        public const int MinPageSize = 1;

        public const int MaxPageSize = 50;

        /// <summary>
        /// Contains default pagination-related constants.
        /// </summary>
        public static class Defaults
        {
            /// <summary>
            /// The default page number.
            /// </summary>
            public const int PageNumber = 1;

            /// <summary>
            /// The default page size.
            /// </summary>
            public const int PageSize = 50;
        }
    }

    /// <summary>
    /// Contains pagination-related constants.
    /// </summary>
    public static class Sorting
    {
        public const bool IsDefaultDescending = false;
    }

    /// <summary>
    /// Contains validation-related constants.
    /// </summary>
    public static class Validation
    {
        public const int MaxWebhooksPerTopic = 5;

        public const int MaxWebhookTargetUrlLength = 500;

        /// <summary>
        /// The allowed length of a name.
        /// </summary>
        public const int MaxCharacterNameLength = 50;

        /// <summary>
        /// The allowed length of a movie episode name.
        /// </summary>
        public const int MaxEpisodeNameLength = 50;

        /// <summary>
        /// The allowed length of a planet name.
        /// </summary>
        public const int MaxPlanetNameLength = 50;

        /// <summary>
        /// The allowed maximum amount of characters originating from a planet.
        /// </summary>
        public const int MaxPlanetCharactersCount = 10;
    }
}
