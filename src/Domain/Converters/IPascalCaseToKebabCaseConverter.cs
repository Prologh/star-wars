﻿namespace StarWars.Domain.Converters;

public interface IPascalCaseToKebabCaseConverter
{
    string Convert(string input);
}
