﻿using StarWars.Domain.Abstractions;
using StarWars.Domain.Characters;

namespace StarWars.Domain.Planets;

/// <summary>
/// Represents a planet in the Star Wars universe.
/// </summary>
public class PlanetEntity : BaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PlanetEntity"/> class.
    /// </summary>
    public PlanetEntity()
    {
        Characters = new List<CharacterEntity>();
    }

    /// <summary>
    /// Gets or sets the planet name.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets all characters that originated from the planet.
    /// </summary>
    public IList<CharacterEntity> Characters { get; set; }
}
