﻿namespace StarWars.Domain.Webhooks;

public enum WebhookTopic
{
    CharacterCreated,
    CharacterUpdated,
    CharacterDeleted,
    EpisodeCreated,
    EpisodeUpdated,
    EpisodeDeleted,
    PlanetCreated,
    PlanetUpdated,
    PlanetDeleted,
}
