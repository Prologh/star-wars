﻿using StarWars.Domain.Abstractions;

namespace StarWars.Domain.Webhooks;

public class WebhookEntity : BaseEntity
{
    public WebhookEntity()
    {
        Calls = [];
    }

    public IList<WebhookCallEntity> Calls { get; set; }

    public string TargetUrl { get; set; }

    public WebhookTopic Topic { get; set; }
}
