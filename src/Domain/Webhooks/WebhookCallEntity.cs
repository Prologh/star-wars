﻿using StarWars.Domain.Abstractions;

namespace StarWars.Domain.Webhooks;

public class WebhookCallEntity : BaseEntity
{
    public long DurationInMiliseconds { get; set; }

    public bool HasFailedResponse { get; set; }

    public bool HasTimedOut { get; set; }

    public int? ResponseStatusCode { get; set; }

    public long? ResponseLength { get; set; }

    public string Response { get; set; }

    public DateTime SentOn { get; set; }

    public string TargetUrl { get; set; }

    public WebhookTopic Topic { get; set; }

    public WebhookEntity Webhook { get; set; }

    public long WebhookId { get; set; }
}
