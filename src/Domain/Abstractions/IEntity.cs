﻿namespace StarWars.Domain.Abstractions;

public interface IEntity
{
    Guid ExternalId { get; }

    long Id { get; }
}
