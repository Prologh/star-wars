﻿namespace StarWars.Domain.Abstractions;

public abstract class BaseEntity : IEntity, IAuditableEntity
{
    protected BaseEntity()
    {
        ExternalId = Guid.NewGuid();
    }

    public Guid ExternalId { get; private set; }

    public long Id { get; private set; }

    public DateTime Created { get; private set; }

    public DateTime? Updated { get; private set; }

    public void SetCreatedToUtcNow() => Created = DateTime.Now;

    public void SetUpdatedToUtcNow() => Updated = DateTime.UtcNow;
}
