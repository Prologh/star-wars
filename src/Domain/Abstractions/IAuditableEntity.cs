﻿namespace StarWars.Domain.Abstractions;

public interface IAuditableEntity
{
    DateTime Created { get; }

    DateTime? Updated { get; }

    void SetCreatedToUtcNow();

    void SetUpdatedToUtcNow();
}
