﻿using StarWars.Domain.Abstractions;
using StarWars.Domain.Episodes;

namespace StarWars.Domain.Characters;

/// <summary>
/// Represents join relation of <see cref="CharacterEntity"/> and <see cref="EpisodeEntity"/>.
/// </summary>
public class CharacterEpisodeEntity : BaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CharacterEpisodeEntity"/> class.
    /// </summary>
    public CharacterEpisodeEntity()
    {

    }

    /// <summary>
    /// Gets or sets the related character.
    /// </summary>
    public CharacterEntity Character { get; set; }

    /// <summary>
    /// Gets or sets the id of related character.
    /// </summary>
    public long CharacterId { get; set; }

    /// <summary>
    /// Gets or sets the related episode.
    /// </summary>
    public EpisodeEntity Episode { get; set; }

    /// <summary>
    /// Gets or sets the related episode id.
    /// </summary>
    public long EpisodeId { get; set; }
}
