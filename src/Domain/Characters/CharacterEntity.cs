﻿using StarWars.Domain.Abstractions;
using StarWars.Domain.Planets;

namespace StarWars.Domain.Characters;

/// <summary>
/// Represents Star Wars character.
/// </summary>
public class CharacterEntity : BaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CharacterEntity"/> class.
    /// </summary>
    public CharacterEntity()
    {
        CharacterEpisodes = new List<CharacterEpisodeEntity>();
        Friends = new List<CharacterEntity>();
    }

    /// <summary>
    /// Gets or sets character name.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the friend.
    /// </summary>
    public CharacterEntity Friend { get; set; }

    /// <summary>
    /// Gets or sets the id of friend.
    /// </summary>
    public long? FriendId { get; set; }

    /// <summary>
    /// Gets a list of Star Wars episodes in which current character took part.
    /// </summary>
    public IList<CharacterEpisodeEntity> CharacterEpisodes { get; private set; }

    /// <summary>
    /// Gets or set a list of friends.
    /// </summary>
    public IList<CharacterEntity> Friends { get; private set; }

    /// <summary>
    /// Gets or sets the origin planet.
    /// </summary>
    public PlanetEntity OriginPlanet { get; set; }

    /// <summary>
    /// Gets or sets the id of origin plane.
    /// </summary>
    public long? OriginPlanetId { get; set; }
}
