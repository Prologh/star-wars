﻿using StarWars.Domain.Abstractions;
using StarWars.Domain.Characters;

namespace StarWars.Domain.Episodes;

/// <summary>
/// Represents an movie episode in the Star Wars universe.
/// </summary>
public class EpisodeEntity : BaseEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="EpisodeEntity"/> class.
    /// </summary>
    public EpisodeEntity()
    {
        CharacterEpisodes = new List<CharacterEpisodeEntity>();
    }

    /// <summary>
    /// Gets or sets the name of episode.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the episode part number in a related set of episodes (e.g. trilogy).
    /// </summary>
    public int PartNumber { get; set; }

    /// <summary>
    /// Gets a list of character episodes.
    /// </summary>
    public IList<CharacterEpisodeEntity> CharacterEpisodes { get; private set; }
}
