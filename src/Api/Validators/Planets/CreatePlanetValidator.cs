﻿using FluentValidation;
using StarWars.Api.InputModels.Planets;
using StarWars.Application.Extensions.FluentValidation;
using StarWars.Domain;

namespace StarWars.Api.Validators.Planets;

public class CreatePlanetValidator : AbstractValidator<CreatePlanetInputModel>
{
    public CreatePlanetValidator()
    {
        RuleFor(x => x.Name)
            .NotEmpty()
            .MaximumLength(Constants.Validation.MaxPlanetNameLength);

        RuleFor(x => x.Characters)
            .MaxElements(Constants.Validation.MaxPlanetCharactersCount);

        RuleForEach(x => x.Characters)
            .MaximumLength(Constants.Validation.MaxCharacterNameLength);
    }
}
