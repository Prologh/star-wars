﻿using FluentValidation;
using StarWars.Api.InputModels.Characters;
using StarWars.Domain;

namespace StarWars.Api.Validators.Characters;

public class CreateCharacterValidator : AbstractValidator<CreateCharacterInputModel>
{
    public CreateCharacterValidator()
    {
        RuleFor(p => p.Name)
            .NotEmpty()
            .MaximumLength(Constants.Validation.MaxCharacterNameLength);

        RuleFor(p => p.Planet)
            .MaximumLength(Constants.Validation.MaxPlanetNameLength);

        RuleForEach(p => p.Episodes)
            .MaximumLength(Constants.Validation.MaxEpisodeNameLength);

        RuleForEach(p => p.Friends)
            .MaximumLength(Constants.Validation.MaxCharacterNameLength);
    }
}
