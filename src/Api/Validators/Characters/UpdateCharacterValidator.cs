﻿using FluentValidation;
using StarWars.Api.InputModels.Characters;
using StarWars.Domain;

namespace StarWars.Api.Validators.Characters;

public class UpdateCharacterValidator : AbstractValidator<UpdateCharacterInputModel>
{
    public UpdateCharacterValidator()
    {
        RuleFor(p => p.NewName)
            .MaximumLength(Constants.Validation.MaxCharacterNameLength);

        RuleFor(p => p.Planet)
            .MaximumLength(Constants.Validation.MaxPlanetNameLength);

        RuleForEach(p => p.Episodes)
            .MaximumLength(Constants.Validation.MaxEpisodeNameLength);

        RuleForEach(p => p.Friends)
            .MaximumLength(Constants.Validation.MaxCharacterNameLength);
    }
}
