﻿using FluentValidation;
using StarWars.Api.InputModels.Webhooks.InputModels;
using StarWars.Domain;

namespace StarWars.Api.Validators.Webhooks;

public class CreateWebhookValidator : AbstractValidator<CreateWebhookInputModel>
{
    public CreateWebhookValidator()
    {
        RuleFor(x => x.TargetUrl)
            .NotEmpty()
            .MaximumLength(Constants.Validation.MaxWebhookTargetUrlLength);
        RuleFor(x => x.Topic)
            .NotNull();
    }
}
