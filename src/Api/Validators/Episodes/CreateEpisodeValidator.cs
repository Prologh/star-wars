﻿using FluentValidation;
using StarWars.Api.InputModels.Episodes;
using StarWars.Domain;

namespace StarWars.Api.Validators.Episodes;

public class CreateEpisodeValidator : AbstractValidator<CreateEpisodeInputModel>
{
    public CreateEpisodeValidator()
    {
        RuleFor(p => p.Name)
            .NotEmpty()
            .MaximumLength(Constants.Validation.MaxEpisodeNameLength);

        RuleFor(p => p.PartNumber)
            .NotEmpty()
            .GreaterThan(0)
            .LessThanOrEqualTo(100);
    }
}
