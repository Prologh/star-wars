﻿using FluentValidation;
using StarWars.Api.InputModels.Episodes;
using StarWars.Domain;

namespace StarWars.Api.Validators.Episodes;

public class UpdateEpisodeValidator : AbstractValidator<UpdateEpisodeInputModel>
{
    public UpdateEpisodeValidator()
    {
        RuleFor(p => p.Name)
            .NotEmpty()
            .MaximumLength(Constants.Validation.MaxEpisodeNameLength);

        RuleFor(p => p.NewPartNumber)
            .GreaterThan(0)
            .LessThanOrEqualTo(100);
    }
}
