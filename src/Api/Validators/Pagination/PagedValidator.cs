﻿using FluentValidation;
using StarWars.Api.InputModels.Pagination;
using StarWars.Domain;

namespace StarWars.Api.Validators.Pagination;

public class PagedValidator : AbstractValidator<PagedInputModel>
{
    public PagedValidator()
    {
        RuleFor(p => p.PageNumber)
            .GreaterThanOrEqualTo(Constants.Pagination.MinPageNumber)
            .LessThanOrEqualTo(Constants.Pagination.MaxPageNumber);
        RuleFor(p => p.PageSize)
            .GreaterThanOrEqualTo(Constants.Pagination.MinPageSize)
            .LessThanOrEqualTo(Constants.Pagination.MaxPageSize);
    }
}
