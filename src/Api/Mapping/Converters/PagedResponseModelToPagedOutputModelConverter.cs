﻿using AutoMapper;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Application.Pagination.ResponseModels;

namespace StarWars.Api.Mapping.Converters;

public class PagedResponseModelToPagedOutputModelConverter<TResponseModel, TOutputModel>
    : ITypeConverter<PagedResponseModel<TResponseModel>, PagedOutputModel<TOutputModel>>
{
    public PagedOutputModel<TOutputModel> Convert(
        PagedResponseModel<TResponseModel> source,
        PagedOutputModel<TOutputModel> destination,
        ResolutionContext context)
    {
        var mappedItems = context.Mapper.Map<IReadOnlyList<TOutputModel>>(source.Items);

        return new PagedOutputModel<TOutputModel>(
            source.PageNumber,
            source.PageSize,
            source.TotalPages,
            source.TotalSize,
            mappedItems);
    }
}
