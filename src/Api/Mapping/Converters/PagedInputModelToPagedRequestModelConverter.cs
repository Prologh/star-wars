﻿using AutoMapper;
using StarWars.Api.InputModels.Pagination;
using StarWars.Application.Pagination.RequestModels;

namespace StarWars.Api.Mapping.Converters;

public class PagedInputModelToPagedRequestModelConverter<TPagedResponseItem>
    : ITypeConverter<PagedInputModel, PagedRequestModel<TPagedResponseItem>>
    where TPagedResponseItem : class
{
    public PagedRequestModel<TPagedResponseItem> Convert(
        PagedInputModel source,
        PagedRequestModel<TPagedResponseItem> destination,
        ResolutionContext context)
    {
        return new PagedRequestModel<TPagedResponseItem>(
            pageNumber: source.PageNumber,
            pageSize: source.PageSize);
    }
}
