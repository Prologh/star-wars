﻿using AutoMapper;
using StarWars.Api.InputModels.Webhooks.InputModels;
using StarWars.Api.OutputModels.Webhooks;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;

namespace StarWars.Api.Mapping;

public class WebhookProfile : Profile
{
    public WebhookProfile()
    {
        // Input model ==> Request model
        CreateMap<CreateWebhookInputModel, CreateWebhookRequestModel>();

        // Response model ==> Output model
        CreateMap<WebhookResponseModel, WebhookOutputModel>();
        CreateMap<WebhookCallResponseModel, WebhookCallOutputModel>();
        CreateMap<WebhookCallPagedResponseModel, WebhookCallPagedOutputModel>();
    }
}
