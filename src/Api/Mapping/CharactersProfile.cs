﻿using AutoMapper;
using StarWars.Api.InputModels.Characters;
using StarWars.Api.OutputModels.Characters;
using StarWars.Application.Characters.RequestModels;
using StarWars.Application.Characters.ResponseModels;

namespace StarWars.Api.Mapping;

public class CharactersProfile : Profile
{
    public CharactersProfile()
    {
        // Input model ==> Request model
        CreateMap<CreateCharacterInputModel, CreateCharacterRequestModel>();
        CreateMap<UpdateCharacterInputModel, UpdateCharacterRequestModel>()
            .ForMember(x => x.Name, opts => opts.Ignore());

        // Response model ==> Output model
        CreateMap<CharacterResponseModel, CharacterOutputModel>()
            .ForMember(dst => dst.OriginPlanet, opts => opts.MapFrom(src => src.OriginPlanet.Name))
            .ForMember(dst => dst.Friends, opts => opts.MapFrom(src => src.Friends.Select(f => f.Name)))
            .ForMember(dst => dst.Episodes, opts => opts.MapFrom(src => src.CharacterEpisodes.Select(s => s.Episode.Name)));
    }
}
