﻿using AutoMapper;
using StarWars.Api.InputModels.Episodes;
using StarWars.Api.OutputModels.Episodes;
using StarWars.Application.Episodes.RequestModels;
using StarWars.Application.Episodes.ResponseModels;

namespace StarWars.Api.Mapping;

public class EpisodesProfile : Profile
{
    public EpisodesProfile()
    {
        // Input model ==> Request model
        CreateMap<CreateEpisodeInputModel, CreateEpisodeRequestModel>();
        CreateMap<UpdateEpisodeInputModel, UpdateEpisodeRequestModel>()
            .ForMember(x => x.PartNumber, opts => opts.Ignore());

        // Response model ==> Output model
        CreateMap<EpisodeResponseModel, EpisodeOutputModel>();
        CreateMap<EpisodeCharacterResponseModel, EpisodeCharacterOutputModel>();
    }
}
