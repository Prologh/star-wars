﻿using AutoMapper;
using StarWars.Api.InputModels.Pagination;
using StarWars.Api.Mapping.Converters;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Pagination.ResponseModels;

namespace StarWars.Api.Mapping;

public class PaginationProfile : Profile
{
    public PaginationProfile()
    {
        // Input model ==> Request model
        CreateMap(typeof(PagedInputModel), typeof(PagedRequestModel<>))
            .ConvertUsing(typeof(PagedInputModelToPagedRequestModelConverter<>));

        // Response model ==> Output model
        CreateMap(typeof(PagedResponseModel<>), typeof(PagedOutputModel<>))
            .ConvertUsing(typeof(PagedResponseModelToPagedOutputModelConverter<,>));
    }
}
