﻿using AutoMapper;
using StarWars.Api.InputModels.Planets;
using StarWars.Api.OutputModels.Planets;
using StarWars.Application.Planets.RequestModels;
using StarWars.Application.Planets.ResponseModels;

namespace StarWars.Api.Mapping;

public class PlanetsProfile : Profile
{
    public PlanetsProfile()
    {
        // Input model ==> Request model
        CreateMap<CreatePlanetInputModel, CreatePlanetRequestModel>()
            .ForMember(dst => dst.PlanetName, opts => opts.MapFrom(src => src.Name));

        // Response model ==> Output model
        CreateMap<PlanetResponseModel, PlanetOutputModel>();
        CreateMap<PlanetCharacterResponseModel, PlanetCharacterOutputModel>();
    }
}
