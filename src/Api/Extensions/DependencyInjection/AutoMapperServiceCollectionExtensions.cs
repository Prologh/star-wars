﻿using AutoMapper;
using StarWars.Domain;
using System.Reflection;

namespace StarWars.Api.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for adding <see cref="Mapper"/> to
/// <see cref="IServiceCollection"/>.
/// </summary>
public static class AutoMapperServiceCollectionExtensions
{
    /// <summary>
    /// Add <see cref="Mapper"/> functionalities to the <see cref="IServiceCollection"/>
    /// and adds predefined set of mapping profiles.
    /// </summary>
    /// <param name="services">
    /// The <see cref="IServiceCollection"/> instance.
    /// </param>
    /// <returns>
    /// Returns <see cref="IMvcBuilder"/> with added <see cref="Mapper"/>
    /// functionalities so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddAutoMapperWithProfiles(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        var projectNameSpace = Constants.ProjectName.Replace(" ", string.Empty);
        var executingAssembly = Assembly.GetExecutingAssembly();
        var assemblies = executingAssembly
            .GetReferencedAssemblies()
            .Where(assemblyName => assemblyName.Name.StartsWith(projectNameSpace))
            .Select(assemblyName => Assembly.Load(assemblyName))
            .Union(new[] { executingAssembly });

        return services.AddAutoMapper(assemblies);
    }
}
