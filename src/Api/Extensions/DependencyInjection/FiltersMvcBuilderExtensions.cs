﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace StarWars.Api.Extensions.DependencyInjection;

public static class FiltersMvcBuilderExtensions
{
    public static IMvcBuilder AddFilter<TFilterType>(this IMvcBuilder mvcBuilder)
        where TFilterType : IFilterMetadata
    {
        Guard.Against.Null(mvcBuilder);

        mvcBuilder.Services.Configure<MvcOptions>(options => options.Filters.Add<TFilterType>());

        return mvcBuilder;
    }

    public static IMvcBuilder AddFilter(this IMvcBuilder mvcBuilder, Type filterType)
    {
        Guard.Against.Null(mvcBuilder);
        Guard.Against.Null(filterType);

        mvcBuilder.Services.Configure<MvcOptions>(options => options.Filters.Add(filterType));

        return mvcBuilder;
    }

    public static IMvcBuilder AddFilter<TFilterType>(this IMvcBuilder mvcBuilder, int order)
        where TFilterType : IFilterMetadata
    {
        Guard.Against.Null(mvcBuilder);

        mvcBuilder.Services.Configure<MvcOptions>(options => options.Filters.Add<TFilterType>(order));

        return mvcBuilder;
    }

    public static IMvcBuilder AddFilter(this IMvcBuilder mvcBuilder, Type filterType, int order)
    {
        Guard.Against.Null(mvcBuilder);
        Guard.Against.Null(filterType);

        mvcBuilder.Services.Configure<MvcOptions>(options => options.Filters.Add(filterType, order));

        return mvcBuilder;
    }

    public static IMvcBuilder AddFilterService<TFilterType>(this IMvcBuilder mvcBuilder)
        where TFilterType : IFilterMetadata
    {
        Guard.Against.Null(mvcBuilder);

        mvcBuilder.Services.Configure<MvcOptions>(options => options.Filters.AddService<TFilterType>());

        return mvcBuilder;
    }

    public static IMvcBuilder AddFilterService(this IMvcBuilder mvcBuilder, Type filterType)
    {
        Guard.Against.Null(mvcBuilder);
        Guard.Against.Null(filterType);

        mvcBuilder.Services.Configure<MvcOptions>(options => options.Filters.AddService(filterType));

        return mvcBuilder;
    }

    public static IMvcBuilder AddFilterService<TFilterType>(this IMvcBuilder mvcBuilder, int order)
        where TFilterType : IFilterMetadata
    {
        Guard.Against.Null(mvcBuilder);

        mvcBuilder.Services.Configure<MvcOptions>(options => options.Filters.AddService<TFilterType>(order));

        return mvcBuilder;
    }

    public static IMvcBuilder AddFilterService(this IMvcBuilder mvcBuilder, Type filterType, int order)
    {
        Guard.Against.Null(mvcBuilder);
        Guard.Against.Null(filterType);

        mvcBuilder.Services.Configure<MvcOptions>(options => options.Filters.AddService(filterType, order));

        return mvcBuilder;
    }
}
