﻿using Microsoft.Extensions.Options;
using StarWars.Api.Mvc.Options;

namespace StarWars.Api.Extensions.DependencyInjection;

public static class ConfigureOptionsServiceCollectionExtensions
{
    public static IServiceCollection ConfigureWithDependencies<TOptions, TDependency>(
        this IServiceCollection serviceCollection,
        Action<TOptions, TDependency> configureOptions)
        where TOptions : class
    {
        Guard.Against.Null(serviceCollection);
        Guard.Against.Null(configureOptions);

        serviceCollection.AddSingleton<IConfigureOptions<TOptions>, ConfigureOptionsWithDependencyContainer<TOptions, TDependency>>();
        serviceCollection.Configure<ConfigureOptionsWithDependency<TOptions, TDependency>>(options => options.Action = configureOptions);

        return serviceCollection;
    }
}
