﻿using StarWars.Domain.Converters;
using StarWars.Infrastructure.Converters;

namespace StarWars.Api.Extensions.DependencyInjection;

public static class TextConvertersServiceCollectionExtensions
{
    public static IServiceCollection AddKebabCaseConverter(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddTransient<IPascalCaseToKebabCaseConverter, SimplePascalCaseToKebabCaseConverter>();

        return services;
    }
}
