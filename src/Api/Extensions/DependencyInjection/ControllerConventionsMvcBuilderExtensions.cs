﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Routing;

namespace StarWars.Api.Extensions.DependencyInjection;

public static class ControllerConventionsMvcBuilderExtensions
{
    public static IMvcBuilder AddRouteTokenTransformerConvention<TTransformer>(
        this IMvcBuilder mvcBuilder)
        where TTransformer : class, IOutboundParameterTransformer
    {
        Guard.Against.Null(mvcBuilder);

        mvcBuilder.Services.AddTransient<TTransformer>();
        mvcBuilder.Services.ConfigureWithDependencies<MvcOptions, TTransformer>((options, transformer) =>
        {
            var routeTokenTransformerConvention = new RouteTokenTransformerConvention(transformer);

            options.Conventions.Add(routeTokenTransformerConvention);
        });

        return mvcBuilder;
    }
}
