﻿using Microsoft.OpenApi.Models;
using StarWars.Api.Swagger.Filters;
using StarWars.Domain;
using System.IO;
using System.Reflection;

namespace StarWars.Api.Extensions.DependencyInjection;

/// <summary>
/// Provides extension methods for adding Swagger generator to
/// <see cref="IServiceCollection"/>.
/// </summary>
public static class SwaggerGeneratorServiceCollectionExtensions
{
    /// <summary>
    /// Adds Swagger generator to the <see cref="IServiceCollection"/>
    /// with default options registering one main Swagger document.
    /// </summary>
    /// <param name="services">
    /// Current <see cref="IServiceCollection"/> instance.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Swagger generator so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddSwaggerGenWithDefaultOptions(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddSwaggerGen(options =>
        {
            options.SchemaFilter<FluentValidationRulesSchemaFilter>();
            options.SwaggerDoc("v1", GetOpenApiInfo());

            var projectPrefix = Assembly.GetExecutingAssembly().GetName().Name.Split(".").First();
            var projectAssemblyNames = AppDomain.CurrentDomain.GetAssemblies()
                .Select(x => x.GetName().Name)
                .Where(x => x.StartsWith(projectPrefix) && !x.Contains("Tests"))
                .ToList();

            foreach (var assemblyName in projectAssemblyNames)
            {
                var xmlFile = $"{assemblyName}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            }
        });
        services.AddSwaggerGenNewtonsoftSupport();

        return services;
    }

    private static OpenApiInfo GetOpenApiInfo()
    {
        return new OpenApiInfo
        {
            Version = "v1",
            Title = $"{Constants.ProjectName} API",
            Description = $"A simple RESTful API managing {Constants.ProjectName} characters.",
            Contact = new OpenApiContact
            {
                Name = "Piotr Wosiek",
                Email = "piotr.m.wosiek@gmail.com",
                Url = new Uri("http://wosiek.azurewebsites.net"),
            },
            License = new OpenApiLicense
            {
                Name = "Licensed under MIT",
                Url = new Uri("https://gitlab.com/Prologh/star-wars/blob/master/LICENSE"),
            },
        };
    }
}
