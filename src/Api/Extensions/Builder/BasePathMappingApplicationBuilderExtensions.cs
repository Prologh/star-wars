﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace StarWars.Api.Extensions.Builder
{
    public static class BasePathMappingApplicationBuilderExtensions
    {
        public static IApplicationBuilder Map200ToBasePath(this IApplicationBuilder app)
        {
            app.UseWhen(context => context.Request.Path == "/" && context.Request.Method == HttpMethod.Get.Method, builder =>
            {
                builder.Use((HttpContext context, RequestDelegate _) => context.Response.WriteAsync("OK", context.RequestAborted));
            });

            return app;
        }
    }
}
