﻿using Microsoft.AspNetCore.Builder;
using StarWars.Api.Middleware;

namespace StarWars.Api.Extensions.Builder;

public static class NonHttpsBlockingApplicationBuilderExtensions
{
    public static IApplicationBuilder UseNonHttpsBlocking(this IApplicationBuilder applicationBuilder)
    {
        Guard.Against.Null(applicationBuilder);

        applicationBuilder.UseMiddleware<NonHttpsBlockingMiddleware>();

        return applicationBuilder;
    }
}
