﻿using Microsoft.AspNetCore.Builder;
using StarWars.Api.Middleware;

namespace StarWars.Api.Extensions.Builder;

public static class ExceptionHandlingApplicationBuilderExtensions
{
    public static IApplicationBuilder UseExceptionHandling(this IApplicationBuilder applicationBuilder)
    {
        Guard.Against.Null(applicationBuilder);

        applicationBuilder.UseMiddleware<ExceptionHandlingMiddleware>();

        return applicationBuilder;
    }
}
