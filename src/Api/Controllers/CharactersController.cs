﻿using AutoMapper;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StarWars.Api.Attributes;
using StarWars.Api.Controllers.Abstractions;
using StarWars.Api.InputModels.Characters;
using StarWars.Api.InputModels.Pagination;
using StarWars.Api.OutputModels.Characters;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Application.Characters.RequestModels;
using StarWars.Application.Characters.ResponseModels;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Pagination.ResponseModels;

namespace StarWars.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
[ApplicationArea(ApplicationArea.Characters)]
public class CharactersController : ControllerBaseWithResults
{
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    public CharactersController(
        IMapper mapper,
        IMediator mediator)
    {
        _mapper = mapper;
        _mediator = mediator;
    }

    /// <summary>
    /// Gets a page of characters.
    /// </summary>
    [HttpGet]
    [ProducesPagedEnvelopeType(typeof(CharacterOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<Envelope<PagedOutputModel<CharacterOutputModel>>>> GetPage([FromQuery] PagedInputModel input)
    {
        var request = _mapper.Map<PagedRequestModel<CharacterResponseModel>>(input);
        var pagedResponseModel = await _mediator.Send(request, RequestAborted);
        var pagedOutputModel = _mapper.Map<PagedOutputModel<CharacterOutputModel>>(pagedResponseModel);

        return new Envelope<PagedOutputModel<CharacterOutputModel>>(pagedOutputModel);
    }

    /// <summary>
    /// Gets single character by name.
    /// </summary>
    /// <param name="name">The name of character.</param>
    [HttpGet("{name}")]
    [ProducesEnvelopeType(typeof(CharacterOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Envelope<CharacterOutputModel>>> GetByName([FromRoute] string name)
    {
        var request = new GetCharacterRequestModel { Name = name };

        return await _mediator.Send(request, RequestAborted)
            .Map(response => _mapper.Map<CharacterOutputModel>(response))
            .Finally(result => CreateGetResult(result));
    }

    /// <summary>
    /// Creates new character.
    /// </summary>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create([FromBody] CreateCharacterInputModel viewModel)
    {
        var request = _mapper.Map<CreateCharacterRequestModel>(viewModel);

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateCreatedResult(result, nameof(GetByName), new { viewModel.Name }));
    }

    /// <summary>
    /// Updates single character.
    /// </summary>
    [HttpPut("{name}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Update([FromRoute] string name, [FromBody] UpdateCharacterInputModel viewModel)
    {
        var request = _mapper.Map<UpdateCharacterRequestModel>(viewModel);
        request.Name = name;

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateUpdatedResult(result));
    }

    /// <summary>
    /// Deletes single character.
    /// </summary>
    [HttpDelete("{name}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete([FromRoute] string name)
    {
        var request = new DeleteCharacterRequestModel { Name = name };

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateDeletedResult(result));
    }
}
