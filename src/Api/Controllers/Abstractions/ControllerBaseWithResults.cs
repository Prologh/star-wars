﻿using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading;

namespace StarWars.Api.Controllers.Abstractions;

[ProducesResponseType(StatusCodes.Status429TooManyRequests)]
public abstract class ControllerBaseWithResults : ControllerBase
{
    protected CancellationToken RequestAborted => Request.HttpContext.RequestAborted;

    protected ActionResult<Envelope<TValue>> CreateGetResult<TValue>(Result<TValue, Error> result)
    {
        return result.IsFailure
            ? HandleFailedResult(result)
            : new ActionResult<Envelope<TValue>>(new Envelope<TValue>(result.Value));
    }

    protected IActionResult CreateCreatedResult(UnitResult<Error> result, string controllerName, string actionName, object routeValues)
    {
        return result.IsFailure
            ? HandleFailedResult(result)
            : CreatedAtAction(actionName, controllerName, routeValues, value: null);
    }

    protected IActionResult CreateCreatedResult(UnitResult<Error> result, string actionName, object routeValues)
    {
        return result.IsFailure
            ? HandleFailedResult(result)
            : CreatedAtAction(actionName, routeValues, value: null);
    }

    protected IActionResult CreateCreatedResult<T>(Result<T, Error> result, string actionName, Func<T, object> routeValuesProvider)
    {
        return result.IsFailure
            ? HandleFailedResult(result)
            : CreatedAtAction(actionName, routeValuesProvider.Invoke(result.Value), value: null);
    }

    protected IActionResult CreateUpdatedResult(UnitResult<Error> result)
    {
        return result.IsFailure
            ? HandleFailedResult(result)
            : NoContent();
    }

    protected IActionResult CreateDeletedResult(UnitResult<Error> result)
    {
        return result.IsFailure
            ? HandleFailedResult(result)
            : NoContent();
    }

    private ObjectResult HandleFailedResult(IError<Error> result)
    {
        var statusCode = GetStatusCode(result.Error.Category);
        var envelope = new EmptyEnvelope(result.Error);

        return StatusCode(statusCode, envelope);
    }

    private int GetStatusCode(ErrorCategory errorCategory) => errorCategory switch
    {
        ErrorCategory.DataNotFound => StatusCodes.Status404NotFound,
        ErrorCategory.DataNotValid => StatusCodes.Status400BadRequest,
        ErrorCategory.Unknown or ErrorCategory.InternalError => StatusCodes.Status500InternalServerError,
        _ => throw new NotImplementedException($"Error category {errorCategory} is not supported."),
    };
}
