﻿using AutoMapper;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StarWars.Api.Attributes;
using StarWars.Api.Controllers.Abstractions;
using StarWars.Api.InputModels.Pagination;
using StarWars.Api.InputModels.Webhooks.InputModels;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Api.OutputModels.Webhooks;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Pagination.ResponseModels;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;

namespace StarWars.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
[ApplicationArea(ApplicationArea.Webhooks)]
public class WebhooksController : ControllerBaseWithResults
{
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    public WebhooksController(
        IMapper mapper,
        IMediator mediator)
    {
        _mapper = mapper;
        _mediator = mediator;
    }

    /// <summary>
    /// Gets a page of subscribed webhooks.
    /// </summary>
    [HttpGet]
    [ProducesPagedEnvelopeType(typeof(WebhookOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<Envelope<PagedOutputModel<WebhookOutputModel>>>> GetPage([FromQuery] PagedInputModel input)
    {
        var request = _mapper.Map<PagedRequestModel<WebhookResponseModel>>(input);
        var pagedResponseModel = await _mediator.Send(request, RequestAborted);
        var pagedOutputModel = _mapper.Map<PagedOutputModel<WebhookOutputModel>>(pagedResponseModel);

        return new Envelope<PagedOutputModel<WebhookOutputModel>>(pagedOutputModel);
    }

    /// <summary>
    /// Gets a single subscribed webhook by external ID.
    /// </summary>
    /// <param name="externalId">The external ID of webhook subscription.</param>
    [HttpGet("{externalId}")]
    [ProducesEnvelopeType(typeof(WebhookOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Envelope<WebhookOutputModel>>> GetById([FromRoute] Guid externalId)
    {
        var request = new GetWebhookRequestModel { ExternalId = externalId };

        return await _mediator.Send(request, RequestAborted)
            .Map(response => _mapper.Map<WebhookOutputModel>(response))
            .Finally(result => CreateGetResult(result));
    }

    /// <summary>
    /// Gets a page of calls for a selected subscribed webhook.
    /// </summary>
    /// <param name="externalId">The external ID of a subscribed webhook.</param>
    [HttpGet("{externalId}/calls")]
    [ProducesPagedEnvelopeType(typeof(WebhookCallPagedOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<Envelope<PagedOutputModel<WebhookCallPagedOutputModel>>>> GetPage([FromRoute] Guid externalId, [FromQuery] PagedInputModel input)
    {
        var request = new GetWebhookCallsPagedRequestModel(externalId, input.PageNumber, input.PageSize);
        var pagedResponseModel = await _mediator.Send(request, RequestAborted);
        var pagedOutputModel = _mapper.Map<PagedOutputModel<WebhookCallPagedOutputModel>>(pagedResponseModel);

        return new Envelope<PagedOutputModel<WebhookCallPagedOutputModel>>(pagedOutputModel);
    }

    /// <summary>
    /// Gets details of a single webhook call by external ID.
    /// </summary>
    /// <param name="externalId">The external ID of webhook subscription.</param>
    [HttpGet("{externalId}/calls/{callExternalId}")]
    [ProducesEnvelopeType(typeof(WebhookCallOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Envelope<WebhookCallOutputModel>>> GetCallById([FromRoute] Guid externalId, [FromRoute] Guid callExternalId)
    {
        var request = new GetWebhookCallRequestModel
        {
            WebhookExternalId = externalId,
            CallExternalId = callExternalId,
        };

        return await _mediator.Send(request, RequestAborted)
            .Map(response => _mapper.Map<WebhookCallOutputModel>(response))
            .Finally(result => CreateGetResult(result));
    }

    /// <summary>
    /// Creates a subscription to a webhook.
    /// </summary>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create([FromBody] CreateWebhookInputModel inputModel)
    {
        var request = _mapper.Map<CreateWebhookRequestModel>(inputModel);

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateCreatedResult(result, nameof(GetById), externalId => new { externalId }));
    }

    /// <summary>
    /// Deletes a subscription from a single webhook.
    /// </summary>
    /// <param name="externalId">The external ID of webhook subscription.</param>
    [HttpDelete("{externalId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete([FromRoute] Guid externalId)
    {
        var request = new DeleteWebhookRequestModel
        {
            ExternalId = externalId,
        };

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateDeletedResult(result));
    }
}
