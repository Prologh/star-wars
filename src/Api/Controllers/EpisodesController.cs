﻿using AutoMapper;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StarWars.Api.Attributes;
using StarWars.Api.Controllers.Abstractions;
using StarWars.Api.InputModels.Episodes;
using StarWars.Api.InputModels.Pagination;
using StarWars.Api.OutputModels.Episodes;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Application.Episodes.RequestModels;
using StarWars.Application.Episodes.ResponseModels;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Pagination.ResponseModels;

namespace StarWars.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
[ApplicationArea(ApplicationArea.Episodes)]
public class EpisodesController : ControllerBaseWithResults
{
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    public EpisodesController(
        IMapper mapper,
        IMediator mediator)
    {
        _mapper = mapper;
        _mediator = mediator;
    }

    /// <summary>
    /// Gets a page of episodes.
    /// </summary>
    [HttpGet]
    [ProducesPagedEnvelopeType(typeof(EpisodeOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<Envelope<PagedOutputModel<EpisodeOutputModel>>>> GetEpisodesPage([FromQuery] PagedInputModel input)
    {
        var request = _mapper.Map<PagedRequestModel<EpisodeResponseModel>>(input);
        var pagedResponseModel = await _mediator.Send(request, RequestAborted);
        var pagedOutputModel = _mapper.Map<PagedOutputModel<EpisodeOutputModel>>(pagedResponseModel);

        return new Envelope<PagedOutputModel<EpisodeOutputModel>>(pagedOutputModel);
    }

    /// <summary>
    /// Gets a episode by part number.
    /// </summary>
    /// <param name="partNumber">The part number of episode.</param>
    [HttpGet("{partNumber}")]
    [ProducesEnvelopeType(typeof(EpisodeOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Envelope<EpisodeOutputModel>>> GetByPartNumber([FromRoute] int partNumber)
    {
        var request = new GetEpisodeRequestModel { PartNumber = partNumber };

        return await _mediator.Send(request, RequestAborted)
            .Map(response => _mapper.Map<EpisodeOutputModel>(response))
            .Finally(result => CreateGetResult(result));
    }

    /// <summary>
    /// Gets a page of episode characters by part number.
    /// </summary>
    /// <param name="partNumber">The part number of episode.</param>
    [HttpGet("{partNumber}/characters")]
    [ProducesPagedEnvelopeType(typeof(EpisodeCharacterOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<Envelope<PagedOutputModel<EpisodeCharacterOutputModel>>>> GetEpisodeCharactersPage([FromRoute] int partNumber, [FromQuery] PagedInputModel input)
    {
        var request = new GetEpisodeCharactersPagedRequestModel(input.PageNumber, input.PageSize);
        request.PartNumber = partNumber;
        var pagedResponseModel = await _mediator.Send(request, RequestAborted);
        var pagedOutputModel = _mapper.Map<PagedOutputModel<EpisodeCharacterOutputModel>>(pagedResponseModel);

        return new Envelope<PagedOutputModel<EpisodeCharacterOutputModel>>(pagedOutputModel);
    }

    /// <summary>
    /// Creates new episode.
    /// </summary>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create([FromBody] CreateEpisodeInputModel viewModel)
    {
        var request = _mapper.Map<CreateEpisodeRequestModel>(viewModel);

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateCreatedResult(result, nameof(GetByPartNumber), new { viewModel.PartNumber }));
    }

    /// <summary>
    /// Updates single episode.
    /// </summary>
    [HttpPut("{partNumber}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Update([FromRoute] int partNumber, [FromBody] UpdateEpisodeInputModel viewModel)
    {
        var request = _mapper.Map<UpdateEpisodeRequestModel>(viewModel);
        request.PartNumber = partNumber;

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateUpdatedResult(result));
    }

    /// <summary>
    /// Deletes single episode.
    /// </summary>
    /// <param name="partNumber">The part number of episode.</param>
    [HttpDelete("{partNumber}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete([FromRoute] int partNumber)
    {
        var request = new DeleteEpisodeRequestModel
        {
            PartNumber = partNumber,
        };

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateDeletedResult(result));
    }
}
