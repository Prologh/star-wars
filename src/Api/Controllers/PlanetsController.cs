﻿using AutoMapper;
using CSharpFunctionalExtensions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StarWars.Api.Attributes;
using StarWars.Api.Controllers.Abstractions;
using StarWars.Api.InputModels.Pagination;
using StarWars.Api.InputModels.Planets;
using StarWars.Api.OutputModels.Pagination;
using StarWars.Api.OutputModels.Planets;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Planets.RequestModels;
using StarWars.Application.Planets.ResponseModels;

namespace StarWars.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
[ApplicationArea(ApplicationArea.Planets)]
public class PlanetsController : ControllerBaseWithResults
{
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    public PlanetsController(
        IMapper mapper,
        IMediator mediator)
    {
        _mapper = mapper;
        _mediator = mediator;
    }

    /// <summary>
    /// Gets a page of planets.
    /// </summary>
    [HttpGet]
    [ProducesPagedEnvelopeType(typeof(PlanetOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<Envelope<PagedOutputModel<PlanetOutputModel>>>> GetPlanetsPage([FromQuery] PagedInputModel input)
    {
        var request = _mapper.Map<PagedRequestModel<PlanetResponseModel>>(input);
        var pagedResponseModel = await _mediator.Send(request, RequestAborted);
        var pagedOutputModel = _mapper.Map<PagedOutputModel<PlanetOutputModel>>(pagedResponseModel);

        return new Envelope<PagedOutputModel<PlanetOutputModel>>(pagedOutputModel);
    }

    /// <summary>
    /// Gets a planet by name.
    /// </summary>
    /// <param name="name">The name of planet.</param>
    [HttpGet("{name}")]
    [ProducesEnvelopeType(typeof(PlanetOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Envelope<PlanetOutputModel>>> GetByName([FromRoute] string name)
    {
        var request = new GetPlanetRequestModel { PlanetName = name };

        return await _mediator.Send(request, RequestAborted)
            .Map(response => _mapper.Map<PlanetOutputModel>(response))
            .Finally(result => CreateGetResult(result));
    }

    /// <summary>
    /// Gets a page of planet characters by name.
    /// </summary>
    /// <param name="name">The name of planet.</param>
    [HttpGet("{name}/characters")]
    [ProducesPagedEnvelopeType(typeof(PlanetCharacterOutputModel), StatusCodes.Status200OK)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<Envelope<PagedOutputModel<PlanetCharacterOutputModel>>>> GetPlanetCharactersPage([FromRoute] string name, [FromQuery] PagedInputModel input)
    {
        var request = new GetPlanetCharactersPagedRequestModel(input.PageNumber, input.PageSize)
        {
            PlanetName = name,
        };
        var pagedResponseModel = await _mediator.Send(request, RequestAborted);
        var pagedOutputModel = _mapper.Map<PagedOutputModel<PlanetCharacterOutputModel>>(pagedResponseModel);

        return new Envelope<PagedOutputModel<PlanetCharacterOutputModel>>(pagedOutputModel);
    }

    /// <summary>
    /// Creates a new planet.
    /// </summary>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create([FromBody] CreatePlanetInputModel viewModel)
    {
        var request = _mapper.Map<CreatePlanetRequestModel>(viewModel);

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateCreatedResult(result, nameof(GetByName), new { viewModel.Name }));
    }

    /// <summary>
    /// Deletes single planet.
    /// </summary>
    /// <param name="name">The name of planet.</param>
    [HttpDelete("{name}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status400BadRequest)]
    [ProducesEmptyEnvelopeType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete([FromRoute] string name)
    {
        var request = new DeletePlanetRequestModel
        {
            PlanetName = name,
        };

        return await _mediator.Send(request, RequestAborted)
            .Finally(result => CreateDeletedResult(result));
    }
}
