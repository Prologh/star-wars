﻿using Microsoft.AspNetCore.Routing;
using StarWars.Domain.Converters;

namespace StarWars.Api.Mvc.Transformers;

public class KebabCaseControllerNameTransformer : IOutboundParameterTransformer
{
    private readonly IPascalCaseToKebabCaseConverter _pascalCaseToKebabCaseConverter;

    public KebabCaseControllerNameTransformer(IPascalCaseToKebabCaseConverter pascalCaseToKebabCaseConverter)
    {
        _pascalCaseToKebabCaseConverter = pascalCaseToKebabCaseConverter;
    }

    public string TransformOutbound(object value)
    {
        return value != null
            ? _pascalCaseToKebabCaseConverter.Convert(value.ToString())
            : null;
    }
}
