﻿using Microsoft.Extensions.Options;

namespace StarWars.Api.Mvc.Options;

public class ConfigureOptionsWithDependencyContainer<TOptions, TDependency> : IConfigureOptions<TOptions>
    where TOptions : class
{
    public ConfigureOptionsWithDependencyContainer(
        TDependency dependency,
        IOptions<ConfigureOptionsWithDependency<TOptions, TDependency>> configureOptionsWithDependency)
    {
        ConfigureOptionsWithDependency = configureOptionsWithDependency.Value;
        Dependency = dependency;
    }

    public ConfigureOptionsWithDependency<TOptions, TDependency> ConfigureOptionsWithDependency { get; set; }

    public TDependency Dependency { get; set; }

    public void Configure(TOptions options)
    {
        ConfigureOptionsWithDependency.Action(options, Dependency);
    }
}
