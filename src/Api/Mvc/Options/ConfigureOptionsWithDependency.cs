﻿namespace StarWars.Api.Mvc.Options;

public class ConfigureOptionsWithDependency<TOptions, TDependency>
{
    public Action<TOptions, TDependency> Action { get; set; }
}
