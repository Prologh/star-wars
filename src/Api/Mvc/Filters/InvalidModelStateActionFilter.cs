﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using StarWars.Api.Attributes;

namespace StarWars.Api.Mvc.Filters;

public class InvalidModelStateActionFilter : IActionFilter
{
    public void OnActionExecuting(ActionExecutingContext context)
    {
        if (context.ModelState.IsValid)
        {
            return;
        }

        var applicationArea = GetApplicationArea(context);
        var errors = GetErrors(context.ModelState, applicationArea);
        var envelope = new EmptyEnvelope(errors);

        context.Result = new BadRequestObjectResult(envelope);
    }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.ModelState.IsValid)
        {
            return;
        }

        var applicationArea = GetApplicationArea(context);
        var errors = GetErrors(context.ModelState, applicationArea);
        var envelope = new EmptyEnvelope(errors);

        context.Result = new BadRequestObjectResult(envelope);
    }

    private List<Error> GetErrors(ModelStateDictionary modelState, ApplicationArea applicationArea)
    {
        return modelState
            .SelectMany(invalidProperty => invalidProperty.Value.Errors)
            .Select(error => new Error(ErrorCategory.DataNotValid, applicationArea))
            .ToList();
    }

    private ApplicationArea GetApplicationArea(ActionContext context)
    {
        if (!context.HttpContext.Items.TryGetValue(ApplicationAreaAttribute.ApplicationAreaKey, out var area))
        {
            throw new InvalidOperationException("Missing application area in HTTP context.");
        }

        return (ApplicationArea)area;
    }
}
