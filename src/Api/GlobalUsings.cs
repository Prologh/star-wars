﻿global using Ardalis.GuardClauses;
global using Microsoft.Extensions.DependencyInjection;
global using StarWars.Application.Common;
global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Threading.Tasks;
