﻿namespace StarWars.Api.InputModels.Characters;

/// <summary>
/// Represents view model for creating Star Wars character.
/// </summary>
public record CreateCharacterInputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CreateCharacterInputModel"/> class.
    /// </summary>
    public CreateCharacterInputModel()
    {
        Episodes = new List<string>();
        Friends = new List<string>();
    }

    /// <summary>
    /// Gets or sets a list of Star Wars episodes in which current character took part.
    /// </summary>
    public IList<string> Episodes { get; set; }

    /// <summary>
    /// Gets or sets a list of friends.
    /// </summary>
    public IList<string> Friends { get; set; }

    /// <summary>
    /// Gets or sets character name.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the origin planet.
    /// </summary>
    public string Planet { get; set; }
}
