﻿namespace StarWars.Api.InputModels.Characters;

public record UpdateCharacterInputModel
{
    public UpdateCharacterInputModel()
    {
        Episodes = new List<string>();
        Friends = new List<string>();
    }

    public IList<string> Episodes { get; set; }

    public IList<string> Friends { get; set; }

    public string NewName { get; set; }

    public string Planet { get; set; }
}
