﻿using StarWars.Domain.Webhooks;

namespace StarWars.Api.InputModels.Webhooks.InputModels;

public record CreateWebhookInputModel
{
    public string TargetUrl { get; set; }

    public WebhookTopic? Topic { get; set; }
}
