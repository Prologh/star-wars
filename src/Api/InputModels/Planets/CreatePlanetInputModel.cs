﻿namespace StarWars.Api.InputModels.Planets;

public class CreatePlanetInputModel
{
    public CreatePlanetInputModel()
    {
        Characters = [];
    }

    public string Name { get; set; }

    public IList<string> Characters { get; set; }
}
