﻿namespace StarWars.Api.InputModels.Pagination;

/// <summary>
/// Represents a query directed to a paged resource.
/// </summary>
public record PagedInputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PagedInputModel"/> class.
    /// </summary>
    public PagedInputModel()
    {

    }

    /// <summary>
    /// Gets or sets the page number.
    /// </summary>
    public int? PageNumber { get; set; }

    /// <summary>
    /// Gets or sets the size of page.
    /// </summary>
    public int? PageSize { get; set; }
}
