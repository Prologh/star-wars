﻿namespace StarWars.Api.InputModels.Episodes;

public class GetEpisodeInputModel
{
    public int? PartNumber { get; set; }
}
