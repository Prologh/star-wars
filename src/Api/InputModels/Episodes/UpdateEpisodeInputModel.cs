﻿namespace StarWars.Api.InputModels.Episodes;

public record UpdateEpisodeInputModel
{
    public UpdateEpisodeInputModel()
    {
        Characters = [];
    }

    public IList<string> Characters { get; set; }

    public string Name { get; set; }

    public int? NewPartNumber { get; set; }
}
