﻿namespace StarWars.Api.InputModels.Episodes;

public record CreateEpisodeInputModel
{
    public CreateEpisodeInputModel()
    {
        Characters = [];
    }

    public IList<string> Characters { get; set; }

    public string Name { get; set; }

    public int? PartNumber { get; set; }
}
