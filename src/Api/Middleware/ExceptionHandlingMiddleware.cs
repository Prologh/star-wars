﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Net.Mime;

namespace StarWars.Api.Middleware;

public class ExceptionHandlingMiddleware
{
    private readonly RequestDelegate _next;
    private readonly IOptions<MvcNewtonsoftJsonOptions> _jsonOptions;
    private readonly ILogger<ExceptionHandlingMiddleware> _logger;

    public ExceptionHandlingMiddleware(
        RequestDelegate next,
        IOptions<MvcNewtonsoftJsonOptions> jsonOptions,
        ILogger<ExceptionHandlingMiddleware> logger)
    {
        _next = Guard.Against.Null(next);
        _jsonOptions = Guard.Against.Null(jsonOptions);
        _logger = Guard.Against.Null(logger);
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next(context);

            return;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "An unhandled exception was thrown.");
        }

        var errors = new List<Error>
        {
            new Error(ErrorCategory.InternalError, ApplicationArea.Unknown),
        };
        var exceptionEnvelope = new EmptyEnvelope(errors);
        var json = JsonConvert.SerializeObject(exceptionEnvelope, _jsonOptions.Value.SerializerSettings);

        context.Response.ContentType = MediaTypeNames.Application.Json;
        context.Response.StatusCode = StatusCodes.Status500InternalServerError;

        await context.Response.WriteAsync(json);
    }
}
