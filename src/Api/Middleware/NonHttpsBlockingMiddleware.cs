﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace StarWars.Api.Middleware;

public class NonHttpsBlockingMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<NonHttpsBlockingMiddleware> _logger;

    public NonHttpsBlockingMiddleware(
        RequestDelegate next,
        ILogger<NonHttpsBlockingMiddleware> logger)
    {
        _next = Guard.Against.Null(next);
        _logger = Guard.Against.Null(logger);
    }

    public async Task InvokeAsync(HttpContext context)
    {
        if (context.Request.Scheme != Uri.UriSchemeHttps)
        {
            _logger.LogDebug("Non-HTTPS request blocked.");

            context.Response.StatusCode = StatusCodes.Status403Forbidden;
            await context.Response.CompleteAsync();

            return;
        }

        await _next(context);
    }
}
