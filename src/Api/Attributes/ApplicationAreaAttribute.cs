﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace StarWars.Api.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public sealed class ApplicationAreaAttribute : ActionFilterAttribute
{
    public const string ApplicationAreaKey = "application-area";

    public ApplicationAreaAttribute(ApplicationArea area)
    {
        Area = area;
        Order = int.MinValue;
    }

    public ApplicationArea Area { get; }

    public override void OnActionExecuting(ActionExecutingContext context)
    {
        context.HttpContext.Items[ApplicationAreaKey] = Area;
    }
}
