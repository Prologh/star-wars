﻿namespace StarWars.Api.Attributes;

public class ProducesEmptyEnvelopeTypeAttribute : ProducesEnvelopeTypeAttribute
{
    public ProducesEmptyEnvelopeTypeAttribute(int statusCode) : base(typeof(EmptyData), statusCode)
    {
    }
}
