﻿using Microsoft.AspNetCore.Mvc;

namespace StarWars.Api.Attributes;

public class ProducesEnvelopeTypeAttribute : ProducesResponseTypeAttribute
{
    public ProducesEnvelopeTypeAttribute(Type type, int statusCode) : base(statusCode)
    {
        Guard.Against.Null(type);

        Type = typeof(Envelope<>).GetGenericTypeDefinition().MakeGenericType(type);
    }
}
