﻿using Microsoft.AspNetCore.Mvc;
using StarWars.Api.OutputModels.Pagination;

namespace StarWars.Api.Attributes;

public class ProducesPagedEnvelopeType : ProducesResponseTypeAttribute
{
    public ProducesPagedEnvelopeType(Type paginatedItemType, int statusCode) : base(statusCode)
    {
        Guard.Against.Null(paginatedItemType);

        var paginatedOutputModelType = typeof(PagedOutputModel<>).GetGenericTypeDefinition().MakeGenericType(paginatedItemType);

        Type = typeof(Envelope<>).GetGenericTypeDefinition().MakeGenericType(paginatedOutputModelType);
    }
}
