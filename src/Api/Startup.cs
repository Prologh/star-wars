﻿using FluentValidation;
using FluentValidation.AspNetCore;
using Fluorite.NRate.AspNetCore.Extensions.ApplicationBuilder;
using Fluorite.NRate.AspNetCore.Extensions.DependencyInjection;
using Hangfire;
using Hangfire.AspNetCore;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Serilog;
using StarWars.Api.Extensions.Builder;
using StarWars.Api.Extensions.DependencyInjection;
using StarWars.Api.Mvc.Filters;
using StarWars.Api.Mvc.Transformers;
using StarWars.Application.Extensions.DependencyInjection;
using StarWars.Infrastructure.Extensions.DependencyInjection;
using StarWars.Infrastructure.Options;
using StarWars.Persistance;
using StarWars.Persistance.Extensions;

namespace StarWars.Api;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        services
            .AddControllers()
            .AddFilter<InvalidModelStateActionFilter>()
            .AddFilter<OperationCancelledExceptionFilter>()
            .AddRouteTokenTransformerConvention<KebabCaseControllerNameTransformer>()
            .AddControllersAsServices()
            .AddNewtonsoftJson(x => x.SerializerSettings.Converters.Add(new StringEnumConverter()));

        services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);

        services.AddDbContext<StarWarsDbContext>(builder =>
        {
            builder.UseInMemoryDatabase("InMemory");
        });

        services.AddFluentValidationAutoValidation();
        services.AddValidatorsFromAssemblyContaining<Startup>();
        services.AddDatabaseInitializer();
        services.AddKebabCaseConverter();
        services.AddAutoMapperWithProfiles();
        services.AddSwaggerGenWithDefaultOptions();
        services.AddMediatRWithRequestHandlers();
        services.AddWebhookServices();
        services.AddSerializers();
        services.AddHttpClient();
        services.AddRepositories();
        services.AddSpecifications();
        services.AddQueries();
        services.AddUnitOfWork();
        services.AddHangfireJobScheduler();
        services.AddHangfire((serviceProvider, config) =>
        {
            config.UseMemoryStorage();

            var jsonSettings = new JsonSerializerSettings();
            jsonSettings.Converters.Add(new StringEnumConverter());
            jsonSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            config.UseSerializerSettings(jsonSettings);
            var serviceScopeFactory = serviceProvider.GetRequiredService<IServiceScopeFactory>();
            config.UseActivator(new AspNetCoreJobActivator(serviceScopeFactory));
        });
        services.AddHangfireServer();
        services.AddNRate(Configuration.GetSection("NRate"));

        services.Configure<CryptographyOptions>(Configuration.GetSection("Cryptography"));
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandling();
            app.UseHsts();
        }

        app.UseNonHttpsBlocking();
        app.UseSwagger();
        app.UseSwaggerUI();
        app.UseHangfireDashboard(options: new DashboardOptions
        {
            DisplayStorageConnectionString = false,
        });
        app.UseSerilogRequestLogging();
        app.UseNRate();
        app.UseRouting();
        app.Map200ToBasePath();
        app.UseEndpoints(endpoints => endpoints.MapControllers());
    }
}
