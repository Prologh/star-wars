﻿namespace StarWars.Api.OutputModels.Episodes;

public class EpisodeOutputModel
{
    public string Name { get; set; }

    public int PartNumber { get; set; }
}
