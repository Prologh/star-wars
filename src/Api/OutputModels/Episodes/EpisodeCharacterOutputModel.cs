﻿namespace StarWars.Api.OutputModels.Episodes;

public record EpisodeCharacterOutputModel
{
    public string Name { get; set; }

    public string OriginPlanet { get; set; }
}
