﻿namespace StarWars.Api.OutputModels.Characters;

public record CharacterOutputModel
{
    public CharacterOutputModel()
    {
        Episodes = new List<string>();
        Friends = new List<string>();
    }

    public IList<string> Episodes { get; set; }

    public IList<string> Friends { get; set; }

    public string Name { get; set; }

    public string OriginPlanet { get; set; }
}
