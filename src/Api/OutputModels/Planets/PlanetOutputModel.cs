﻿namespace StarWars.Api.OutputModels.Planets;

public class PlanetOutputModel
{
    public string Name { get; set; }
}
