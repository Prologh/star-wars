﻿namespace StarWars.Api.OutputModels.Planets;

public class PlanetCharacterOutputModel
{
    public string Name { get; set; }
}
