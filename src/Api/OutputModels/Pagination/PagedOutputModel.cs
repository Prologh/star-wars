﻿namespace StarWars.Api.OutputModels.Pagination;

/// <summary>
/// Represents a query directed to a paged resource.
/// </summary>
public abstract record PagedOutputModel
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PagedOutputModel"/> class.
    /// </summary>
    protected PagedOutputModel(int pageNumber, int pageSize, long totalPages, long totalSize)
    {
        PageNumber = pageNumber;
        PageSize = pageSize;
        TotalPages = totalPages;
        TotalSize = totalSize;
    }

    /// <summary>
    /// Gets the page number.
    /// </summary>
    public int PageNumber { get; }

    /// <summary>
    /// Gets the size of page.
    /// </summary>
    public int PageSize { get; }

    /// <summary>
    ///  Gets the total amount of pages.
    /// </summary>
    public long TotalPages { get; }

    /// <summary>
    /// Gets the total size of all items on all pages.
    /// </summary>
    public long TotalSize { get; }
}
