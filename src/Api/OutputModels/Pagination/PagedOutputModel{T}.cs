﻿namespace StarWars.Api.OutputModels.Pagination;

public record PagedOutputModel<T> : PagedOutputModel
{
    public PagedOutputModel(
        int pageNumber,
        int pageSize,
        long totalPages,
        long totalSize,
        IReadOnlyList<T> items)
        : base(pageNumber, pageSize, totalPages, totalSize)
    {
        Items = Guard.Against.Null(items);
    }

    public IReadOnlyList<T> Items { get; }
}
