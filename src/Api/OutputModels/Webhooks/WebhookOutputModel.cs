﻿using StarWars.Domain.Webhooks;

namespace StarWars.Api.OutputModels.Webhooks;

public record WebhookOutputModel
{
    public DateTime Created { get; set; }

    public Guid ExternalId { get; set; }

    public string TargetUrl { get; set; }

    public WebhookTopic Topic { get; set; }
}
