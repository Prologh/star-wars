﻿using StarWars.Domain.Webhooks;

namespace StarWars.Api.OutputModels.Webhooks;

public record WebhookCallOutputModel
{
    public Guid ExternalId { get; set; }

    public long DurationInMiliseconds { get; set; }

    public bool HasFailedResponse { get; set; }

    public bool HasTimedOut { get; set; }

    public long? ResponseLength { get; set; }

    public int? ResponseStatusCode { get; set; }

    public string Response { get; set; }

    public DateTime SentOn { get; set; }

    public string TargetUrl { get; set; }

    public WebhookTopic Topic { get; set; }
}
