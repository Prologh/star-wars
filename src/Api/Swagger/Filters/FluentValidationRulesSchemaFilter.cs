﻿using FluentValidation;
using FluentValidation.Validators;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using StarWars.Application.FluentValidation;
using StarWars.Infrastructure.Extensions;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections;
using System.Reflection;

namespace StarWars.Api.Swagger.Filters;

public class FluentValidationRulesSchemaFilter : ISchemaFilter
{
    private const string EmailFormat = "email";

    private readonly IServiceProvider serviceProvider;

    public FluentValidationRulesSchemaFilter(IServiceProvider serviceProvider)
    {
        this.serviceProvider = serviceProvider;
    }

    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        var model = schema;
        var isEmptySchema = !model.Properties.Any();
        if (isEmptySchema)
        {
            return;
        }

        var modelType = context.Type;
        using var scope = serviceProvider.CreateScope();
        var logger = scope.ServiceProvider.GetRequiredService<ILogger<FluentValidationRulesSchemaFilter>>();
        var validatorTypeDefinition = typeof(IValidator<>).GetGenericTypeDefinition();
        var validatorType = validatorTypeDefinition.MakeGenericType(modelType);
        if (scope.ServiceProvider.GetService(validatorType) is not IValidator validator)
        {
            return;
        }

        var validatorDescriptor = validator.CreateDescriptor();
        var properties = modelType.GetProperties(BindingFlags.Instance | BindingFlags.Public);
        foreach (var propertyInfo in properties)
        {
            foreach (var (propertyValidator, _) in validatorDescriptor.GetValidatorsForMember(propertyInfo.Name))
            {
                var propertyNameLower = propertyInfo.Name.FirstToLower();

                if (!model.Properties.TryGetValue(propertyNameLower, out var propertySchema))
                {
                    logger.LogWarning(
                        "Skipping adding validation rules to Swagger because no parent schema was found " +
                        "when looking for {propertyNameLower} of {modelType.Name}.",
                        propertyNameLower,
                        modelType.Name);
                    continue;
                }

                switch (propertyValidator)
                {
                    case INotNullValidator:
                        model.Required.Add(propertyNameLower);
                        break;
                    case INotEmptyValidator:
                        ApplyNotEmptyValidator(model, propertySchema, propertyInfo, propertyNameLower);
                        break;
                    case ILengthValidator lengthValidator:
                        ApplyLengthValidator(propertySchema, lengthValidator);
                        break;
                    case IComparisonValidator comparisonValidator:
                        ApplyComparisonValidator(propertySchema, comparisonValidator);
                        break;
                    case IRegularExpressionValidator regularExpressionValidator:
                        propertySchema.Pattern = regularExpressionValidator.Expression;
                        break;
                    case IEmailValidator:
                        propertySchema.Format = EmailFormat;
                        break;
                    case ICollectionCountValidator collectionCountValidator:
                        ApplyMaxElementsValidator(propertySchema, collectionCountValidator);
                        break;
                }

                // Add more validation properties here;
            }
        }
    }

    private static void ApplyNotEmptyValidator(OpenApiSchema modelSchema, OpenApiSchema propertySchema, PropertyInfo propertyInfo, string propertyName)
    {
        modelSchema.Required.Add(propertyName);

        if (propertyInfo.PropertyType == typeof(string))
        {
            propertySchema.MinLength = 1;
        }
        else if (propertyInfo.PropertyType.IsAssignableTo(typeof(IEnumerable)))
        {
            propertySchema.MinItems = 1;
        }
    }

    private static void ApplyLengthValidator(OpenApiSchema propertySchema, ILengthValidator lengthValidator)
    {
        if (lengthValidator.Max > 0)
        {
            propertySchema.MaxLength = lengthValidator.Max;
        }

        if (lengthValidator.Min > 0)
        {
            propertySchema.MinLength = lengthValidator.Min;
        }
    }

    private static void ApplyMaxElementsValidator(OpenApiSchema propertySchema, ICollectionCountValidator collectionCountValidator)
    {
        if (propertySchema.Type != "array")
        {
            return;
        }

        if (collectionCountValidator.Max.HasValue)
        {
            propertySchema.MaxItems = collectionCountValidator.Max.Value;
        }

        if (collectionCountValidator.Min.HasValue)
        {
            propertySchema.MinItems = collectionCountValidator.Min.Value;
        }
    }

    private static void ApplyComparisonValidator(OpenApiSchema propertySchema, IComparisonValidator comparisonValidator)
    {
        if (comparisonValidator.ValueToCompare is null)
        {
            return;
        }

        var valueToCompareType = comparisonValidator.ValueToCompare.GetType();
        var valueType = Nullable.GetUnderlyingType(valueToCompareType) ?? valueToCompareType;

        // Support only numeric type comparison as other are not that easy to portray in Swagger.
        if (!valueType.IsEnum && valueType.IsValueType && IsNumericType(valueType))
        {
            var value = (decimal)Convert.ChangeType(comparisonValidator.ValueToCompare, typeof(decimal));

            switch (comparisonValidator.Comparison)
            {
                case Comparison.LessThan:
                    propertySchema.ExclusiveMaximum = true;
                    propertySchema.Maximum = value;
                    break;
                case Comparison.GreaterThan:
                    propertySchema.ExclusiveMinimum = true;
                    propertySchema.Minimum = value;
                    break;
                case Comparison.GreaterThanOrEqual:
                    propertySchema.ExclusiveMinimum = false;
                    propertySchema.Minimum = value;
                    break;
                case Comparison.LessThanOrEqual:
                    propertySchema.ExclusiveMaximum = false;
                    propertySchema.Maximum = value;
                    break;
            }
        }
    }

    private static bool IsNumericType(Type type) => Type.GetTypeCode(Nullable.GetUnderlyingType(type) ?? type) switch
    {
        TypeCode.Byte
        or TypeCode.SByte
        or TypeCode.UInt16
        or TypeCode.UInt32
        or TypeCode.UInt64
        or TypeCode.Int16
        or TypeCode.Int32
        or TypeCode.Int64
        or TypeCode.Decimal
        or TypeCode.Double
        or TypeCode.Single => true,
        _ => false,
    };
}
