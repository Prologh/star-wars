﻿namespace StarWars.Application.Episodes.ResponseModels;

public record EpisodeCharacterResponseModel
{
    public string Name { get; set; }

    public string OriginPlanet { get; set; }
}
