﻿namespace StarWars.Application.Episodes.ResponseModels;

public record EpisodeResponseModel
{
    public string Name { get; set; }

    public int PartNumber { get; set; }
}
