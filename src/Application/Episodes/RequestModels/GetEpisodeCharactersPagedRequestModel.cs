﻿using StarWars.Application.Episodes.ResponseModels;
using StarWars.Application.Pagination.RequestModels;

namespace StarWars.Application.Episodes.RequestModels;

public record GetEpisodeCharactersPagedRequestModel : PagedRequestModel<EpisodeCharacterResponseModel>
{
    public GetEpisodeCharactersPagedRequestModel(int? pageNumber, int? pageSize)
        : base(pageNumber, pageSize)
    {
    }

    public int PartNumber { get; set; }
}
