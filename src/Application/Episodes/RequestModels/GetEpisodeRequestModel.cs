﻿using StarWars.Application.Common.Requests;
using StarWars.Application.Episodes.ResponseModels;

namespace StarWars.Application.Episodes.RequestModels;

public class GetEpisodeRequestModel : IGetRequest<EpisodeResponseModel>
{
    public int PartNumber { get; set; }
}
