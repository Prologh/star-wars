﻿using StarWars.Application.Common.Requests;

namespace StarWars.Application.Episodes.RequestModels;

public record DeleteEpisodeRequestModel : IDeleteRequest
{
    public int PartNumber { get; set; }
}
