﻿using StarWars.Application.Common.Requests;

namespace StarWars.Application.Episodes.RequestModels;

public record UpdateEpisodeRequestModel : IUpdateRequest
{
    public UpdateEpisodeRequestModel()
    {
        Characters = new List<string>();
    }

    public IList<string> Characters { get; set; }

    public string Name { get; set; }

    public int? NewPartNumber { get; set; }

    public int PartNumber { get; set; }
}
