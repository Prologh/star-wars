﻿using StarWars.Application.Common.Queries;
using StarWars.Application.Episodes.RequestModels;
using StarWars.Application.Episodes.ResponseModels;

namespace StarWars.Application.Episodes.Queries;

public interface IGetPagedEpisodeCharactersQuery : IPagedQuery<GetEpisodeCharactersPagedRequestModel, EpisodeCharacterResponseModel>
{
}
