﻿using StarWars.Application.Common.Queries;
using StarWars.Application.Episodes.ResponseModels;

namespace StarWars.Application.Episodes.Queries;

public interface IGetEpisodeQuery : IMaybeQuery<int, EpisodeResponseModel>
{
}
