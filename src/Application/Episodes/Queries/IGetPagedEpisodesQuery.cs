﻿using StarWars.Application.Common.Queries;
using StarWars.Application.Episodes.ResponseModels;
using StarWars.Application.Pagination.RequestModels;

namespace StarWars.Application.Episodes.Queries;

public interface IGetPagedEpisodesQuery : IPagedQuery<PagedRequestModel, EpisodeResponseModel>
{
}
