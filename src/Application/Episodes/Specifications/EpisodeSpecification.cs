﻿using StarWars.Application.Extensions;
using StarWars.Application.Specifications;
using StarWars.Domain.Episodes;

namespace StarWars.Application.Episodes.Specifications;

public class EpisodeSpecification : BaseSpecification<EpisodeEntity>
{
    public EpisodeSpecification ByPartNumber(int partNumber)
    {
        AddCriteria(x => x.PartNumber == partNumber);

        return this;
    }

    public EpisodeSpecification ByExcludingId(long id)
    {
        AddCriteria(x => x.Id != id);

        return this;
    }

    public EpisodeSpecification WithCharacters()
    {
        AddInclude(x => x.CharacterEpisodes)
            .ThenInclude(x => x.Character);

        return this;
    }

    public EpisodeSpecification ByName(string episodeName)
    {
        Guard.Against.NullOrWhiteSpace(episodeName);

        AddCriteria(x => x.Name == episodeName);

        return this;
    }
}
