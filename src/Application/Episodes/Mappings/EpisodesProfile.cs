﻿using AutoMapper;
using StarWars.Application.Episodes.ResponseModels;
using StarWars.Domain.Episodes;

namespace StarWars.Application.Episodes.Mappings;

public class EpisodesProfile : Profile
{
    public EpisodesProfile()
    {
        // Request model ==> Entity

        // Entity ==> Response model
        CreateMap<EpisodeEntity, EpisodeResponseModel>();
    }
}
