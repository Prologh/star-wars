﻿using AutoMapper;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Episodes.Queries;
using StarWars.Application.Episodes.RequestModels;
using StarWars.Application.Episodes.ResponseModels;
using StarWars.Application.Pagination.ResponseModels;
using System.Threading;

namespace StarWars.Application.Episodes.Handlers;

public class GetPagedEpisodeCharactersRequestHandler : IGetPagedRequestHandler<GetEpisodeCharactersPagedRequestModel, EpisodeCharacterResponseModel>
{
    private readonly IMapper _mapper;
    private readonly IGetPagedEpisodeCharactersQuery _getPagedEpisodeCharactersQuery;

    public GetPagedEpisodeCharactersRequestHandler(
        IMapper mapper,
        IGetPagedEpisodeCharactersQuery getPagedEpisodeCharactersQuery)
    {
        _mapper = Guard.Against.Null(mapper);
        _getPagedEpisodeCharactersQuery = Guard.Against.Null(getPagedEpisodeCharactersQuery);
    }

    public async Task<PagedResponseModel<EpisodeCharacterResponseModel>> Handle(
        GetEpisodeCharactersPagedRequestModel request,
        CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var pagedResult = await _getPagedEpisodeCharactersQuery.ExecuteAsync(request, cancellationToken);

        return _mapper.Map<PagedResponseModel<EpisodeCharacterResponseModel>>(pagedResult);
    }
}
