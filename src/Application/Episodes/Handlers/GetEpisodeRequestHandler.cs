﻿using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Episodes.Queries;
using StarWars.Application.Episodes.RequestModels;
using StarWars.Application.Episodes.ResponseModels;
using System.Threading;

namespace StarWars.Application.Episodes.Handlers;

public class GetEpisodeRequestHandler : IGetRequestHandler<GetEpisodeRequestModel, EpisodeResponseModel>
{
    private readonly IGetEpisodeQuery _getEpisodeQuery;

    public GetEpisodeRequestHandler(IGetEpisodeQuery getEpisodeQuery)
    {
        _getEpisodeQuery = getEpisodeQuery;
    }

    public async Task<Result<EpisodeResponseModel, Error>> Handle(
        GetEpisodeRequestModel request,
        CancellationToken cancellationToken)
    {
        var episode = await _getEpisodeQuery.ExecuteAsync(request.PartNumber, cancellationToken);

        return episode
            .ToResult(new Error(ErrorCategory.DataNotFound, ApplicationArea.Episodes));
    }
}
