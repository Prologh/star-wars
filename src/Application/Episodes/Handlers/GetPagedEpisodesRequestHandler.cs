﻿using AutoMapper;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Episodes.Queries;
using StarWars.Application.Episodes.ResponseModels;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Pagination.ResponseModels;
using System.Threading;

namespace StarWars.Application.Episodes.Handlers;

public class GetPagedEpisodesRequestHandler : IGetPagedRequestHandler<PagedRequestModel<EpisodeResponseModel>, EpisodeResponseModel>
{
    private readonly IMapper _mapper;
    private readonly IGetPagedEpisodesQuery _getPagedEpisodesQuery;

    public GetPagedEpisodesRequestHandler(
        IMapper mapper,
        IGetPagedEpisodesQuery getPagedEpisodesQuery)
    {
        _mapper = Guard.Against.Null(mapper);
        _getPagedEpisodesQuery = Guard.Against.Null(getPagedEpisodesQuery);
    }

    public async Task<PagedResponseModel<EpisodeResponseModel>> Handle(
        PagedRequestModel<EpisodeResponseModel> request,
        CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var pagedResult = await _getPagedEpisodesQuery.ExecuteAsync(request, cancellationToken);

        return _mapper.Map<PagedResponseModel<EpisodeResponseModel>>(pagedResult);
    }
}
