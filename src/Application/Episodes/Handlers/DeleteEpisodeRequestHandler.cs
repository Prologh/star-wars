﻿using MediatR;
using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Episodes.RequestModels;
using StarWars.Application.Episodes.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Episodes;
using System.Threading;

namespace StarWars.Application.Episodes.Handlers;

public class DeleteEpisodeRequestHandler : IDeleteRequestHandler<DeleteEpisodeRequestModel>
{
    private readonly IRepository<EpisodeEntity> _episodeRepository;
    private readonly IUnitOfWork _unitOfWork;

    public DeleteEpisodeRequestHandler(
        IRepository<EpisodeEntity> episodeRepository,
        IUnitOfWork unitOfWork)
    {
        _episodeRepository = episodeRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<UnitResult<Error>> Handle(DeleteEpisodeRequestModel request, CancellationToken cancellationToken)
    {
        var partNumberCheckSpecification = new EpisodeSpecification()
            .ByPartNumber(request.PartNumber);
        var episode = await _episodeRepository.FirstOrDefaultAsync(partNumberCheckSpecification, cancellationToken);
        if (episode == null)
        {
            return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Episodes));
        }

        _unitOfWork.Remove(episode);
        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return UnitResult.Success<Error>();
    }
}
