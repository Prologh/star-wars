﻿using StarWars.Application.Characters.Specifications;
using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Episodes.RequestModels;
using StarWars.Application.Episodes.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Characters;
using StarWars.Domain.Episodes;
using System.Threading;

namespace StarWars.Application.Episodes.Handlers;

public class UpdateEpisodeRequestHandler : IUpdateRequestHandler<UpdateEpisodeRequestModel>
{
    private readonly IRepository<CharacterEntity> _characterRepository;
    private readonly IRepository<EpisodeEntity> _episodeRepository;
    private readonly IUnitOfWork _unitOfWork;

    public UpdateEpisodeRequestHandler(
        IRepository<CharacterEntity> characterRepository,
        IRepository<EpisodeEntity> episodeRepository,
        IUnitOfWork unitOfWork)
    {
        _characterRepository = Guard.Against.Null(characterRepository);
        _episodeRepository = Guard.Against.Null(episodeRepository);
        _unitOfWork = Guard.Against.Null(unitOfWork);
    }

    public async Task<UnitResult<Error>> Handle(UpdateEpisodeRequestModel request, CancellationToken cancellationToken)
    {
        var partNumberCheckSpecification = new EpisodeSpecification()
            .ByPartNumber(request.PartNumber)
            .WithCharacters();
        var episode = await _episodeRepository.SingleOrDefaultAsync(partNumberCheckSpecification, cancellationToken);
        if (episode is null)
        {
            return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Episodes));
        }

        episode.Name = request.Name;

        if (request.NewPartNumber.HasValue)
        {
            var specification = new EpisodeSpecification()
                .ByPartNumber(request.PartNumber)
                .ByExcludingId(episode.Id);
            var partNumberAlreadyOccupied = await _episodeRepository.AnyAsync(specification, cancellationToken);
            if (partNumberAlreadyOccupied)
            {
                return UnitResult.Failure(new Error(ErrorCategory.DataNotValid, ApplicationArea.Episodes, ErrorCodes.EpisodePartNumberAlreadyUsed));
            }

            episode.PartNumber = request.NewPartNumber.Value;
        }

        var existingCharacters = episode.CharacterEpisodes.Select(x => x.Character.Name).ToList();
        var shouldUpdateCharacters = !existingCharacters.SequenceEqual(request.Characters);
        if (shouldUpdateCharacters)
        {
            var charactersToRemove = existingCharacters.Except(request.Characters).ToList();

            foreach (var characterName in charactersToRemove)
            {
                var characterToRemove = episode.CharacterEpisodes.Single(x => x.Character.Name == characterName);
                episode.CharacterEpisodes.Remove(characterToRemove);
            }

            var charactersToAdd = request.Characters.Except(existingCharacters).ToList();

            foreach (var characterName in charactersToAdd)
            {
                var specification = new CharacterSpecification()
                    .ByName(characterName);
                var character = await _characterRepository.FirstOrDefaultAsync(specification, cancellationToken);
                if (character is null)
                {
                    return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Characters));
                }

                episode.CharacterEpisodes.Add(new CharacterEpisodeEntity
                {
                    Character = character,
                    Episode = episode,
                });
            }
        }

        _unitOfWork.Update(episode);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return UnitResult.Success<Error>();
    }
}
