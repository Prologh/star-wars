﻿using StarWars.Application.Characters.Specifications;
using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Episodes.RequestModels;
using StarWars.Application.Episodes.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Characters;
using StarWars.Domain.Episodes;
using System.Threading;

namespace StarWars.Application.Episodes.Handlers;

public class CreateEpisodeRequestHandler : ICreateRequestHandler<CreateEpisodeRequestModel>
{
    private readonly IRepository<CharacterEntity> _characterRepository;
    private readonly IRepository<EpisodeEntity> _episodeRepository;
    private readonly IUnitOfWork _unitOfWork;

    public CreateEpisodeRequestHandler(
        IRepository<CharacterEntity> characterRepository,
        IRepository<EpisodeEntity> episodeRepository,
        IUnitOfWork unitOfWork)
    {
        _characterRepository = Guard.Against.Null(characterRepository);
        _episodeRepository = Guard.Against.Null(episodeRepository);
        _unitOfWork = Guard.Against.Null(unitOfWork);
    }

    public async Task<UnitResult<Error>> Handle(CreateEpisodeRequestModel request, CancellationToken cancellationToken)
    {
        var partNumberCheckSpecification = new EpisodeSpecification()
            .ByPartNumber(request.PartNumber);
        var isPartNumberTaken = await _episodeRepository.AnyAsync(partNumberCheckSpecification, cancellationToken);
        if (isPartNumberTaken)
        {
            return UnitResult.Failure(new Error(ErrorCategory.DataNotValid, ApplicationArea.Episodes, ErrorCodes.EpisodePartNumberAlreadyUsed));
        }

        var entity = new EpisodeEntity
        {
            PartNumber = request.PartNumber,
            Name = request.Name,
        };

        foreach (var characterName in request.Characters)
        {
            var specification = new CharacterSpecification()
                .ByName(characterName);
            var character = await _characterRepository.FirstOrDefaultAsync(specification, cancellationToken);
            if (character is null)
            {
                return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Characters));
            }

            var characterEpisode = new CharacterEpisodeEntity
            {
                Character = character,
                Episode = entity,
            };
            entity.CharacterEpisodes.Add(characterEpisode);
        }

        _unitOfWork.Add(entity);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return UnitResult.Success<Error>();
    }
}
