﻿using StarWars.Domain.Abstractions;
using System.Threading;

namespace StarWars.Application;

public interface IUnitOfWork
{
    TEntity Add<TEntity>(TEntity entity)
        where TEntity : class, IEntity;

    IList<TEntity> AddRange<TEntity>(IList<TEntity> entities)
        where TEntity : class, IEntity;

    TEntity Update<TEntity>(TEntity entity)
        where TEntity : class, IEntity;

    IList<TEntity> UpdateRange<TEntity>(IList<TEntity> entities)
        where TEntity : class, IEntity;

    TEntity Remove<TEntity>(TEntity entity)
        where TEntity : class, IEntity;

    IList<TEntity> RemoveRange<TEntity>(IList<TEntity> entities)
        where TEntity : class, IEntity;

    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}
