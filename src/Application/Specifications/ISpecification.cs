﻿using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Sorting;
using StarWars.Domain.Abstractions;
using System.Linq.Expressions;

namespace StarWars.Application.Specifications;

public interface ISpecification<TEntity>
    where TEntity : class, IEntity
{
    Expression<Func<TEntity, bool>> Criteria { get; }

    IReadOnlySet<string> Includes { get; }

    bool IsSortingDescending { get; }

    PagedRequestModel Pagination { get; }

    Expression<Func<TEntity, object>> SortingExpression { get; }

    string SortingName { get; }

    SortingSourceType? SortingSourceType { get; }
}
