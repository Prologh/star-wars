﻿using StarWars.Application.Extensions.Expressions;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Sorting;
using StarWars.Domain;
using StarWars.Domain.Abstractions;
using System.Linq.Expressions;

namespace StarWars.Application.Specifications;

public abstract class BaseSpecification<TEntity> : ISpecification<TEntity>, IIncludeStore
    where TEntity : class, IEntity
{
    private readonly HashSet<string> _includes;

    protected BaseSpecification()
    {
        _includes = [];
        IsSortingDescending = Constants.Sorting.IsDefaultDescending;
    }

    public Expression<Func<TEntity, bool>> Criteria { get; private set; }

    public IReadOnlySet<string> Includes => _includes;

    public PagedRequestModel Pagination { get; private set; }

    public bool IsSortingDescending { get; private set; }

    public Expression<Func<TEntity, object>> SortingExpression { get; private set; }

    public string SortingName { get; private set; }

    public SortingSourceType? SortingSourceType { get; private set; }

    protected void AddCriteria(Expression<Func<TEntity, bool>> criteria)
    {
        Guard.Against.Null(criteria);

        if (Criteria is null)
        {
            Criteria = criteria;
        }
        else
        {
            Criteria = Criteria.AndAlso(criteria);
        }
    }

    IReadOnlyList<string> IIncludeStore.AddInclude<TOther, TProperty>(
        Expression<Func<TOther, TProperty>> expresssion,
        IReadOnlyList<string> propertyNames)
    {
        var newPropertyName = expresssion.GetPropertyName();
        var extendedPropertyNames = propertyNames.Append(newPropertyName);
        var navigationPath = string.Join(".", extendedPropertyNames);
        _includes.Add(navigationPath);

        return extendedPropertyNames.ToList().AsReadOnly();
    }

    protected IIncludeBuilder<TProperty> AddInclude<TProperty>(Expression<Func<TEntity, TProperty>> includeExpression)
    {
        var propertyName = includeExpression.GetPropertyName();
        _includes.Add(propertyName);

        return new IncludeBuilder<TProperty>(this, [propertyName]);
    }

    protected void SetPagination(PagedRequestModel pagination)
    {
        Guard.Against.Null(pagination);

        Pagination = pagination;
    }

    protected void SetSorting(string name, bool isDescending)
    {
        Guard.Against.NullOrWhiteSpace(name);

        SortingName = name;
        SortingExpression = null;
        IsSortingDescending = isDescending;
        SortingSourceType = Sorting.SortingSourceType.StringName;
    }

    protected void SetSorting(Expression<Func<TEntity, object>> expression, bool isDescending)
    {
        Guard.Against.Null(expression);

        SortingExpression = expression;
        SortingName = null;
        IsSortingDescending = isDescending;
        SortingSourceType = Sorting.SortingSourceType.Expression;
    }
}
