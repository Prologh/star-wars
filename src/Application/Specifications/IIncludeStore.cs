﻿using System.Linq.Expressions;

namespace StarWars.Application.Specifications;

public interface IIncludeStore
{
    IReadOnlyList<string> AddInclude<TOther, TProperty>(
        Expression<Func<TOther, TProperty>> expresssion,
        IReadOnlyList<string> propertyNames);
}
