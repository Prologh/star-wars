﻿namespace StarWars.Application.Specifications
{
    public class IncludeBuilder<TProperty> : IIncludeBuilder<TProperty>
    {
        public IncludeBuilder(IIncludeStore includeStore, IReadOnlyList<string> propertyNames)
        {
            Guard.Against.Null(includeStore);
            Guard.Against.Null(propertyNames);

            IncludeStore = includeStore;
            PropertyNames = propertyNames;
        }

        public IIncludeStore IncludeStore { get; }

        public IReadOnlyList<string> PropertyNames { get; }
    }
}
