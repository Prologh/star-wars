﻿namespace StarWars.Application.Specifications;

public interface IIncludeBuilder<out TProperty>
{
    IIncludeStore IncludeStore { get; }

    IReadOnlyList<string> PropertyNames { get; }
}
