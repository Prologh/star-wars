﻿using StarWars.Domain.Abstractions;

namespace StarWars.Application.Specifications;

public interface ISpecificationEvaluator<TEntity>
    where TEntity : class, IEntity
{
    IQueryable<TEntity> Evaluate(IQueryable<TEntity> query, ISpecification<TEntity> specification);
}
