﻿using System.Linq.Expressions;

namespace StarWars.Application.Scheduling;

public interface IJobScheduler
{
    string Schedule<TService>(Expression<Func<TService, Task>> expression)
        where TService : class;
}
