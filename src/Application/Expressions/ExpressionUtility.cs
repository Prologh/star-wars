﻿using System.Linq.Expressions;

namespace StarWars.Application.Expressions;

public class ExpressionUtility : IExpressionUtility
{
    public MemberExpression GetProperty(Expression expression, string name, bool nestedProperty = true)
    {
        if (!nestedProperty)
        {
            return Expression.Property(expression, name);
        }

        var path = name.Split('.');
        var result = Expression.Property(expression, path[0]);

        return path.Skip(1).Aggregate(result, Expression.Property);
    }

    public Expression<Func<T, TProperty>> LambdaFunc<T, TProperty>(Expression expression, params ParameterExpression[] parameters)
    {
        return Expression.Lambda<Func<T, TProperty>>(expression, parameters);
    }

    public bool TryProperty(Expression expression, string fullName, bool nestedProperty, out MemberExpression memberExpression)
    {
        memberExpression = null;

        try
        {
            memberExpression = GetProperty(expression, fullName, nestedProperty);

            return true;
        }
        catch (ArgumentException)
        {
            return false;
        }
    }
}
