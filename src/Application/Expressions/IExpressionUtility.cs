﻿using System.Linq.Expressions;

namespace StarWars.Application.Expressions;

public interface IExpressionUtility
{
    MemberExpression GetProperty(Expression expression, string name, bool nestedProperty = true);

    Expression<Func<T, TProperty>> LambdaFunc<T, TProperty>(Expression expression, params ParameterExpression[] parameters);

    bool TryProperty(Expression expression, string fullName, bool nestedProperty, out MemberExpression memberExpression);
}
