﻿using StarWars.Application.Common.Queries;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;

namespace StarWars.Application.Webhooks.Queries;

public interface IGetPagedWebhookCallsQuery : IPagedQuery<GetWebhookCallsPagedRequestModel, WebhookCallPagedResponseModel>
{
}
