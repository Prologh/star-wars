﻿using StarWars.Application.Common.Queries;
using StarWars.Application.Webhooks.ResponseModels;

namespace StarWars.Application.Webhooks.Queries;

public interface IGetWebhookQuery : IMaybeQuery<Guid, WebhookResponseModel>
{
}
