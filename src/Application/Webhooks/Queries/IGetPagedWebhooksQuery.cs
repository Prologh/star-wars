﻿using StarWars.Application.Common.Queries;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;

namespace StarWars.Application.Webhooks.Queries;

public interface IGetPagedWebhooksQuery : IPagedQuery<PagedRequestModel, WebhookResponseModel>
{
}
