﻿using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;

namespace StarWars.Application.Webhooks.RequestModels;

public record GetWebhookCallsPagedRequestModel : PagedRequestModel<WebhookCallPagedResponseModel>
{
    public GetWebhookCallsPagedRequestModel(Guid externalId, int? pageNumber, int? pageSize)
        : base(pageNumber, pageSize)
    {
        ExternalId = externalId;
    }

    public Guid ExternalId { get; }
}
