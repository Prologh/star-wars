﻿using StarWars.Application.Common.Requests;

namespace StarWars.Application.Webhooks.RequestModels;

public record DeleteWebhookRequestModel : IDeleteRequest
{
    public Guid ExternalId { get; set; }
}
