﻿using StarWars.Application.Common.Requests;
using StarWars.Application.Webhooks.ResponseModels;

namespace StarWars.Application.Webhooks.RequestModels;

public record GetWebhookRequestModel : IGetRequest<WebhookResponseModel>
{
    public Guid ExternalId { get; set; }
}
