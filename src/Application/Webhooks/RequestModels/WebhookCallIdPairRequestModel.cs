﻿namespace StarWars.Application.Webhooks.RequestModels;

public record WebhookCallIdPairRequestModel
{
    public Guid WebhookExternalId { get; init; }

    public Guid CallExternalId { get; init; }
}
