﻿using StarWars.Application.Common.Requests;
using StarWars.Domain.Webhooks;

namespace StarWars.Application.Webhooks.RequestModels;

public record CreateWebhookRequestModel : ICreateRequest<Guid>
{
    public string TargetUrl { get; set; }

    public WebhookTopic Topic { get; set; }
}
