﻿using StarWars.Application.Common.Requests;
using StarWars.Application.Webhooks.ResponseModels;

namespace StarWars.Application.Webhooks.RequestModels;

public record GetWebhookCallRequestModel : IGetRequest<WebhookCallResponseModel>
{
    public Guid WebhookExternalId { get; set; }

    public Guid CallExternalId { get; set; }
}
