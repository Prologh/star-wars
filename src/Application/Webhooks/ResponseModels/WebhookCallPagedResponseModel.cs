﻿using StarWars.Domain.Webhooks;

namespace StarWars.Application.Webhooks.ResponseModels;

public class WebhookCallPagedResponseModel
{
    public long DurationInMiliseconds { get; set; }

    public bool HasFailedResponse { get; set; }

    public bool HasTimedOut { get; set; }

    public int? ResponseStatusCode { get; set; }

    public DateTime SentOn { get; set; }

    public string TargetUrl { get; set; }

    public WebhookTopic Topic { get; set; }
}
