﻿using StarWars.Domain.Webhooks;

namespace StarWars.Application.Webhooks.ResponseModels;

public record WebhookResponseModel
{
    public DateTime Created { get; set; }

    public Guid ExternalId { get; set; }

    public string TargetUrl { get; set; }

    public WebhookTopic Topic { get; set; }
}
