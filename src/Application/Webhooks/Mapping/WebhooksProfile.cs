﻿using AutoMapper;
using StarWars.Application.Webhooks.ResponseModels;
using StarWars.Domain.Webhooks;

namespace StarWars.Application.Webhooks.Mapping;

public class WebhooksProfile : Profile
{
    public WebhooksProfile()
    {
        // Request model ==> Entity

        // Entity ==> Response model
        CreateMap<WebhookEntity, WebhookResponseModel>();
        CreateMap<WebhookCallEntity, WebhookCallResponseModel>();
        CreateMap<WebhookCallEntity, WebhookCallPagedResponseModel>();
    }
}
