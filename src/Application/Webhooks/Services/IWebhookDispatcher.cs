﻿using StarWars.Domain.Characters;
using System.Threading;

namespace StarWars.Application.Webhooks.Services;

public interface IWebhookDispatcher
{
    Task DispatchCharacterCreated(CharacterEntity character, CancellationToken cancellationToken);

    Task DispatchCharacterUpdated(CharacterEntity character, CancellationToken cancellationToken);
}
