﻿using System.Linq.Expressions;

namespace StarWars.Application.Webhooks.Services;

public interface IBackgroundJobWebhookDispatcher
{
    string Schedule(Expression<Func<IWebhookDispatcher, Task>> expression);
}
