﻿using StarWars.Application.Webhooks.Payloads;
using StarWars.Domain.Webhooks;
using System.Threading;

namespace StarWars.Application.Webhooks.Services;

public interface IWebhookSendingService
{
    Task SendWebhook<T>(WebhookEntity webhook, T payload, CancellationToken cancellationToken)
        where T : class, IWebhookPayload;
}
