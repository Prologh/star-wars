﻿using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Webhooks.Queries;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;
using System.Threading;

namespace StarWars.Application.Webhooks.Handlers;

public class GetWebhookCallRequestHandler : IGetRequestHandler<GetWebhookCallRequestModel, WebhookCallResponseModel>
{
    private readonly IGetWebhookCallQuery _getWebhookQuery;

    public GetWebhookCallRequestHandler(IGetWebhookCallQuery getWebhookQuery)
    {
        _getWebhookQuery = getWebhookQuery;
    }

    public async Task<Result<WebhookCallResponseModel, Error>> Handle(
        GetWebhookCallRequestModel request,
        CancellationToken cancellationToken)
    {
        var id = new WebhookCallIdPairRequestModel
        {
            CallExternalId = request.CallExternalId,
            WebhookExternalId = request.WebhookExternalId,
        };
        var webhook = await _getWebhookQuery.ExecuteAsync(id, cancellationToken);

        return webhook
            .ToResult(new Error(ErrorCategory.DataNotFound, ApplicationArea.Webhooks));
    }
}
