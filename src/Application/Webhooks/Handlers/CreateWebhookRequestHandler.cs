﻿using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.Specifications;
using StarWars.Domain;
using StarWars.Domain.Webhooks;
using System.Threading;

namespace StarWars.Application.Webhooks.Handlers;

public class CreateWebhookRequestHandler : ICreateRequestHandler<Guid, CreateWebhookRequestModel>
{
    private readonly IRepository<WebhookEntity> _webhookRepository;
    private readonly IUnitOfWork _unitOfWork;

    public CreateWebhookRequestHandler(
        IRepository<WebhookEntity> webhookRepository,
        IUnitOfWork unitOfWork)
    {
        _webhookRepository = Guard.Against.Null(webhookRepository);
        _unitOfWork = Guard.Against.Null(unitOfWork);
    }

    public async Task<Result<Guid, Error>> Handle(CreateWebhookRequestModel request, CancellationToken cancellationToken)
    {
        var countSpecification = new WebhookSpecification()
            .ByTopic(request.Topic);
        var existingWebhookCount = await _webhookRepository.CountAsync(countSpecification, cancellationToken);
        if (existingWebhookCount >= Constants.Validation.MaxWebhooksPerTopic)
        {
            // TODO: Add more specific error details.
            return Result.Failure<Guid, Error>(new Error(ErrorCategory.DataNotValid, ApplicationArea.Webhooks));
        }

        var sameWebhookSpecification = new WebhookSpecification()
            .ByTopic(request.Topic)
            .ByTargetUrl(request.TargetUrl);
        var sameWebhookExists = await _webhookRepository.AnyAsync(sameWebhookSpecification, cancellationToken);
        if (sameWebhookExists)
        {
            // TODO: Add more specific error details.
            return Result.Failure<Guid, Error>(new Error(ErrorCategory.DataNotValid, ApplicationArea.Webhooks));
        }

        var webhook = new WebhookEntity
        {
            TargetUrl = request.TargetUrl,
            Topic = request.Topic,
        };

        _unitOfWork.Add(webhook);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return Result.Success<Guid, Error>(webhook.ExternalId);
    }
}
