﻿using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.Specifications;
using StarWars.Domain.Webhooks;
using System.Threading;

namespace StarWars.Application.Webhooks.Handlers;

public class DeleteWebhookRequestHandler : IDeleteRequestHandler<DeleteWebhookRequestModel>
{
    private readonly IRepository<WebhookEntity> _webhookRepository;
    private readonly IUnitOfWork _unitOfWork;

    public DeleteWebhookRequestHandler(
        IRepository<WebhookEntity> webhookRepository,
        IUnitOfWork unitOfWork)
    {
        _webhookRepository = webhookRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<UnitResult<Error>> Handle(DeleteWebhookRequestModel request, CancellationToken cancellationToken)
    {
        var partNumberCheckSpecification = new WebhookSpecification()
            .ByExternalId(request.ExternalId);
        var webhook = await _webhookRepository.FirstOrDefaultAsync(partNumberCheckSpecification, cancellationToken);
        if (webhook == null)
        {
            return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Webhooks));
        }

        _unitOfWork.Remove(webhook);
        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return UnitResult.Success<Error>();
    }
}
