﻿using AutoMapper;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Pagination.ResponseModels;
using StarWars.Application.Webhooks.Queries;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;
using System.Threading;

namespace StarWars.Application.Webhooks.Handlers;

public class GetPagedWebhookCallsRequestHandler : IGetPagedRequestHandler<GetWebhookCallsPagedRequestModel, WebhookCallPagedResponseModel>
{
    private readonly IMapper _mapper;
    private readonly IGetPagedWebhookCallsQuery _getPagedWebhookCallsQuery;

    public GetPagedWebhookCallsRequestHandler(
        IMapper mapper,
        IGetPagedWebhookCallsQuery getPagedWebhookCallsQuery)
    {
        _mapper = Guard.Against.Null(mapper);
        _getPagedWebhookCallsQuery = Guard.Against.Null(getPagedWebhookCallsQuery);
    }

    public async Task<PagedResponseModel<WebhookCallPagedResponseModel>> Handle(
        GetWebhookCallsPagedRequestModel request,
        CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var pagedResult = await _getPagedWebhookCallsQuery.ExecuteAsync(request, cancellationToken);

        return _mapper.Map<PagedResponseModel<WebhookCallPagedResponseModel>>(pagedResult);
    }
}
