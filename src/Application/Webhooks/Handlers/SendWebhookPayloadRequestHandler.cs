﻿using StarWars.Application.Common.Handlers;
using StarWars.Application.Common.Requests;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Payloads;
using StarWars.Application.Webhooks.Services;
using StarWars.Application.Webhooks.Specifications;
using StarWars.Domain.Webhooks;
using System.Threading;

namespace StarWars.Application.Webhooks.Handlers;

public abstract class SendWebhookPayloadRequestHandler<TWebhookModel> : IWebhookRequestHandler<TWebhookModel>
    where TWebhookModel : class, IWebhookRequest, IWebhookPayload
{
    private readonly IWebhookSendingService _webhookSendingService;
    private readonly IRepository<WebhookEntity> _webhookRepository;

    protected SendWebhookPayloadRequestHandler(
        IWebhookSendingService webhookSendingService,
        IRepository<WebhookEntity> webhookRepository)
    {
        _webhookSendingService = webhookSendingService;
        _webhookRepository = webhookRepository;
    }

    public abstract WebhookTopic TopicSupported { get; }

    public async Task Handle(TWebhookModel request, CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var specification = new WebhookSpecification()
            .ByTopic(TopicSupported);
        var webhooks = await _webhookRepository.ToListAsync(specification, cancellationToken);
        if (webhooks.Count == 0)
        {
            return;
        }

        var options = new ParallelOptions
        {
            CancellationToken = cancellationToken,
            MaxDegreeOfParallelism = 2,
        };

        await Parallel.ForEachAsync(webhooks, options, async (w, token) => await _webhookSendingService.SendWebhook(w, request, token));
    }
}
