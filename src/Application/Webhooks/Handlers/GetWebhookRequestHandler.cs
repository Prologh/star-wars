﻿using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Webhooks.Queries;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;
using System.Threading;

namespace StarWars.Application.Webhooks.Handlers;

public class GetWebhookRequestHandler : IGetRequestHandler<GetWebhookRequestModel, WebhookResponseModel>
{
    private readonly IGetWebhookQuery _getWebhookQuery;

    public GetWebhookRequestHandler(IGetWebhookQuery getWebhookQuery)
    {
        _getWebhookQuery = getWebhookQuery;
    }

    public async Task<Result<WebhookResponseModel, Error>> Handle(
        GetWebhookRequestModel request,
        CancellationToken cancellationToken)
    {
        var webhook = await _getWebhookQuery.ExecuteAsync(request.ExternalId, cancellationToken);

        return webhook
            .ToResult(new Error(ErrorCategory.DataNotFound, ApplicationArea.Webhooks));
    }
}
