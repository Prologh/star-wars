﻿using AutoMapper;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Pagination.ResponseModels;
using StarWars.Application.Webhooks.Queries;
using StarWars.Application.Webhooks.ResponseModels;
using System.Threading;

namespace StarWars.Application.Webhooks.Handlers;

public class GetPagedWebhooksRequestHandler : IGetPagedRequestHandler<PagedRequestModel<WebhookResponseModel>, WebhookResponseModel>
{
    private readonly IMapper _mapper;
    private readonly IGetPagedWebhooksQuery _getPagedWebhooksQuery;

    public GetPagedWebhooksRequestHandler(
        IMapper mapper,
        IGetPagedWebhooksQuery getPagedWebhooksQuery)
    {
        _mapper = Guard.Against.Null(mapper);
        _getPagedWebhooksQuery = Guard.Against.Null(getPagedWebhooksQuery);
    }

    public async Task<PagedResponseModel<WebhookResponseModel>> Handle(
        PagedRequestModel<WebhookResponseModel> request,
        CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var pagedResult = await _getPagedWebhooksQuery.ExecuteAsync(request, cancellationToken);

        return _mapper.Map<PagedResponseModel<WebhookResponseModel>>(pagedResult);
    }
}
