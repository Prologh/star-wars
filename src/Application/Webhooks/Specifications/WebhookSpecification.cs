﻿using StarWars.Application.Specifications;
using StarWars.Domain.Webhooks;

namespace StarWars.Application.Webhooks.Specifications;

public class WebhookSpecification : BaseSpecification<WebhookEntity>
{
    public WebhookSpecification ByExternalId(Guid externalId)
    {
        AddCriteria(x => x.ExternalId == externalId);

        return this;
    }

    public WebhookSpecification ByTopic(WebhookTopic topic)
    {
        AddCriteria(x => x.Topic == topic);

        return this;
    }

    public WebhookSpecification ByTargetUrl(string targetUrl)
    {
        AddCriteria(x => x.TargetUrl == targetUrl);

        return this;
    }
}
