﻿using StarWars.Application.Specifications;
using StarWars.Domain.Webhooks;

namespace StarWars.Application.Webhooks.Specifications;

public class WebhookCallSpecification : BaseSpecification<WebhookCallEntity>
{
    public WebhookCallSpecification ByWebhookExternalId(Guid externalId)
    {
        AddCriteria(x => x.Webhook.ExternalId == externalId);

        return this;
    }

    public WebhookCallSpecification WithWebhook()
    {
        AddInclude(x => x.Webhook);

        return this;
    }
}
