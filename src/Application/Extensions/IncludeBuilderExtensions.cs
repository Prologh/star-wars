﻿using StarWars.Application.Specifications;
using System.Linq.Expressions;

namespace StarWars.Application.Extensions;

public static class IncludeBuilderExtensions
{
    public static IIncludeBuilder<TProperty> ThenInclude<TPreviousProperty, TProperty>(
        this IIncludeBuilder<IEnumerable<TPreviousProperty>> source,
        Expression<Func<TPreviousProperty, TProperty>> navigationPropertyPath)
    {
        Guard.Against.Null(source);
        Guard.Against.Null(navigationPropertyPath);

        var newPropertyNames = source.IncludeStore.AddInclude(navigationPropertyPath, source.PropertyNames);

        return new IncludeBuilder<TProperty>(source.IncludeStore, newPropertyNames);
    }

    public static IIncludeBuilder<TProperty> ThenInclude<TPreviousProperty, TProperty>(
        this IIncludeBuilder<TPreviousProperty> source,
        Expression<Func<TPreviousProperty, TProperty>> navigationPropertyPath)
    {
        Guard.Against.Null(source);
        Guard.Against.Null(navigationPropertyPath);

        var newPropertyNames = source.IncludeStore.AddInclude(navigationPropertyPath, source.PropertyNames);

        return new IncludeBuilder<TProperty>(source.IncludeStore, newPropertyNames);
    }
}
