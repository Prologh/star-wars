﻿using System.Linq.Expressions;
using System.Reflection;

namespace StarWars.Application.Extensions.Expressions;

public static class ExpressionsExtensions
{
    public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
    {
        Guard.Against.Null(expression1);
        Guard.Against.Null(expression2);

        var firstParameter = expression1.Parameters[0];
        var visitor = new SubstituteExpressionVisitor();
        visitor.Substitute[expression2.Parameters[0]] = firstParameter;

        var expressionBody = Expression.AndAlso(expression1.Body, visitor.Visit(expression2.Body));

        return Expression.Lambda<Func<T, bool>>(expressionBody, firstParameter);
    }

    public static Expression<Func<T, bool>> OrElse<T>(this Expression<Func<T, bool>> expression1, Expression<Func<T, bool>> expression2)
    {
        Guard.Against.Null(expression1);
        Guard.Against.Null(expression2);

        var firstParameter = expression1.Parameters[0];
        var visitor = new SubstituteExpressionVisitor();
        visitor.Substitute[expression2.Parameters[0]] = firstParameter;

        var expressionBody = Expression.OrElse(expression1.Body, visitor.Visit(expression2.Body));

        return Expression.Lambda<Func<T, bool>>(expressionBody, firstParameter);
    }

    public static string GetPropertyName<TSource, TOther>(this Expression<Func<TSource, TOther>> propertyLambda)
    {
        return GetPropertyInfo(propertyLambda).Name;
    }

    public static PropertyInfo GetPropertyInfo<TSource, TOther>(this Expression<Func<TSource, TOther>> propertyLambda)
    {
        Guard.Against.Null(propertyLambda);

        var type = typeof(TSource);

        if (propertyLambda.Body is not MemberExpression member)
        {
            throw new ArgumentException(string.Format(
                "Expression '{0}' refers to a method, not a property.",
                propertyLambda.ToString()));
        }

        var propInfo = member.Member as PropertyInfo;
        if (propInfo == null)
        {
            throw new ArgumentException(string.Format(
                "Expression '{0}' refers to a field, not a property.",
                propertyLambda.ToString()));
        }

        var hasProperty = type
            .GetProperties(BindingFlags.Public | BindingFlags.Instance)
            .Any(p =>
                p.Name == propInfo.Name
                && p.PropertyType == propInfo.PropertyType
                && p.DeclaringType == propInfo.DeclaringType);
        if (!hasProperty)
        {
            throw new ArgumentException(string.Format(
                "Expression '{0}' refers to a property that is not from type {1}.",
                propertyLambda.ToString(),
                type));
        }

        return propInfo;
    }
}
