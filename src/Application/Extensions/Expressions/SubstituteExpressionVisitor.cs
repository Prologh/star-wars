﻿using System.Linq.Expressions;

namespace StarWars.Application.Extensions.Expressions;

internal class SubstituteExpressionVisitor : ExpressionVisitor
{
    public SubstituteExpressionVisitor()
    {
        Substitute = new Dictionary<Expression, Expression>();
    }

    public Dictionary<Expression, Expression> Substitute { get; }

    protected override Expression VisitParameter(ParameterExpression node)
    {
        Guard.Against.Null(node);

        if (Substitute.TryGetValue(node, out var newExpression))
        {
            return newExpression;
        }

        return node;
    }
}
