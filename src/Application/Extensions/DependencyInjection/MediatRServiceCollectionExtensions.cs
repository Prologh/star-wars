﻿using Microsoft.Extensions.DependencyInjection;

namespace StarWars.Application.Extensions.DependencyInjection;

public static class MediatRServiceCollectionExtensions
{
    public static IServiceCollection AddMediatRWithRequestHandlers(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<IUnitOfWork>());

        return services;
    }
}
