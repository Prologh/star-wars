﻿using FluentValidation;
using StarWars.Application.FluentValidation;

namespace StarWars.Application.Extensions.FluentValidation;

public static class CollectionValidationRules
{
    public static IRuleBuilderOptions<T, ICollection<TElement>> MaxElements<T, TElement>(
        this IRuleBuilder<T, ICollection<TElement>> ruleBuilder,
        int max)
    {
        Guard.Against.Null(ruleBuilder);
        Guard.Against.OutOfRange(max, nameof(max), 0, int.MaxValue);

        return ruleBuilder
            .SetValidator(new CollectionCountValidator<T, TElement>(null, max));
    }
}
