﻿using StarWars.Application.Specifications;
using StarWars.Domain.Abstractions;
using System.Threading;

namespace StarWars.Application.Repositories;

public interface IRepository<TEntity>
    where TEntity : class, IEntity
{
    IQueryable<TEntity> Query { get; }

    Task<bool> AnyAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken);

    Task<int> CountAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken);

    Task<TEntity> FirstAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken);

    Task<TEntity> FirstOrDefaultAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken);

    Task<List<TEntity>> ToListAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken);

    Task<TEntity> SingleOrDefaultAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken);
}
