﻿namespace StarWars.Application.Sorting;

public enum SortingSourceType
{
    Expression = 0,
    StringName,
}
