﻿using FluentValidation;
using FluentValidation.Validators;

namespace StarWars.Application.FluentValidation;

public class CollectionCountValidator<T, TElement> : ICollectionCountValidator, IPropertyValidator<T, ICollection<TElement>>
{
    public CollectionCountValidator(int? min, int? max)
    {
        if (min < 0 || min > max)
        {
            throw new ArgumentOutOfRangeException(nameof(min));
        }

        if (max < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(max));
        }

        Min = min;
        Max = max;
    }

    public int? Min { get; }

    public int? Max { get; }

    public string Name => "CollectionCountValidator";

    public string GetDefaultMessageTemplate(string errorCode)
    {
        return "The array contains too many items.";
    }

    public bool IsValid(ValidationContext<T> context, ICollection<TElement> value)
    {
        Guard.Against.Null(context);
        Guard.Against.Null(value);

        return (!Min.HasValue || value.Count >= Min)
            && (!Max.HasValue || value.Count <= Max);
    }
}
