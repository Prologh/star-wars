﻿using FluentValidation.Validators;

namespace StarWars.Application.FluentValidation;

public interface ICollectionCountValidator : IPropertyValidator
{
    public int? Min { get; }

    public int? Max { get; }
}
