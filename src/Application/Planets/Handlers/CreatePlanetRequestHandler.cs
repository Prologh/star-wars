﻿using StarWars.Application.Characters.Specifications;
using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Planets.RequestModels;
using StarWars.Application.Planets.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Characters;
using StarWars.Domain.Planets;
using System.Threading;

namespace StarWars.Application.Planets.Handlers;

public class CreatePlanetRequestHandler : ICreateRequestHandler<CreatePlanetRequestModel>
{
    private readonly IRepository<CharacterEntity> _characterRepository;
    private readonly IRepository<PlanetEntity> _planetRepository;
    private readonly IUnitOfWork _unitOfWork;

    public CreatePlanetRequestHandler(
        IRepository<CharacterEntity> characterRepository,
        IRepository<PlanetEntity> planetRepository,
        IUnitOfWork unitOfWork)
    {
        _planetRepository = Guard.Against.Null(planetRepository);
        _unitOfWork = Guard.Against.Null(unitOfWork);
        _characterRepository = Guard.Against.Null(characterRepository);
    }

    public async Task<UnitResult<Error>> Handle(CreatePlanetRequestModel request, CancellationToken cancellationToken)
    {
        var nameCheckSpecification = new PlanetSpecification()
            .ByName(request.PlanetName);
        var isNameTaken = await _planetRepository.AnyAsync(nameCheckSpecification, cancellationToken);
        if (isNameTaken)
        {
            return UnitResult.Failure(new Error(ErrorCategory.DataNotValid, ApplicationArea.Planets, ErrorCodes.PlanetNameAlreadyUsed));
        }

        var entity = new PlanetEntity
        {
            Name = request.PlanetName,
        };

        foreach (var characterName in request.Characters)
        {
            var specification = new CharacterSpecification()
                .ByName(characterName);
            var character = await _characterRepository.FirstOrDefaultAsync(specification, cancellationToken);
            if (character is null)
            {
                return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Characters));
            }

            entity.Characters.Add(character);
        }

        _unitOfWork.Add(entity);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return UnitResult.Success<Error>();
    }
}
