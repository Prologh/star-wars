﻿using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Planets.RequestModels;
using StarWars.Application.Planets.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Planets;
using System.Threading;

namespace StarWars.Application.Planets.Handlers;

public class DeletePlanetRequestHandler : IDeleteRequestHandler<DeletePlanetRequestModel>
{
    private readonly IRepository<PlanetEntity> _planetRepository;
    private readonly IUnitOfWork _unitOfWork;

    public DeletePlanetRequestHandler(
        IRepository<PlanetEntity> planetRepository,
        IUnitOfWork unitOfWork)
    {
        _planetRepository = planetRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<UnitResult<Error>> Handle(DeletePlanetRequestModel request, CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var partNumberCheckSpecification = new PlanetSpecification()
            .ByName(request.PlanetName);
        var planet = await _planetRepository.FirstOrDefaultAsync(partNumberCheckSpecification, cancellationToken);
        if (planet == null)
        {
            return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Planets));
        }

        _unitOfWork.Remove(planet);
        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return UnitResult.Success<Error>();
    }
}
