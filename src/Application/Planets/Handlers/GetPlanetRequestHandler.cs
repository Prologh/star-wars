﻿using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Planets.Queries;
using StarWars.Application.Planets.RequestModels;
using StarWars.Application.Planets.ResponseModels;
using System.Threading;

namespace StarWars.Application.Planets.Handlers;

public class GetPlanetRequestHandler : IGetRequestHandler<GetPlanetRequestModel, PlanetResponseModel>
{
    private readonly IGetPlanetQuery _getPlanetQuery;

    public GetPlanetRequestHandler(IGetPlanetQuery getPlanetQuery)
    {
        _getPlanetQuery = getPlanetQuery;
    }

    public async Task<Result<PlanetResponseModel, Error>> Handle(
        GetPlanetRequestModel request,
        CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var planet = await _getPlanetQuery.ExecuteAsync(request.PlanetName, cancellationToken);

        return planet.ToResult(new Error(ErrorCategory.DataNotFound, ApplicationArea.Planets));
    }
}
