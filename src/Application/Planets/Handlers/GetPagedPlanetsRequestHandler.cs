﻿using AutoMapper;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Pagination.ResponseModels;
using StarWars.Application.Planets.Queries;
using StarWars.Application.Planets.ResponseModels;
using System.Threading;

namespace StarWars.Application.Planets.Handlers;

public class GetPagedPlanetsRequestHandler : IGetPagedRequestHandler<PagedRequestModel<PlanetResponseModel>, PlanetResponseModel>
{
    private readonly IMapper _mapper;
    private readonly IGetPagedPlanetsQuery _getPagedPlanetsQuery;

    public GetPagedPlanetsRequestHandler(
        IMapper mapper,
        IGetPagedPlanetsQuery getPagedPlanetsQuery)
    {
        _mapper = Guard.Against.Null(mapper);
        _getPagedPlanetsQuery = Guard.Against.Null(getPagedPlanetsQuery);
    }

    public async Task<PagedResponseModel<PlanetResponseModel>> Handle(
        PagedRequestModel<PlanetResponseModel> request,
        CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var pagedResult = await _getPagedPlanetsQuery.ExecuteAsync(request, cancellationToken);

        return _mapper.Map<PagedResponseModel<PlanetResponseModel>>(pagedResult);
    }
}
