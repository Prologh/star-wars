﻿using AutoMapper;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Pagination.ResponseModels;
using StarWars.Application.Planets.Queries;
using StarWars.Application.Planets.RequestModels;
using StarWars.Application.Planets.ResponseModels;
using System.Threading;

namespace StarWars.Application.Planets.Handlers;

public class GetPagedPlanetCharactersRequestHandler : IGetPagedRequestHandler<GetPlanetCharactersPagedRequestModel, PlanetCharacterResponseModel>
{
    private readonly IMapper _mapper;
    private readonly IGetPagedPlanetCharactersQuery _getPagedPlanetCharactersQuery;

    public GetPagedPlanetCharactersRequestHandler(
        IMapper mapper,
        IGetPagedPlanetCharactersQuery getPagedPlanetCharactersQuery)
    {
        _mapper = Guard.Against.Null(mapper);
        _getPagedPlanetCharactersQuery = Guard.Against.Null(getPagedPlanetCharactersQuery);
    }

    public async Task<PagedResponseModel<PlanetCharacterResponseModel>> Handle(
        GetPlanetCharactersPagedRequestModel request,
        CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var pagedResult = await _getPagedPlanetCharactersQuery.ExecuteAsync(request, cancellationToken);

        return _mapper.Map<PagedResponseModel<PlanetCharacterResponseModel>>(pagedResult);
    }
}
