﻿using StarWars.Application.Specifications;
using StarWars.Domain.Planets;

namespace StarWars.Application.Planets.Specifications;

public class PlanetSpecification : BaseSpecification<PlanetEntity>
{
    public PlanetSpecification ByName(string name)
    {
        Guard.Against.NullOrWhiteSpace(name);

        AddCriteria(x => x.Name == name);

        return this;
    }
}
