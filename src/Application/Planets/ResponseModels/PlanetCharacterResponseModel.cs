﻿namespace StarWars.Application.Planets.ResponseModels;

public class PlanetCharacterResponseModel
{
    public string Name { get; set; }
}
