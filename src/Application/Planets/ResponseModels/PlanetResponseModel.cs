﻿namespace StarWars.Application.Planets.ResponseModels;

public record PlanetResponseModel
{
    public string Name { get; set; }
}
