﻿using StarWars.Application.Common.Requests;
using StarWars.Application.Planets.ResponseModels;

namespace StarWars.Application.Planets.RequestModels;

public class GetPlanetRequestModel : IGetRequest<PlanetResponseModel>
{
    public string PlanetName { get; set; }
}
