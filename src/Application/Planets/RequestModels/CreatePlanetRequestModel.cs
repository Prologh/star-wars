﻿using StarWars.Application.Common.Requests;

namespace StarWars.Application.Planets.RequestModels;

public class CreatePlanetRequestModel : ICreateRequest
{
    public CreatePlanetRequestModel()
    {
        Characters = [];
    }

    public string PlanetName { get; set; }

    public IList<string> Characters { get; set; }
}
