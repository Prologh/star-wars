﻿using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Planets.ResponseModels;

namespace StarWars.Application.Planets.RequestModels;

public record GetPlanetCharactersPagedRequestModel : PagedRequestModel<PlanetCharacterResponseModel>
{
    public GetPlanetCharactersPagedRequestModel(int? pageNumber, int? pageSize)
        : base(pageNumber, pageSize)
    {
    }

    public string PlanetName { get; set; }
}
