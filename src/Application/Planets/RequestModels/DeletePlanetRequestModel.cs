﻿using StarWars.Application.Common.Requests;

namespace StarWars.Application.Planets.RequestModels;

public record DeletePlanetRequestModel : IDeleteRequest
{
    public string PlanetName { get; set; }
}
