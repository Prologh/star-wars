﻿using AutoMapper;
using StarWars.Application.Planets.ResponseModels;
using StarWars.Domain.Planets;

namespace StarWars.Application.Planets.Mapping;

public class PlanetsProfile : Profile
{
    public PlanetsProfile()
    {
        // Request model ==> Entity

        // Entity ==> Response model
        CreateMap<PlanetEntity, PlanetResponseModel>();
    }
}
