﻿using StarWars.Application.Common.Queries;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Planets.ResponseModels;

namespace StarWars.Application.Planets.Queries;

public interface IGetPagedPlanetsQuery : IPagedQuery<PagedRequestModel, PlanetResponseModel>
{
}
