﻿using StarWars.Application.Common.Queries;
using StarWars.Application.Planets.RequestModels;
using StarWars.Application.Planets.ResponseModels;

namespace StarWars.Application.Planets.Queries;

public interface IGetPagedPlanetCharactersQuery : IPagedQuery<GetPlanetCharactersPagedRequestModel, PlanetCharacterResponseModel>
{
}
