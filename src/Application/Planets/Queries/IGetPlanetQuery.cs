﻿using StarWars.Application.Common.Queries;
using StarWars.Application.Planets.ResponseModels;

namespace StarWars.Application.Planets.Queries;

public interface IGetPlanetQuery : IMaybeQuery<string, PlanetResponseModel>
{
}
