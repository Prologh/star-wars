﻿using MediatR;

namespace StarWars.Application.Common.Requests;

public interface IDeleteRequest : IRequest<UnitResult<Error>>
{
}
