﻿using MediatR;

namespace StarWars.Application.Common.Requests;

public interface ICreateRequest : IRequest<UnitResult<Error>>
{
}

public interface ICreateRequest<TId> : IRequest<Result<TId, Error>>
    where TId : struct
{
}
