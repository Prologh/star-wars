﻿using MediatR;

namespace StarWars.Application.Common.Requests;

public interface IWebhookRequest : IRequest
{
}
