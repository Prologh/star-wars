﻿using MediatR;
using StarWars.Application.Pagination.ResponseModels;

namespace StarWars.Application.Common.Requests;

public interface IGetPagedRequest<in TPagedRequest, TPagedResponseItem> : IRequest<PagedResponseModel<TPagedResponseItem>>
    where TPagedResponseItem : class
{
}
