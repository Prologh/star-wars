﻿using MediatR;

namespace StarWars.Application.Common.Requests;

public interface IUpdateRequest : IRequest<UnitResult<Error>>
{
}
