﻿using MediatR;

namespace StarWars.Application.Common.Requests;

public interface IGetRequest<TResponse> : IRequest<Result<TResponse, Error>>
{
}
