﻿namespace StarWars.Application.Common;

public enum ErrorCategory
{
    Unknown = 0,
    DataNotFound,
    DataNotValid,
    InternalError,
}
