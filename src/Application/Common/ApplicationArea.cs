﻿namespace StarWars.Application.Common;

public enum ApplicationArea
{
    Unknown = 0,
    Characters,
    CharacterFriends,
    Episodes,
    Planets,
    Webhooks,
}
