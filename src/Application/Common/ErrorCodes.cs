﻿namespace StarWars.Application.Common;

public static class ErrorCodes
{
    public const string CharacterNameAlreadyUsed = "0001";
    public const string EpisodePartNumberAlreadyUsed = "0002";
    public const string PlanetNameAlreadyUsed = "0003";
}
