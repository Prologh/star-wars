﻿using Newtonsoft.Json;

namespace StarWars.Application.Common;

public sealed class EmptyEnvelope : Envelope<EmptyData>
{
    public EmptyEnvelope() : base(new EmptyData())
    {
    }

    public EmptyEnvelope(IReadOnlyList<Error> errors) : base(errors)
    {
    }

    public EmptyEnvelope(Error error) : base(error)
    {
    }

    [JsonConstructor]
    private EmptyEnvelope(EmptyData emptyData, IReadOnlyList<Error> errors) : base(emptyData, errors)
    {
    }
}
