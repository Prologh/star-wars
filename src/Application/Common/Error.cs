﻿using Newtonsoft.Json;

namespace StarWars.Application.Common;

public sealed class Error
{
    public Error(ErrorCategory category, ApplicationArea applicationArea)
    {
        Category = category;
        ApplicationArea = applicationArea;
        ErrorCode = BuildErrorCode(category, applicationArea);
    }

    [JsonConstructor]
    public Error(ErrorCategory category, ApplicationArea applicationArea, string errorCode)
    {
        Category = category;
        ApplicationArea = applicationArea;
        ErrorCode = errorCode;
    }

    public ApplicationArea ApplicationArea { get; }

    public ErrorCategory Category { get; }

    public string ErrorCode { get; }

    public static string BuildErrorCode(ErrorCategory category, ApplicationArea applicationArea)
    {
        return ((int)applicationArea).ToString("D4") + "." + ((int)category).ToString("D4");
    }
}
