﻿namespace StarWars.Application.Common.Queries;

public interface IMaybeQuery<TRequest, TResponse> : IQuery<TRequest, Maybe<TResponse>>
{
}
