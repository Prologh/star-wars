﻿using StarWars.Application.Pagination;
using StarWars.Application.Pagination.RequestModels;

namespace StarWars.Application.Common.Queries;

public interface IPagedQuery<TRequest, TResponse> : IQuery<TRequest, IPagedResult<TResponse>>
    where TRequest : PagedRequestModel
{
}
