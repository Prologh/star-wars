﻿using System.Threading;

namespace StarWars.Application.Common.Queries;

public interface IQuery<TRequest, TResponse>
{
    Task<TResponse> ExecuteAsync(TRequest request, CancellationToken cancellationToken);
}
