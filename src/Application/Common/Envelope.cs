﻿using Newtonsoft.Json;

namespace StarWars.Application.Common;

public class Envelope<T>
{
    public Envelope(T data)
    {
        Data = data;
        Errors = new List<Error>().AsReadOnly();
    }

    public Envelope(IReadOnlyList<Error> errors)
    {
        Errors = errors;
    }

    public Envelope(Error error)
    {
        Errors = new List<Error> { error }.AsReadOnly();
    }

    [JsonConstructor]
    protected Envelope(T data, IReadOnlyList<Error> errors) : this(errors)
    {
        Data = data;
    }

    public T Data { get; }

    public IReadOnlyList<Error> Errors { get; }

    public bool HasErrors => Errors.Count > 0;
}
