﻿using MediatR;
using StarWars.Application.Common.Requests;

namespace StarWars.Application.Common.Handlers;

public interface IWebhookRequestHandler<in TRequest> : IRequestHandler<TRequest>
    where TRequest : class, IWebhookRequest
{
}
