﻿using MediatR;
using StarWars.Application.Common.Requests;

namespace StarWars.Application.Common.Handlers;

public interface IGetRequestHandler<in TRequest, TResponse> : IRequestHandler<TRequest, Result<TResponse, Error>>
    where TRequest : class, IGetRequest<TResponse>
{
}
