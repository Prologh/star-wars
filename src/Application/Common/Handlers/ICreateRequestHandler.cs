﻿using MediatR;
using StarWars.Application.Common.Requests;

namespace StarWars.Application.Common.Handlers;

public interface ICreateRequestHandler<in TRequest> : IRequestHandler<TRequest, UnitResult<Error>>
    where TRequest : class, ICreateRequest
{
}

public interface ICreateRequestHandler<TId, in TRequest> : IRequestHandler<TRequest, Result<TId, Error>>
    where TRequest : class, ICreateRequest<TId>
    where TId : struct
{
}
