﻿using MediatR;
using StarWars.Application.Common.Requests;
using StarWars.Application.Pagination.ResponseModels;

namespace StarWars.Application.Common.Handlers;

public interface IGetPagedRequestHandler<in TPagedRequest, TPagedResponseItem> : IRequestHandler<TPagedRequest, PagedResponseModel<TPagedResponseItem>>
    where TPagedRequest : class, IGetPagedRequest<TPagedRequest, TPagedResponseItem>
    where TPagedResponseItem : class
{
}
