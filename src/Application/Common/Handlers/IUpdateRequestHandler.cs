﻿using MediatR;
using StarWars.Application.Common.Requests;

namespace StarWars.Application.Common.Handlers;

public interface IUpdateRequestHandler<in TRequest> : IRequestHandler<TRequest, UnitResult<Error>>
    where TRequest : class, IUpdateRequest
{
}
