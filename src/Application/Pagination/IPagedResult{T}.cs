﻿namespace StarWars.Application.Pagination;

/// <summary>
/// Defines minimum requirements for paged query result.
/// </summary>
/// <typeparam name="T">
/// The type of items held within the result.
/// </typeparam>
public interface IPagedResult<out T> : IPagedResult
{
    /// <summary>
    /// Gets the list of result items.
    /// </summary>
    IReadOnlyList<T> Items { get; }
}
