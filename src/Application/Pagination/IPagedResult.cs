﻿namespace StarWars.Application.Pagination;

/// <summary>
/// Defines minimum requirements for paged query result.
/// </summary>
public interface IPagedResult
{
    /// <summary>
    /// Gets the page number.
    /// </summary>
    int PageNumber { get; }

    /// <summary>
    /// Gets the page size.
    /// </summary>
    int PageSize { get; }

    /// <summary>
    /// Gets a total amount of pages.
    /// </summary>
    long TotalPages { get; }

    /// <summary>
    /// Gets a total amount of items.
    /// </summary>
    long TotalSize { get; }
}
