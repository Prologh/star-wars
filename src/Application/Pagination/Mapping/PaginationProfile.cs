﻿using AutoMapper;
using StarWars.Application.Pagination.ResponseModels;

namespace StarWars.Application.Pagination.Mapping;

public class PaginationProfile : Profile
{
    public PaginationProfile()
    {
        // Paged result ==> Paged response model
        CreateMap(typeof(IPagedResult<>), typeof(PagedResponseModel<>));
    }
}
