﻿using StarWars.Domain;

namespace StarWars.Application.Pagination.RequestModels;

public abstract record PagedRequestModel
{
    protected PagedRequestModel(int? pageNumber, int? pageSize)
    {
        PageNumber = pageNumber ?? Constants.Pagination.Defaults.PageNumber;
        PageSize = pageSize ?? Constants.Pagination.Defaults.PageSize;
    }

    public int PageNumber { get; }

    public int PageSize { get; }

    public int GetSkipCount() => (PageNumber - 1) * PageSize;
}
