﻿using StarWars.Application.Common.Requests;

namespace StarWars.Application.Pagination.RequestModels;

public record PagedRequestModel<TPagedResponseItem> : PagedRequestModel, IGetPagedRequest<PagedRequestModel<TPagedResponseItem>, TPagedResponseItem>
    where TPagedResponseItem : class
{
    public PagedRequestModel(int? pageNumber, int? pageSize) : base(pageNumber, pageSize)
    {
    }
}
