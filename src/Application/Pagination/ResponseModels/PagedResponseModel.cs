﻿namespace StarWars.Application.Pagination.ResponseModels;

/// <summary>
/// Represents a query directed to a paged resource.
/// </summary>
public abstract record PagedResponseModel : IPagedResult
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PagedResponseModel"/> class.
    /// </summary>
    protected PagedResponseModel(int pageNumber, int pageSize, long totalPages, long totalSize)
    {
        PageNumber = pageNumber;
        PageSize = pageSize;
        TotalPages = totalPages;
        TotalSize = totalSize;
    }

    /// <summary>
    /// Gets or sets the page number.
    /// </summary>
    public int PageNumber { get; set; }

    /// <summary>
    /// Gets or sets the size of page.
    /// </summary>
    public int PageSize { get; set; }

    /// <summary>
    ///  Gets or sets the total amount of pages.
    /// </summary>
    public long TotalPages { get; set; }

    /// <summary>
    /// Gets or sets the total size of all items on all pages.
    /// </summary>
    public long TotalSize { get; set; }
}
