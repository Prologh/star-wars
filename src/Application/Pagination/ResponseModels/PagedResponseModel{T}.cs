﻿using System.Collections.ObjectModel;

namespace StarWars.Application.Pagination.ResponseModels;

public record PagedResponseModel<T> : PagedResponseModel, IPagedResult<T>
{
    public PagedResponseModel(int pageNumber, int pageSize, long totalPages, long totalSize, IList<T> items)
        : base(pageNumber, pageSize, totalPages, totalSize)
    {
        Guard.Against.Null(items);

        Items = new ReadOnlyCollection<T>(items);
    }

    /// <summary>
    /// Gets the list of result items.
    /// </summary>
    public IReadOnlyList<T> Items { get; }
}
