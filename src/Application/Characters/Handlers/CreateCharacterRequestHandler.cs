﻿using MediatR;
using StarWars.Application.Characters.RequestModels;
using StarWars.Application.Characters.Specifications;
using StarWars.Application.Common;
using StarWars.Application.Episodes.Specifications;
using StarWars.Application.Planets.Specifications;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Services;
using StarWars.Domain.Characters;
using StarWars.Domain.Episodes;
using StarWars.Domain.Planets;
using System.Threading;

namespace StarWars.Application.Characters.Handlers;

public class CreateCharacterRequestHandler : IRequestHandler<CreateCharacterRequestModel, UnitResult<Error>>
{
    private readonly IRepository<CharacterEntity> _characterRepository;
    private readonly IRepository<EpisodeEntity> _episodeRepository;
    private readonly IRepository<PlanetEntity> _planetRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IBackgroundJobWebhookDispatcher _backgroundJobWebhookDispatcher;

    public CreateCharacterRequestHandler(
        IRepository<CharacterEntity> characterRepository,
        IRepository<EpisodeEntity> episodeRepository,
        IRepository<PlanetEntity> planetRepository,
        IUnitOfWork unitOfWork,
        IBackgroundJobWebhookDispatcher backgroundJobWebhookDispatcher)
    {
        _characterRepository = Guard.Against.Null(characterRepository);
        _episodeRepository = Guard.Against.Null(episodeRepository);
        _planetRepository = Guard.Against.Null(planetRepository);
        _unitOfWork = Guard.Against.Null(unitOfWork);
        _backgroundJobWebhookDispatcher = Guard.Against.Null(backgroundJobWebhookDispatcher);
    }

    public async Task<UnitResult<Error>> Handle(CreateCharacterRequestModel request, CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var nameCheckSpecification = new CharacterSpecification()
            .ByName(request.Name);
        var isNameAlreadyTaken = await _characterRepository.AnyAsync(nameCheckSpecification, cancellationToken);
        if (isNameAlreadyTaken)
        {
            return UnitResult.Failure(new Error(ErrorCategory.DataNotValid, ApplicationArea.Characters, ErrorCodes.CharacterNameAlreadyUsed));
        }

        var newCharacter = new CharacterEntity
        {
            Name = request.Name,
        };

        foreach (var friendName in request.Friends)
        {
            var specification = new CharacterSpecification()
                .ByName(friendName);
            var friend = await _characterRepository.FirstOrDefaultAsync(specification, cancellationToken);
            if (friend is null)
            {
                return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.CharacterFriends));
            }

            newCharacter.Friends.Add(friend);
        }

        foreach (var episodeName in request.Episodes)
        {
            var specification = new EpisodeSpecification()
                .ByName(episodeName);
            var episode = await _episodeRepository.FirstOrDefaultAsync(specification, cancellationToken);
            if (episode is null)
            {
                return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Episodes));
            }

            newCharacter.CharacterEpisodes.Add(new CharacterEpisodeEntity
            {
                Character = newCharacter,
                Episode = episode,
            });
        }

        if (request.Planet != null)
        {
            var specification = new PlanetSpecification()
                .ByName(request.Planet);
            var originPlanet = await _planetRepository.FirstOrDefaultAsync(specification, cancellationToken);
            if (originPlanet is null)
            {
                return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Planets));
            }

            newCharacter.OriginPlanet = originPlanet;
        }

        _unitOfWork.Add(newCharacter);
        await _unitOfWork.SaveChangesAsync(cancellationToken);

        _backgroundJobWebhookDispatcher.Schedule(x => x.DispatchCharacterCreated(newCharacter, default));

        return UnitResult.Success<Error>();
    }
}
