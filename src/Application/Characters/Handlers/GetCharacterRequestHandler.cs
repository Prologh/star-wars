﻿using StarWars.Application.Characters.Queries;
using StarWars.Application.Characters.RequestModels;
using StarWars.Application.Characters.ResponseModels;
using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using System.Threading;

namespace StarWars.Application.Characters.Handlers;

public class GetCharacterRequestHandler : IGetRequestHandler<GetCharacterRequestModel, CharacterResponseModel>
{
    private readonly IGetCharacterQuery _getCharacterQuery;

    public GetCharacterRequestHandler(IGetCharacterQuery getCharacterQuery)
    {
        _getCharacterQuery = getCharacterQuery;
    }

    public async Task<Result<CharacterResponseModel, Error>> Handle(
        GetCharacterRequestModel request,
        CancellationToken cancellationToken)
    {
        var character = await _getCharacterQuery.ExecuteAsync(request.Name, cancellationToken);

        return character
            .ToResult(new Error(ErrorCategory.DataNotFound, ApplicationArea.Characters));
    }
}
