﻿using AutoMapper;
using StarWars.Application.Characters.Queries;
using StarWars.Application.Characters.ResponseModels;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Pagination.ResponseModels;
using System.Threading;

namespace StarWars.Application.Characters.Handlers;

public class GetPagedCharactersRequestHandler :
    IGetPagedRequestHandler<PagedRequestModel<CharacterResponseModel>, CharacterResponseModel>
{
    private readonly IMapper _mapper;
    private readonly IGetPagedCharactersQuery _getPagedCharactersQuery;

    public GetPagedCharactersRequestHandler(
        IMapper mapper,
        IGetPagedCharactersQuery getPagedCharactersQuery)
    {
        _mapper = Guard.Against.Null(mapper);
        _getPagedCharactersQuery = Guard.Against.Null(getPagedCharactersQuery);
    }

    public async Task<PagedResponseModel<CharacterResponseModel>> Handle(
        PagedRequestModel<CharacterResponseModel> request,
        CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var pagedResult = await _getPagedCharactersQuery.ExecuteAsync(request, cancellationToken);

        return _mapper.Map<PagedResponseModel<CharacterResponseModel>>(pagedResult);
    }
}
