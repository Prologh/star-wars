﻿using StarWars.Application.Characters.RequestModels;
using StarWars.Application.Characters.Specifications;
using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Episodes.Specifications;
using StarWars.Application.Planets.Specifications;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Services;
using StarWars.Domain.Characters;
using StarWars.Domain.Episodes;
using StarWars.Domain.Planets;
using System.Threading;

namespace StarWars.Application.Characters.Handlers;

public class UpdateCharacterRequestHandler : IUpdateRequestHandler<UpdateCharacterRequestModel>
{
    private readonly IRepository<CharacterEntity> _characterRepository;
    private readonly IRepository<PlanetEntity> _planetRepository;
    private readonly IRepository<EpisodeEntity> _episodeRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IBackgroundJobWebhookDispatcher _backgroundJobWebhookDispatcher;

    public UpdateCharacterRequestHandler(
        IRepository<CharacterEntity> characterRepository,
        IRepository<EpisodeEntity> episodeRepository,
        IRepository<PlanetEntity> planetRepository,
        IUnitOfWork unitOfWork,
        IBackgroundJobWebhookDispatcher backgroundJobWebhookDispatcher)
    {
        _characterRepository = Guard.Against.Null(characterRepository);
        _episodeRepository = Guard.Against.Null(episodeRepository);
        _unitOfWork = Guard.Against.Null(unitOfWork);
        _planetRepository = Guard.Against.Null(planetRepository);
        _backgroundJobWebhookDispatcher = Guard.Against.Null(backgroundJobWebhookDispatcher);
    }

    public async Task<UnitResult<Error>> Handle(UpdateCharacterRequestModel request, CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var nameCheckSpecification = new CharacterSpecification()
            .ByName(request.Name)
            .WithOriginPlanet();
        var character = await _characterRepository.SingleOrDefaultAsync(nameCheckSpecification, cancellationToken);
        if (character is null)
        {
            return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Characters));
        }

        if (request.NewName is not null)
        {
            character.Name = request.NewName;
        }

        var existingFriends = character.Friends.Select(x => x.Name).ToList();
        var friendsToAdd = request.Friends.Except(existingFriends).ToList();
        foreach (var friendName in friendsToAdd)
        {
            var specification = new CharacterSpecification()
                .ByName(friendName);
            var friend = await _characterRepository.FirstOrDefaultAsync(specification, cancellationToken);
            if (friend is null)
            {
                return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.CharacterFriends));
            }

            character.Friends.Add(friend);
        }

        var friendsToRemove = existingFriends.Except(request.Friends).ToList();
        foreach (var friendName in friendsToRemove)
        {
            var friendToRemove = character.Friends.Single(x => x.Name == friendName);
            character.Friends.Remove(friendToRemove);
        }

        var existingEpisodes = character.CharacterEpisodes.Select(x => x.Episode.Name).ToList();
        var episodesToAdd = request.Episodes.Except(existingEpisodes).ToList();
        foreach (var episodeName in episodesToAdd)
        {
            var specification = new EpisodeSpecification()
                .ByName(episodeName);
            var episode = await _episodeRepository.FirstOrDefaultAsync(specification, cancellationToken);
            if (episode is null)
            {
                return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Episodes));
            }

            character.CharacterEpisodes.Add(new CharacterEpisodeEntity
            {
                Character = character,
                Episode = episode,
            });
        }

        var episodesToRemove = existingEpisodes.Except(request.Episodes).ToList();
        foreach (var episodeName in episodesToRemove)
        {
            var episode = character.CharacterEpisodes.Single(x => x.Episode.Name == episodeName);
            character.CharacterEpisodes.Remove(episode);
        }

        if (request.Planet is null)
        {
            character.OriginPlanetId = null;
        }
        else
        {
            var specification = new PlanetSpecification()
                .ByName(request.Planet);
            var originPlanet = await _planetRepository.FirstOrDefaultAsync(specification, cancellationToken);
            if (originPlanet is null)
            {
                return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Planets));
            }

            character.OriginPlanet = originPlanet;
        }

        _unitOfWork.Update(character);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        _backgroundJobWebhookDispatcher.Schedule(x => x.DispatchCharacterUpdated(character, default));

        return UnitResult.Success<Error>();
    }
}
