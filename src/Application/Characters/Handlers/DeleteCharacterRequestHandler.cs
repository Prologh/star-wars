﻿using StarWars.Application.Characters.RequestModels;
using StarWars.Application.Characters.Specifications;
using StarWars.Application.Common;
using StarWars.Application.Common.Handlers;
using StarWars.Application.Repositories;
using StarWars.Domain.Characters;
using System.Threading;

namespace StarWars.Application.Characters.Handlers;

public class DeleteCharacterRequestHandler : IDeleteRequestHandler<DeleteCharacterRequestModel>
{
    private readonly IRepository<CharacterEntity> _charactersRepository;
    private readonly IUnitOfWork _unitOfWork;

    public DeleteCharacterRequestHandler(
        IRepository<CharacterEntity> characterRepository,
        IUnitOfWork unitOfWork)
    {
        _charactersRepository = characterRepository;
        _unitOfWork = unitOfWork;
    }

    public async Task<UnitResult<Error>> Handle(DeleteCharacterRequestModel request, CancellationToken cancellationToken)
    {
        var nameCheckSpecification = new CharacterSpecification()
            .ByName(request.Name);
        var character = await _charactersRepository.FirstOrDefaultAsync(nameCheckSpecification, cancellationToken);
        if (character == null)
        {
            return UnitResult.Failure(new Error(ErrorCategory.DataNotFound, ApplicationArea.Characters));
        }

        _unitOfWork.Remove(character);
        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return UnitResult.Success<Error>();
    }
}
