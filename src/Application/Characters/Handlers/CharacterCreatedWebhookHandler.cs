﻿using StarWars.Application.Characters.RequestModels;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Handlers;
using StarWars.Application.Webhooks.Services;
using StarWars.Domain.Webhooks;

namespace StarWars.Application.Characters.Handlers;

public class CharacterCreatedWebhookHandler : SendWebhookPayloadRequestHandler<CharacterCreatedWebhookRequestModel>
{
    public CharacterCreatedWebhookHandler(
        IWebhookSendingService webhookSendingService,
        IRepository<WebhookEntity> webhookRepository)
        : base(webhookSendingService, webhookRepository)
    {
    }

    public override WebhookTopic TopicSupported => WebhookTopic.CharacterCreated;
}
