﻿using StarWars.Application.Common.Requests;

namespace StarWars.Application.Characters.RequestModels;

public record UpdateCharacterRequestModel : IUpdateRequest
{
    public UpdateCharacterRequestModel()
    {
        Episodes = new List<string>();
        Friends = new List<string>();
    }

    public IList<string> Episodes { get; set; }

    public IList<string> Friends { get; set; }

    public string Name { get; set; }

    public string NewName { get; set; }

    public string Planet { get; set; }
}
