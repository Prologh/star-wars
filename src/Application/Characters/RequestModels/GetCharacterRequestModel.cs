﻿using StarWars.Application.Characters.ResponseModels;
using StarWars.Application.Common.Requests;

namespace StarWars.Application.Characters.RequestModels;

public record GetCharacterRequestModel : IGetRequest<CharacterResponseModel>
{
    public string Name { get; set; }
}
