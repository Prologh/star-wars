﻿using StarWars.Application.Common.Requests;

namespace StarWars.Application.Characters.RequestModels;

public record DeleteCharacterRequestModel : IDeleteRequest
{
    public string Name { get; set; }
}
