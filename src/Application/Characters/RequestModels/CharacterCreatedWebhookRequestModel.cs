﻿using StarWars.Application.Common.Requests;
using StarWars.Application.Webhooks.Payloads;

namespace StarWars.Application.Characters.RequestModels;

public record CharacterCreatedWebhookRequestModel : IWebhookRequest, IWebhookPayload
{
    public CharacterCreatedWebhookRequestModel()
    {
        Episodes = new List<string>();
        Friends = new List<string>();
    }

    public IList<string> Episodes { get; set; }

    public IList<string> Friends { get; set; }

    public string Name { get; set; }

    public string Planet { get; set; }
}
