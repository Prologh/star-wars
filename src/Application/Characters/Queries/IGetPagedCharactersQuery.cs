﻿using StarWars.Application.Characters.ResponseModels;
using StarWars.Application.Common.Queries;
using StarWars.Application.Pagination.RequestModels;

namespace StarWars.Application.Characters.Queries;

public interface IGetPagedCharactersQuery : IPagedQuery<PagedRequestModel, CharacterResponseModel>
{
}
