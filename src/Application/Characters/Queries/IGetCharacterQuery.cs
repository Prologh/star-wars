﻿using StarWars.Application.Characters.ResponseModels;
using StarWars.Application.Common.Queries;

namespace StarWars.Application.Characters.Queries;

public interface IGetCharacterQuery : IMaybeQuery<string, CharacterResponseModel>
{
}
