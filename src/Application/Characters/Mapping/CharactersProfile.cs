﻿using AutoMapper;
using StarWars.Application.Characters.ResponseModels;
using StarWars.Application.Episodes.ResponseModels;
using StarWars.Application.Planets.ResponseModels;
using StarWars.Domain.Characters;

namespace StarWars.Application.Characters.Mapping;

public class CharactersProfile : Profile
{
    public CharactersProfile()
    {
        // Request model ==> Entity

        // Entity ==> Response model
        CreateMap<CharacterEntity, CharacterResponseModel>();
        CreateMap<CharacterEntity, PlanetCharacterResponseModel>();
        CreateMap<CharacterEpisodeEntity, CharacterEpisodeResponseModel>();
        CreateMap<CharacterEntity, EpisodeCharacterResponseModel>()
            .ForMember(x => x.OriginPlanet, opts => opts.MapFrom(x => x.OriginPlanet.Name));
    }
}
