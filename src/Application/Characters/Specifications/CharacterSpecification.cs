﻿using StarWars.Application.Specifications;
using StarWars.Domain.Characters;

namespace StarWars.Application.Characters.Specifications;

public class CharacterSpecification : BaseSpecification<CharacterEntity>
{
    public CharacterSpecification ByName(string name)
    {
        Guard.Against.NullOrWhiteSpace(name);

        AddCriteria(x => x.Name == name);

        return this;
    }

    public CharacterSpecification ByPlanetName(string planetName)
    {
        Guard.Against.NullOrWhiteSpace(planetName);

        AddCriteria(x => x.OriginPlanet.Name == planetName);

        return this;
    }

    public CharacterSpecification WithOriginPlanet()
    {
        AddInclude(x => x.OriginPlanet);

        return this;
    }
}
