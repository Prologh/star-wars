﻿using StarWars.Application.Specifications;
using StarWars.Domain.Characters;

namespace StarWars.Application.Characters.Specifications;

public class CharacterEpisodeSpecification : BaseSpecification<CharacterEpisodeEntity>
{
    public CharacterEpisodeSpecification ByEpisodePartNumber(int partNumber)
    {
        AddCriteria(x => x.Episode.PartNumber == partNumber);

        return this;
    }
}
