﻿using StarWars.Application.Planets.ResponseModels;

namespace StarWars.Application.Characters.ResponseModels;

public record CharacterResponseModel
{
    public CharacterResponseModel()
    {
        CharacterEpisodes = new List<CharacterEpisodeResponseModel>();
        Friends = new List<CharacterResponseModel>();
    }

    public IList<CharacterEpisodeResponseModel> CharacterEpisodes { get; set; }

    public IList<CharacterResponseModel> Friends { get; set; }

    public string Name { get; set; }

    public PlanetResponseModel OriginPlanet { get; set; }
}
