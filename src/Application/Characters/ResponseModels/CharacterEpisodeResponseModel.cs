﻿using StarWars.Application.Episodes.ResponseModels;

namespace StarWars.Application.Characters.ResponseModels;

public record CharacterEpisodeResponseModel
{
    public CharacterResponseModel Character { get; set; }

    public EpisodeResponseModel Episode { get; set; }
}
