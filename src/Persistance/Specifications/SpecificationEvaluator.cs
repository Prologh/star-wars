﻿using StarWars.Application.Sorting;
using StarWars.Application.Specifications;
using StarWars.Domain.Abstractions;
using StarWars.Persistance.Sorting;

namespace StarWars.Persistance.Specifications;

public class SpecificationEvaluator<TEntity> : ISpecificationEvaluator<TEntity>
    where TEntity : class, IEntity
{
    private readonly ISortingExpressionProvider sortingExpressionProvider;

    public SpecificationEvaluator(ISortingExpressionProvider sortingExpressionProvider)
    {
        this.sortingExpressionProvider = sortingExpressionProvider;
    }

    public IQueryable<TEntity> Evaluate(IQueryable<TEntity> query, ISpecification<TEntity> specification)
    {
        Guard.Against.Null(query);
        Guard.Against.Null(specification);

        if (specification.Includes.Any())
        {
            foreach (var include in specification.Includes)
            {
                query = query.Include(include);
            }
        }

        if (specification.Criteria is not null)
        {
            query = query.Where(specification.Criteria);
        }

        if (specification.Pagination is not null)
        {
            query = query.Skip(specification.Pagination.GetSkipCount());
            query = query.Take(specification.Pagination.PageSize);
        }

        if (specification.SortingSourceType is not null)
        {
            var sortingExpression = specification.SortingSourceType == SortingSourceType.Expression
                ? specification.SortingExpression
                : sortingExpressionProvider.GetExpression<TEntity>(specification.SortingName);

            if (specification.IsSortingDescending)
            {
                query = query.OrderByDescending(sortingExpression);
            }
            else
            {
                query = query.OrderBy(sortingExpression);
            }
        }

        return query;
    }
}
