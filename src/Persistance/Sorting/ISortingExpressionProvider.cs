﻿using StarWars.Domain.Abstractions;
using System.Linq.Expressions;

namespace StarWars.Persistance.Sorting;

public interface ISortingExpressionProvider
{
    Expression<Func<TEntity, object>> GetExpression<TEntity>(string name)
        where TEntity : class, IEntity;
}
