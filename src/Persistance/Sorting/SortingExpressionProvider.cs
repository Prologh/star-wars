﻿using StarWars.Application.Expressions;
using StarWars.Domain.Abstractions;
using System.Linq.Expressions;

namespace StarWars.Persistance.Sorting;

public class SortingExpressionProvider : ISortingExpressionProvider
{
    private readonly IExpressionUtility _expressionUtility;

    public SortingExpressionProvider(IExpressionUtility expressionUtility)
    {
        _expressionUtility = expressionUtility;
    }

    public Expression<Func<TEntity, object>> GetExpression<TEntity>(string name)
        where TEntity : class, IEntity
    {
        Guard.Against.NullOrWhiteSpace(name);

        var parameter = Expression.Parameter(typeof(TEntity));
        if (!_expressionUtility.TryProperty(parameter, name, nestedProperty: false, out var propertyExpression))
        {
            return null;
        }

        var convertedProperty = Expression.Convert(propertyExpression, typeof(object));

        return _expressionUtility.LambdaFunc<TEntity, object>(convertedProperty, parameter);
    }
}
