﻿using AutoMapper;
using CSharpFunctionalExtensions;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Queries;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;
using StarWars.Domain.Webhooks;
using System.Threading;

namespace StarWars.Persistance.Webhooks.Queries;

public class GetWebhookCallQuery : IGetWebhookCallQuery
{
    private readonly IRepository<WebhookCallEntity> _webhookCallRepository;
    private readonly IMapper _mapper;

    public GetWebhookCallQuery(
        IRepository<WebhookCallEntity> webhookCallRepository,
        IMapper mapper)
    {
        _webhookCallRepository = webhookCallRepository;
        _mapper = mapper;
    }

    public async Task<Maybe<WebhookCallResponseModel>> ExecuteAsync(WebhookCallIdPairRequestModel request, CancellationToken cancellationToken)
    {
        var entity = await _webhookCallRepository.Query
            .FirstOrDefaultAsync(
                c => c.ExternalId == request.CallExternalId && c.Webhook.ExternalId == request.WebhookExternalId,
                cancellationToken);

        return _mapper.Map<WebhookCallResponseModel>(entity);
    }
}
