﻿using AutoMapper;
using StarWars.Application.Pagination;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Queries;
using StarWars.Application.Webhooks.RequestModels;
using StarWars.Application.Webhooks.ResponseModels;
using StarWars.Application.Webhooks.Specifications;
using StarWars.Domain.Webhooks;
using StarWars.Persistance.Common.Queries;
using StarWars.Persistance.Extensions;
using System.Threading;

namespace StarWars.Persistance.Webhooks.Queries;

public class GetPagedWebhookCallsQuery : BasePagedQuery<WebhookCallEntity, WebhookCallPagedResponseModel>, IGetPagedWebhookCallsQuery
{
    private readonly IRepository<WebhookCallEntity> _webhookCallRepository;

    public GetPagedWebhookCallsQuery(
        IRepository<WebhookCallEntity> webhookCallRepository,
        IMapper mapper)
        : base(mapper)
    {
        _webhookCallRepository = Guard.Against.Null(webhookCallRepository);
    }

    public async Task<IPagedResult<WebhookCallPagedResponseModel>> ExecuteAsync(GetWebhookCallsPagedRequestModel pagedRequest, CancellationToken cancellationToken)
    {
        var specification = new WebhookCallSpecification()
            .ByWebhookExternalId(pagedRequest.ExternalId);
        var count = await _webhookCallRepository.CountAsync(specification, cancellationToken);

        var items = await _webhookCallRepository.Query
            .Where(specification.Criteria)
            .Include(x => x.Webhook)
            .Paginate(pagedRequest)
            .ToListAsync(cancellationToken);

        return MapToResponseModel(pagedRequest, count, items);
    }
}
