﻿using AutoMapper;
using StarWars.Application.Pagination;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Queries;
using StarWars.Application.Webhooks.ResponseModels;
using StarWars.Application.Webhooks.Specifications;
using StarWars.Domain.Webhooks;
using StarWars.Persistance.Common.Queries;
using StarWars.Persistance.Extensions;
using System.Threading;

namespace StarWars.Persistance.Webhooks.Queries;

public class GetPagedWebhooksQuery : BasePagedQuery<WebhookEntity, WebhookResponseModel>, IGetPagedWebhooksQuery
{
    private readonly IRepository<WebhookEntity> _webhookRepository;

    public GetPagedWebhooksQuery(
        IRepository<WebhookEntity> webhookRepository,
        IMapper mapper)
        : base(mapper)
    {
        _webhookRepository = Guard.Against.Null(webhookRepository);
    }

    public async Task<IPagedResult<WebhookResponseModel>> ExecuteAsync(PagedRequestModel pagedRequest, CancellationToken cancellationToken)
    {
        var specification = new WebhookSpecification();
        var count = await _webhookRepository.CountAsync(specification, cancellationToken);
        var items = await _webhookRepository.Query
            .Paginate(pagedRequest)
            .ToListAsync(cancellationToken);

        return MapToResponseModel(pagedRequest, count, items);
    }
}
