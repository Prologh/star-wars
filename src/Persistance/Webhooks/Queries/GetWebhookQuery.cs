﻿using AutoMapper;
using CSharpFunctionalExtensions;
using StarWars.Application.Repositories;
using StarWars.Application.Webhooks.Queries;
using StarWars.Application.Webhooks.ResponseModels;
using StarWars.Domain.Webhooks;
using System.Threading;

namespace StarWars.Persistance.Webhooks.Queries;

public class GetWebhookQuery : IGetWebhookQuery
{
    private readonly IRepository<WebhookEntity> _webhookRepository;
    private readonly IMapper _mapper;

    public GetWebhookQuery(
        IRepository<WebhookEntity> webhookEntity,
        IMapper mapper)
    {
        _webhookRepository = webhookEntity;
        _mapper = mapper;
    }

    public async Task<Maybe<WebhookResponseModel>> ExecuteAsync(Guid externalId, CancellationToken cancellationToken)
    {
        var entity = await _webhookRepository.Query
            .FirstOrDefaultAsync(c => c.ExternalId == externalId, cancellationToken);

        return _mapper.Map<WebhookResponseModel>(entity);
    }
}
