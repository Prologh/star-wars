﻿using StarWars.Application.Repositories;
using StarWars.Application.Specifications;
using StarWars.Domain.Abstractions;
using System.Threading;

namespace StarWars.Persistance.Repositories;

public class Repository<TEntity> : IRepository<TEntity>
    where TEntity : class, IEntity
{
    private readonly ISpecificationEvaluator<TEntity> _specificationEvaluator;

    public Repository(StarWarsDbContext context, ISpecificationEvaluator<TEntity> specificationEvaluator)
    {
        DbSet = context.Set<TEntity>();
        _specificationEvaluator = specificationEvaluator;
    }

    public IQueryable<TEntity> Query => DbSet.AsQueryable();

    protected DbSet<TEntity> DbSet { get; }

    public Task<bool> AnyAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken)
    {
        Guard.Against.Null(specification);

        var query = _specificationEvaluator.Evaluate(DbSet, specification);

        return query.AnyAsync(cancellationToken);
    }

    public Task<int> CountAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken)
    {
        Guard.Against.Null(specification);

        var query = _specificationEvaluator.Evaluate(DbSet, specification);

        return query.CountAsync(cancellationToken);
    }

    public Task<TEntity> FirstAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken)
    {
        Guard.Against.Null(specification);

        var query = _specificationEvaluator.Evaluate(DbSet, specification);

        return query.FirstAsync(cancellationToken);
    }

    public Task<TEntity> FirstOrDefaultAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken)
    {
        Guard.Against.Null(specification);

        var query = _specificationEvaluator.Evaluate(DbSet, specification);

        return query.FirstOrDefaultAsync(cancellationToken);
    }

    public Task<List<TEntity>> ToListAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken)
    {
        Guard.Against.Null(specification);

        var query = _specificationEvaluator.Evaluate(DbSet, specification);

        return query.ToListAsync(cancellationToken);
    }

    public Task<TEntity> SingleOrDefaultAsync(ISpecification<TEntity> specification, CancellationToken cancellationToken)
    {
        Guard.Against.Null(specification);

        var query = _specificationEvaluator.Evaluate(DbSet, specification);

        return query.SingleOrDefaultAsync(cancellationToken);
    }
}
