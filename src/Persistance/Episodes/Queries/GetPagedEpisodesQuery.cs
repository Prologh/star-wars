﻿using AutoMapper;
using StarWars.Application.Episodes.Queries;
using StarWars.Application.Episodes.ResponseModels;
using StarWars.Application.Episodes.Specifications;
using StarWars.Application.Pagination;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Repositories;
using StarWars.Domain.Episodes;
using StarWars.Persistance.Common.Queries;
using StarWars.Persistance.Extensions;
using System.Threading;

namespace StarWars.Persistance.Episodes.Queries;

public class GetPagedEpisodesQuery : BasePagedQuery<EpisodeEntity, EpisodeResponseModel>, IGetPagedEpisodesQuery
{
    private readonly IRepository<EpisodeEntity> _episodeRepository;

    public GetPagedEpisodesQuery(
        IRepository<EpisodeEntity> episodeRepository,
        IMapper mapper)
        : base(mapper)
    {
        _episodeRepository = Guard.Against.Null(episodeRepository);
    }

    public async Task<IPagedResult<EpisodeResponseModel>> ExecuteAsync(PagedRequestModel pagedRequest, CancellationToken cancellationToken)
    {
        var specification = new EpisodeSpecification();
        var count = await _episodeRepository.CountAsync(specification, cancellationToken);

        var items = await _episodeRepository.Query
            .Paginate(pagedRequest)
            .ToListAsync(cancellationToken);

        return MapToResponseModel(pagedRequest, count, items);
    }
}
