﻿using AutoMapper;
using CSharpFunctionalExtensions;
using StarWars.Application.Episodes.Queries;
using StarWars.Application.Episodes.ResponseModels;
using StarWars.Application.Repositories;
using StarWars.Domain.Episodes;
using System.Threading;

namespace StarWars.Persistance.Episodes.Queries;

public class GetEpisodeQuery : IGetEpisodeQuery
{
    private readonly IRepository<EpisodeEntity> _episodeRopository;
    private readonly IMapper _mapper;

    public GetEpisodeQuery(
        IRepository<EpisodeEntity> episodeRepository,
        IMapper mapper)
    {
        _episodeRopository = episodeRepository;
        _mapper = mapper;
    }

    public async Task<Maybe<EpisodeResponseModel>> ExecuteAsync(int partNumber, CancellationToken cancellationToken)
    {
        var entity = await _episodeRopository.Query
            .Include(c => c.CharacterEpisodes)
            .ThenInclude(ce => ce.Character)
            .FirstOrDefaultAsync(c => c.PartNumber == partNumber, cancellationToken);

        return _mapper.Map<EpisodeResponseModel>(entity);
    }
}
