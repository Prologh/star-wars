﻿using AutoMapper;
using StarWars.Application.Characters.Specifications;
using StarWars.Application.Episodes.Queries;
using StarWars.Application.Episodes.RequestModels;
using StarWars.Application.Episodes.ResponseModels;
using StarWars.Application.Pagination;
using StarWars.Application.Repositories;
using StarWars.Domain.Characters;
using StarWars.Persistance.Common.Queries;
using StarWars.Persistance.Extensions;
using System.Threading;

namespace StarWars.Persistance.Episodes.Queries;

public class GetPagedEpisodeCharactersQuery : BasePagedQuery<CharacterEntity, EpisodeCharacterResponseModel>, IGetPagedEpisodeCharactersQuery
{
    private readonly IRepository<CharacterEpisodeEntity> _characterEpisodeRepository;

    public GetPagedEpisodeCharactersQuery(
        IRepository<CharacterEpisodeEntity> characterEpisodeRepository,
        IMapper mapper)
        : base(mapper)
    {
        _characterEpisodeRepository = Guard.Against.Null(characterEpisodeRepository);
    }

    public async Task<IPagedResult<EpisodeCharacterResponseModel>> ExecuteAsync(GetEpisodeCharactersPagedRequestModel pagedRequest, CancellationToken cancellationToken)
    {
        var specification = new CharacterEpisodeSpecification()
            .ByEpisodePartNumber(pagedRequest.PartNumber);
        var count = await _characterEpisodeRepository.CountAsync(specification, cancellationToken);

        var items = await _characterEpisodeRepository.Query
            .Where(x => x.Episode.PartNumber == pagedRequest.PartNumber)
            .Include(x => x.Character.OriginPlanet)
            .Paginate(pagedRequest)
            .Select(x => x.Character)
            .ToListAsync(cancellationToken);

        return MapToResponseModel(pagedRequest, count, items);
    }
}
