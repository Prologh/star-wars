﻿using StarWars.Domain.Abstractions;
using StarWars.Domain.Characters;
using StarWars.Domain.Episodes;
using StarWars.Domain.Planets;
using StarWars.Domain.Webhooks;
using System.Threading;

namespace StarWars.Persistance;

/// <summary>
/// Represents a database context that can be used to query and save
/// instances of entities.
/// </summary>
public class StarWarsDbContext : DbContext
{
    /// <summary>
    /// Initializes a new instance of the <see cref="StarWarsDbContext"/>
    /// class using the specified options.
    /// </summary>
    /// <param name="options">
    /// The options for this context.
    /// </param>
    public StarWarsDbContext(DbContextOptions<StarWarsDbContext> options) : base(options)
    {

    }

    public DbSet<CharacterEntity> Characters { get; private set; }

    public DbSet<CharacterEpisodeEntity> CharacterEpisodes { get; private set; }

    public DbSet<EpisodeEntity> Episodes { get; private set; }

    public DbSet<PlanetEntity> Planets { get; private set; }

    public DbSet<WebhookEntity> Webhooks { get; private set; }

    public DbSet<WebhookCallEntity> WebhookCalls { get; private set; }

    public override int SaveChanges() => SaveChanges(acceptAllChangesOnSuccess: true);

    public override int SaveChanges(bool acceptAllChangesOnSuccess)
    {
        UpdateBaseEntities();

        return base.SaveChanges(acceptAllChangesOnSuccess);
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        => SaveChangesAsync(acceptAllChangesOnSuccess: true, cancellationToken);

    public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
    {
        UpdateBaseEntities();

        return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfigurationsFromAssembly(typeof(StarWarsDbContext).Assembly);
    }

    private void UpdateBaseEntities()
    {
        var modifiedEntries = ChangeTracker
            .Entries()
            .Where(entry => entry.State == EntityState.Added
                || entry.State == EntityState.Modified);

        foreach (var entry in modifiedEntries)
        {
            if (entry.Entity is IAuditableEntity auditableEntity)
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        auditableEntity.SetCreatedToUtcNow();
                        break;

                    case EntityState.Modified:
                        auditableEntity.SetUpdatedToUtcNow();
                        break;
                }
            }
        }
    }
}
