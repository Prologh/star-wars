﻿using StarWars.Application.Repositories;
using StarWars.Persistance.Repositories;

namespace StarWars.Persistance.Extensions;

public static class RepositoryServiceCollectionExtensions
{
    public static IServiceCollection AddRepositories(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

        return services;
    }
}
