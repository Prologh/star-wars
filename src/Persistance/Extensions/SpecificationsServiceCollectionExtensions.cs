﻿using StarWars.Application.Expressions;
using StarWars.Application.Specifications;
using StarWars.Persistance.Sorting;
using StarWars.Persistance.Specifications;

namespace StarWars.Persistance.Extensions;

public static class SpecificationsServiceCollectionExtensions
{
    public static IServiceCollection AddSpecifications(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddTransient<IExpressionUtility, ExpressionUtility>();
        services.AddScoped(typeof(ISpecificationEvaluator<>), typeof(SpecificationEvaluator<>));
        services.AddTransient<ISortingExpressionProvider, SortingExpressionProvider>();

        return services;
    }
}
