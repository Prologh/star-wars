﻿using StarWars.Application.Pagination.RequestModels;

namespace StarWars.Persistance.Extensions;

public static class QueryableExtensions
{
    public static IQueryable<T> Paginate<T>(this IQueryable<T> source, PagedRequestModel requestModel)
    {
        Guard.Against.Null(source);
        Guard.Against.Null(requestModel);

        var skip = (requestModel.PageNumber - 1) * requestModel.PageSize;

        return source
            .Skip(skip)
            .Take(requestModel.PageSize);
    }
}
