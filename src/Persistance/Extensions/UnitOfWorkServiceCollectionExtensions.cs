﻿using StarWars.Application;

namespace StarWars.Persistance.Extensions;

public static class UnitOfWorkServiceCollectionExtensions
{
    public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddScoped<IUnitOfWork, UnitOfWork>();

        return services;
    }
}
