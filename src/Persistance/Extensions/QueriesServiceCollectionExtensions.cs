﻿using StarWars.Application.Common.Queries;

namespace StarWars.Persistance.Extensions;

public static class QueriesServiceCollectionExtensions
{
    public static IServiceCollection AddQueries(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.Scan(selector => selector
            .FromAssemblyOf<UnitOfWork>()
            .AddClasses(filter => filter.AssignableTo(typeof(IQuery<,>)))
            .AsImplementedInterfaces()
            .WithScopedLifetime());

        return services;
    }
}
