﻿using StarWars.Persistance.Seeding;

namespace StarWars.Persistance.Extensions;

public static class DatabaseInitializerServiceCollectionExtensions
{
    public static IServiceCollection AddDatabaseInitializer(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddScoped<DatabaseInitializer>();
        services.AddScoped<SeedDataReader>();

        return services;
    }
}
