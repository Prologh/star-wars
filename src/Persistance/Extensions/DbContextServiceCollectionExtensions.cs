﻿namespace StarWars.Persistance.Extensions;

public static class DbContextServiceCollectionExtensions
{
    public static IServiceCollection RemoveDbContextOptions<TDbContext>(this IServiceCollection services)
        where TDbContext : DbContext
    {
        Guard.Against.Null(services);

        var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<TDbContext>));
        if (descriptor != null)
        {
            services.Remove(descriptor);
        }

        return services;
    }
}
