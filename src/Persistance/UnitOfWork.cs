﻿using StarWars.Application;
using StarWars.Domain.Abstractions;
using System.Threading;

namespace StarWars.Persistance;

public class UnitOfWork : IUnitOfWork
{
    private readonly StarWarsDbContext _dbContext;

    public UnitOfWork(StarWarsDbContext starWarsDbContext)
    {
        _dbContext = Guard.Against.Null(starWarsDbContext);
    }

    public TEntity Add<TEntity>(TEntity entity)
        where TEntity : class, IEntity
    {
        Guard.Against.Null(entity);

        return _dbContext.Add(entity).Entity;
    }

    public IList<TEntity> AddRange<TEntity>(IList<TEntity> entities)
        where TEntity : class, IEntity
    {
        Guard.Against.Null(entities);

        _dbContext.AddRange(entities);

        return entities;
    }

    public TEntity Update<TEntity>(TEntity entity)
        where TEntity : class, IEntity
    {
        Guard.Against.Null(entity);

        return _dbContext.Update(entity).Entity;
    }

    public IList<TEntity> UpdateRange<TEntity>(IList<TEntity> entities)
        where TEntity : class, IEntity
    {
        Guard.Against.Null(entities);

        _dbContext.UpdateRange(entities);

        return entities;
    }

    public TEntity Remove<TEntity>(TEntity entity)
        where TEntity : class, IEntity
    {
        Guard.Against.Null(entity);

        return _dbContext.Remove(entity).Entity;
    }

    public IList<TEntity> RemoveRange<TEntity>(IList<TEntity> entities)
        where TEntity : class, IEntity
    {
        Guard.Against.Null(entities);

        _dbContext.RemoveRange(entities);

        return entities;
    }

    public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
    {
        return _dbContext.SaveChangesAsync(cancellationToken);
    }
}
