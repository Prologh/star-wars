﻿using AutoMapper;
using CSharpFunctionalExtensions;
using StarWars.Application.Planets.Queries;
using StarWars.Application.Planets.ResponseModels;
using StarWars.Application.Repositories;
using StarWars.Domain.Planets;
using System.Threading;

namespace StarWars.Persistance.Planets.Queries;

public class GetPlanetQuery : IGetPlanetQuery
{
    private readonly IRepository<PlanetEntity> _planetRepository;
    private readonly IMapper _mapper;

    public GetPlanetQuery(
        IRepository<PlanetEntity> planetRepository,
        IMapper mapper)
    {
        _planetRepository = Guard.Against.Null(planetRepository);
        _mapper = Guard.Against.Null(mapper);
    }

    public async Task<Maybe<PlanetResponseModel>> ExecuteAsync(string name, CancellationToken cancellationToken)
    {
        var entity = await _planetRepository.Query
            .Include(c => c.Characters)
            .SingleOrDefaultAsync(c => c.Name == name, cancellationToken);

        return _mapper.Map<PlanetResponseModel>(entity);
    }
}
