﻿using AutoMapper;
using StarWars.Application.Characters.Specifications;
using StarWars.Application.Pagination;
using StarWars.Application.Planets.Queries;
using StarWars.Application.Planets.RequestModels;
using StarWars.Application.Planets.ResponseModels;
using StarWars.Application.Repositories;
using StarWars.Domain.Characters;
using StarWars.Persistance.Common.Queries;
using StarWars.Persistance.Extensions;
using System.Threading;

namespace StarWars.Persistance.Planets.Queries;

public class GetPagedPlanetCharactersQuery : BasePagedQuery<CharacterEntity, PlanetCharacterResponseModel>, IGetPagedPlanetCharactersQuery
{
    private readonly IRepository<CharacterEntity> _characterRepository;

    public GetPagedPlanetCharactersQuery(
        IRepository<CharacterEntity> characterRepository,
        IMapper mapper)
        : base(mapper)
    {
        _characterRepository = Guard.Against.Null(characterRepository);
    }

    public async Task<IPagedResult<PlanetCharacterResponseModel>> ExecuteAsync(GetPlanetCharactersPagedRequestModel pagedRequest, CancellationToken cancellationToken)
    {
        var specification = new CharacterSpecification()
            .ByPlanetName(pagedRequest.PlanetName);
        var count = await _characterRepository.CountAsync(specification, cancellationToken);

        var items = await _characterRepository.Query
            .Where(x => x.OriginPlanet.Name == pagedRequest.PlanetName)
            .Paginate(pagedRequest)
            .ToListAsync(cancellationToken);

        return MapToResponseModel(pagedRequest, count, items);
    }
}
