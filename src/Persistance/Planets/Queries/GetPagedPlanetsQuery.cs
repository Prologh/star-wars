﻿using AutoMapper;
using StarWars.Application.Pagination;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Planets.Queries;
using StarWars.Application.Planets.ResponseModels;
using StarWars.Application.Planets.Specifications;
using StarWars.Application.Repositories;
using StarWars.Domain.Planets;
using StarWars.Persistance.Common.Queries;
using StarWars.Persistance.Extensions;
using System.Threading;

namespace StarWars.Persistance.Planets.Queries;

public class GetPagedPlanetsQuery : BasePagedQuery<PlanetEntity, PlanetResponseModel>, IGetPagedPlanetsQuery
{
    private readonly IRepository<PlanetEntity> _characterRepository;

    public GetPagedPlanetsQuery(
        IRepository<PlanetEntity> characterRepository,
        IMapper mapper)
        : base(mapper)
    {
        _characterRepository = Guard.Against.Null(characterRepository);
    }

    public async Task<IPagedResult<PlanetResponseModel>> ExecuteAsync(PagedRequestModel pagedRequest, CancellationToken cancellationToken)
    {
        var specification = new PlanetSpecification();
        var count = await _characterRepository.CountAsync(specification, cancellationToken);

        var items = await _characterRepository.Query
            .Include(c => c.Characters)
            .Paginate(pagedRequest)
            .ToListAsync(cancellationToken);

        return MapToResponseModel(pagedRequest, count, items);
    }
}
