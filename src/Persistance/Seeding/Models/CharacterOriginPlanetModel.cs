﻿namespace StarWars.Persistance.Seeding.Models;

public record CharacterOriginPlanetModel
{
    public string CharacterName { get; set; }

    public string OriginPlanetName { get; set; }
}
