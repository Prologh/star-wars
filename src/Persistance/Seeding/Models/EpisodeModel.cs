﻿namespace StarWars.Persistance.Seeding.Models;

public record EpisodeModel
{
    public int PartNumber { get; set; }

    public string Name { get; set; }
}
