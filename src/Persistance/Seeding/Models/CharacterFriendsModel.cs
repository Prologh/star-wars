﻿namespace StarWars.Persistance.Seeding.Models;

public record CharacterFriendsModel
{
    public CharacterFriendsModel()
    {
        FriendsNames = Array.Empty<string>();
    }

    public string CharacterName { get; set; }

    public string[] FriendsNames { get; set; }
}
