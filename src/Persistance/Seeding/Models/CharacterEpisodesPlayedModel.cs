﻿namespace StarWars.Persistance.Seeding.Models;

public record CharacterEpisodesPlayedModel
{
    public CharacterEpisodesPlayedModel()
    {
        PartNumbers = Array.Empty<int>();
    }

    public string CharacterName { get; set; }

    public int[] PartNumbers { get; set; }
}
