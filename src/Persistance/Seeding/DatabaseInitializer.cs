﻿using StarWars.Domain.Characters;
using StarWars.Domain.Episodes;
using StarWars.Domain.Planets;
using StarWars.Domain.Webhooks;
using StarWars.Persistance.Seeding.Models;

namespace StarWars.Persistance.Seeding;

/// <summary>
/// Provides means of database initialization.
/// </summary>
public class DatabaseInitializer
{
    private readonly StarWarsDbContext _dbContext;
    private readonly SeedDataReader _seedDataReader;

    public DatabaseInitializer(
        SeedDataReader seedDataReader,
        StarWarsDbContext starWarsDbContext)
    {
        _dbContext = Guard.Against.Null(starWarsDbContext);
        _seedDataReader = Guard.Against.Null(seedDataReader);
    }

    /// <summary>
    /// Creates the database if it does not exists and seeds it with
    /// initial data if it is empty.
    /// </summary>
    public void Initialize(bool ensureDeleted = false, bool ensureCreated = false, bool migrate = false)
    {
        if (ensureDeleted)
        {
            _dbContext.Database.EnsureDeleted();
        }

        if (migrate)
        {
            _dbContext.Database.Migrate();
        }

        if (ensureCreated)
        {
            _dbContext.Database.EnsureCreated();
        }

        // Look for any characters.
        if (_dbContext.Characters.AsNoTracking().Any())
        {
            // Database already has records.
            return;
        }

        var planets = _seedDataReader.ReadPlanets();
        _dbContext.Planets.AddRange(planets.Values);

        var episodes = _seedDataReader.ReadEpisodes();
        _dbContext.Episodes.AddRange(episodes.Values);

        var characters = _seedDataReader.ReadCharacters();
        _dbContext.Characters.AddRange(characters.Values);

        var characterOriginPlanets = _seedDataReader.ReadCharacterOriginPlanets();
        AssignOriginPlanets(characters, planets, characterOriginPlanets);

        var characterEpisodesPlayed = _seedDataReader.ReadCharacterEpisodesPlayed();
        AssignEpisodesPlayed(characters, episodes, characterEpisodesPlayed);

        var characterFriends = _seedDataReader.ReadCharacterFriends();
        AssignCharactersFriends(characters, characterFriends);

        var webhooks = _seedDataReader.ReadWebhooks();
        _dbContext.Webhooks.AddRange(webhooks);

        var webhookCalls = _seedDataReader.ReadWebhookCalls();
        AssignWebhookCalls(webhooks, webhookCalls);

        _dbContext.SaveChanges();
    }

    private void AssignOriginPlanets(
        Dictionary<string, CharacterEntity> characters,
        Dictionary<string, PlanetEntity> planets,
        Dictionary<string, CharacterOriginPlanetModel> characterOriginPlanets)
    {
        foreach (var originPlanet in characterOriginPlanets.Values)
        {
            var character = characters[originPlanet.CharacterName];
            var planet = planets[originPlanet.OriginPlanetName];

            character.OriginPlanet = planet;
            planet.Characters.Add(character);
        }
    }

    private void AssignEpisodesPlayed(
        Dictionary<string, CharacterEntity> characters,
        Dictionary<int, EpisodeEntity> episodes,
        Dictionary<string, CharacterEpisodesPlayedModel> characterEpisodesPlayed)
    {
        foreach (var episodePlayed in characterEpisodesPlayed.Values)
        {
            var character = characters[episodePlayed.CharacterName];

            foreach (var episodeNumber in episodePlayed.PartNumbers)
            {
                var episode = episodes[episodeNumber];
                var characterEpisode = new CharacterEpisodeEntity
                {
                    Character = character,
                    Episode = episode,
                };

                character.CharacterEpisodes.Add(characterEpisode);
                episode.CharacterEpisodes.Add(characterEpisode);
            }
        }
    }

    private void AssignCharactersFriends(
        Dictionary<string, CharacterEntity> characters,
        Dictionary<string, CharacterFriendsModel> characterFriends)
    {
        foreach (var friendsModel in characterFriends.Values)
        {
            var character = characters[friendsModel.CharacterName];

            foreach (var friendName in friendsModel.FriendsNames)
            {
                var friend = characters[friendName];

                character.Friends.Add(friend);
                friend.Friends.Add(character);
            }
        }
    }

    private void AssignWebhookCalls(List<WebhookEntity> webhooks, List<WebhookCallEntity> webhookCalls)
    {
        foreach (var call in webhookCalls)
        {
            var webhook = webhooks.Single(x => x.Topic == call.Topic && x.TargetUrl == call.TargetUrl);

            webhook.Calls.Add(call);
        }
    }
}
