﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using StarWars.Domain.Characters;
using StarWars.Domain.Episodes;
using StarWars.Domain.Planets;
using StarWars.Domain.Webhooks;
using StarWars.Persistance.Seeding.Models;
using System.IO;
using System.Reflection;

namespace StarWars.Persistance.Seeding;

public class SeedDataReader
{
    public Dictionary<string, PlanetEntity> ReadPlanets()
    {
        var planetNames = ReadJsonDataFile<string[]>("Planets");

        return planetNames.Select(name => new PlanetEntity
        {
            Name = name,
        }).ToDictionary(x => x.Name, x => x);
    }

    public Dictionary<int, EpisodeEntity> ReadEpisodes()
    {
        var episodes = ReadJsonDataFile<List<EpisodeModel>>("Episodes");

        return episodes.Select(episode => new EpisodeEntity
        {
            Name = episode.Name,
            PartNumber = episode.PartNumber,
        }).ToDictionary(x => x.PartNumber, x => x);
    }

    public Dictionary<string, CharacterEntity> ReadCharacters()
    {
        var characterNames = ReadJsonDataFile<string[]>("Characters");

        return characterNames.Select(name => new CharacterEntity
        {
            Name = name,
        }).ToDictionary(x => x.Name, x => x);
    }

    public Dictionary<string, CharacterOriginPlanetModel> ReadCharacterOriginPlanets()
    {
        var planets = ReadJsonDataFile<List<CharacterOriginPlanetModel>>("CharacterOriginPlanets");

        return planets.ToDictionary(x => x.CharacterName, x => x);
    }

    public Dictionary<string, CharacterEpisodesPlayedModel> ReadCharacterEpisodesPlayed()
    {
        var planets = ReadJsonDataFile<List<CharacterEpisodesPlayedModel>>("CharacterEpisodesPlayed");

        return planets.ToDictionary(x => x.CharacterName, x => x);
    }

    public Dictionary<string, CharacterFriendsModel> ReadCharacterFriends()
    {
        var planets = ReadJsonDataFile<List<CharacterFriendsModel>>("CharacterFriends");

        return planets.ToDictionary(x => x.CharacterName, x => x);
    }

    public List<WebhookEntity> ReadWebhooks()
    {
        return ReadJsonDataFile<List<WebhookEntity>>("Webhooks");
    }

    public List<WebhookCallEntity> ReadWebhookCalls()
    {
        return ReadJsonDataFile<List<WebhookCallEntity>>("WebhookCalls");
    }

    private T ReadJsonDataFile<T>(string fileName)
    {
        var assembly = Assembly.GetExecutingAssembly().Location;
        var assemblyPath = Path.GetDirectoryName(assembly);
        var filePath = Path.Combine(assemblyPath, "Seeding", "Data", fileName + ".json");
        var json = File.ReadAllText(filePath);
        var settings = new JsonSerializerSettings();
        settings.Converters.Add(new StringEnumConverter());

        return JsonConvert.DeserializeObject<T>(json, settings);
    }
}
