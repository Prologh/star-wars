﻿using AutoMapper;
using CSharpFunctionalExtensions;
using StarWars.Application.Characters.Queries;
using StarWars.Application.Characters.ResponseModels;
using StarWars.Application.Repositories;
using StarWars.Domain.Characters;
using System.Threading;

namespace StarWars.Persistance.Characters.Queries;

public class GetCharacterQuery : IGetCharacterQuery
{
    private readonly IRepository<CharacterEntity> _characterRopository;
    private readonly IMapper _mapper;

    public GetCharacterQuery(
        IRepository<CharacterEntity> characterEntity,
        IMapper mapper)
    {
        _characterRopository = characterEntity;
        _mapper = mapper;
    }

    public async Task<Maybe<CharacterResponseModel>> ExecuteAsync(string name, CancellationToken cancellationToken)
    {
        var entity = await _characterRopository.Query
            .Include(c => c.OriginPlanet)
            .Include(c => c.Friends)
            .Include(c => c.CharacterEpisodes)
            .ThenInclude(ce => ce.Episode)
            .FirstOrDefaultAsync(c => c.Name == name, cancellationToken);

        return _mapper.Map<CharacterResponseModel>(entity);
    }
}
