﻿using AutoMapper;
using StarWars.Application.Characters.Queries;
using StarWars.Application.Characters.ResponseModels;
using StarWars.Application.Characters.Specifications;
using StarWars.Application.Pagination;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Repositories;
using StarWars.Domain.Characters;
using StarWars.Persistance.Common.Queries;
using StarWars.Persistance.Extensions;
using System.Threading;

namespace StarWars.Persistance.Characters.Queries;

public class GetPagedCharactersQuery : BasePagedQuery<CharacterEntity, CharacterResponseModel>, IGetPagedCharactersQuery
{
    private readonly IRepository<CharacterEntity> _characterRepository;

    public GetPagedCharactersQuery(
        IRepository<CharacterEntity> characterRepository,
        IMapper mapper)
        : base(mapper)
    {
        _characterRepository = Guard.Against.Null(characterRepository);
    }

    public async Task<IPagedResult<CharacterResponseModel>> ExecuteAsync(PagedRequestModel pagedRequest, CancellationToken cancellationToken)
    {
        var specification = new CharacterSpecification();
        var count = await _characterRepository.CountAsync(specification, cancellationToken);

        var items = await _characterRepository.Query
            .Include(c => c.CharacterEpisodes)
            .ThenInclude(ce => ce.Episode)
            .Include(c => c.OriginPlanet)
            .Include(c => c.Friends)
            .Paginate(pagedRequest)
            .ToListAsync(cancellationToken);

        return MapToResponseModel(pagedRequest, count, items);
    }
}
