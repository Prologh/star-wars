﻿using AutoMapper;
using StarWars.Application.Pagination.RequestModels;
using StarWars.Application.Pagination.ResponseModels;
using StarWars.Domain.Abstractions;

namespace StarWars.Persistance.Common.Queries;

public abstract class BasePagedQuery<TEntity, TResponseModel>
    where TEntity : class, IEntity
    where TResponseModel : class
{
    private readonly IMapper _mapper;

    protected BasePagedQuery(IMapper mapper)
    {
        _mapper = Guard.Against.Null(mapper);
    }

    protected PagedResponseModel<TResponseModel> MapToResponseModel(
        PagedRequestModel pagedRequest,
        int count,
        IList<TEntity> items)
    {
        Guard.Against.Null(pagedRequest);
        Guard.Against.Null(items);
        Guard.Against.Negative(count);

        var pageCount = (double)count / pagedRequest.PageSize;
        var totalPages = (int)Math.Ceiling(pageCount);
        var mappedItems = _mapper.Map<IList<TResponseModel>>(items);

        return new PagedResponseModel<TResponseModel>(
            pagedRequest.PageNumber,
            pagedRequest.PageSize,
            totalPages,
            count,
            mappedItems);
    }
}
