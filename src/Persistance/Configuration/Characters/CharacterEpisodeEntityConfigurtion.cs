﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarWars.Domain.Characters;
using StarWars.Persistance.Configuration.Abstractions;

namespace StarWars.Persistance.Configuration.Characters;

public class CharacterEpisodeEntityConfigurtion : BaseEntityConfiguration<CharacterEpisodeEntity>
{
    protected override void ConfigureEntity(EntityTypeBuilder<CharacterEpisodeEntity> builder)
    {
        builder
            .HasOne(p => p.Character)
            .WithMany(p => p.CharacterEpisodes)
            .HasForeignKey(p => p.CharacterId)
            .IsRequired();

        builder
            .HasOne(p => p.Episode)
            .WithMany(p => p.CharacterEpisodes)
            .HasForeignKey(p => p.EpisodeId)
            .IsRequired();
    }
}
