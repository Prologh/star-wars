﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarWars.Domain;
using StarWars.Domain.Characters;
using StarWars.Persistance.Configuration.Abstractions;

namespace StarWars.Persistance.Configuration.Characters;

public class CharacterEntityConfiguration : BaseEntityConfiguration<CharacterEntity>
{
    protected override void ConfigureEntity(EntityTypeBuilder<CharacterEntity> builder)
    {
        builder.HasAlternateKey(p => p.Name);

        builder
            .Property(p => p.Name)
            .HasMaxLength(Constants.Validation.MaxCharacterNameLength)
            .IsRequired();

        builder
            .HasMany(p => p.Friends)
            .WithOne(p => p.Friend)
            .HasForeignKey(p => p.FriendId)
            .IsRequired(required: false)
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasOne(p => p.OriginPlanet)
            .WithMany(p => p.Characters)
            .HasForeignKey(p => p.OriginPlanetId)
            .IsRequired(required: false);
    }
}
