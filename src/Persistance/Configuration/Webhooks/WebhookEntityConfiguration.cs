﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarWars.Domain;
using StarWars.Domain.Webhooks;
using StarWars.Persistance.Configuration.Abstractions;

namespace StarWars.Persistance.Configuration.Webhooks;

public class WebhookEntityConfiguration : BaseEntityConfiguration<WebhookEntity>
{
    protected override void ConfigureEntity(EntityTypeBuilder<WebhookEntity> builder)
    {
        builder
            .HasMany(x => x.Calls)
            .WithOne(x => x.Webhook)
            .HasForeignKey(x => x.WebhookId);

        builder
            .Property(x => x.TargetUrl)
            .IsRequired()
            .HasMaxLength(Constants.Validation.MaxWebhookTargetUrlLength);

        builder
            .Property(x => x.Topic)
            .IsRequired()
            .HasConversion<string>();

        builder.HasIndex(x => x.TargetUrl);
        builder.HasIndex(x => x.Topic);

        builder
            .HasIndex(x => new { x.Topic, x.TargetUrl })
            .IsUnique();
    }
}
