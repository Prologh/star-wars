﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarWars.Domain;
using StarWars.Domain.Webhooks;
using StarWars.Persistance.Configuration.Abstractions;

namespace StarWars.Persistance.Configuration.Webhooks;

public class WebhookCallEntityConfiguration : BaseEntityConfiguration<WebhookCallEntity>
{
    protected override void ConfigureEntity(EntityTypeBuilder<WebhookCallEntity> builder)
    {
        builder
            .Property(x => x.TargetUrl)
            .IsRequired()
            .HasMaxLength(Constants.Validation.MaxWebhookTargetUrlLength);

        builder
            .Property(x => x.Topic)
            .IsRequired()
            .HasConversion<string>();
    }
}
