﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarWars.Domain;
using StarWars.Domain.Episodes;
using StarWars.Persistance.Configuration.Abstractions;

namespace StarWars.Persistance.Configuration.Episodes;

public class EpisodeEntityConfiguration : BaseEntityConfiguration<EpisodeEntity>
{
    protected override void ConfigureEntity(EntityTypeBuilder<EpisodeEntity> builder)
    {
        builder
            .Property(p => p.Name)
            .HasMaxLength(Constants.Validation.MaxEpisodeNameLength)
            .IsRequired();

        builder
            .HasIndex(p => p.Name)
            .IsUnique();

        builder
            .HasIndex(x => x.PartNumber)
            .IsUnique();
    }
}
