﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarWars.Domain;
using StarWars.Domain.Planets;
using StarWars.Persistance.Configuration.Abstractions;

namespace StarWars.Persistance.Configuration.Planets;

public class PlanetEntityConfiguration : BaseEntityConfiguration<PlanetEntity>
{
    protected override void ConfigureEntity(EntityTypeBuilder<PlanetEntity> builder)
    {
        builder
            .Property(p => p.Name)
            .HasMaxLength(Constants.Validation.MaxPlanetNameLength)
            .IsRequired();

        builder
            .HasIndex(x => x.Name)
            .IsUnique();
    }
}
