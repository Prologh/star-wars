﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StarWars.Domain.Abstractions;

namespace StarWars.Persistance.Configuration.Abstractions;

public abstract class BaseEntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity>
    where TEntity : class, IEntity
{
    public void Configure(EntityTypeBuilder<TEntity> builder)
    {
        ConfigureEntity(builder);

        builder.HasKey(p => p.Id);

        builder.HasAlternateKey(p => p.ExternalId);

        builder.ToTable(typeof(TEntity).Name.Replace("Entity", string.Empty));
    }

    protected abstract void ConfigureEntity(EntityTypeBuilder<TEntity> builder);
}
