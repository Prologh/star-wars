﻿using Ardalis.GuardClauses;
using StarWars.Application.Scheduling;
using StarWars.Application.Webhooks.Services;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace StarWars.Infrastructure.Webhooks.Services;

public class BackgroundJobWebhookDispatcher : IBackgroundJobWebhookDispatcher
{
    private readonly IJobScheduler _jobScheduler;

    public BackgroundJobWebhookDispatcher(IJobScheduler jobScheduler)
    {
        _jobScheduler = Guard.Against.Null(jobScheduler);
    }

    public string Schedule(Expression<Func<IWebhookDispatcher, Task>> expression)
    {
        Guard.Against.Null(expression);

        return _jobScheduler.Schedule(expression);
    }
}
