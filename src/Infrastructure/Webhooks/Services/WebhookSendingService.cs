﻿using Ardalis.GuardClauses;
using Flurl.Http;
using Flurl.Http.Content;
using Microsoft.Extensions.Logging;
using StarWars.Application;
using StarWars.Application.Webhooks.Payloads;
using StarWars.Application.Webhooks.Services;
using StarWars.Domain.Webhooks;
using StarWars.Infrastructure.Cryptography;
using StarWars.Infrastructure.Extensions.Flurl;
using StarWars.Infrastructure.Json;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace StarWars.Infrastructure.Webhooks.Services;

public class WebhookSendingService : IWebhookSendingService
{
    public const string TimestampHeaderName = "X-Webhook-Timestamp";
    public const string PayloadSignatureHeaderName = "X-HMAC-SHA256-Signature";

    private readonly IWebhookApiClient _webhookApiClient;
    private readonly IJsonSerializer _jsonSerializer;
    private readonly ISignatureProvider _signatureProvider;
    private readonly ILogger<WebhookSendingService> _logger;
    private readonly IUnitOfWork _unitOfWork;

    public WebhookSendingService(
        IWebhookApiClient webhookApiClient,
        IJsonSerializer jsonSerializer,
        IUnitOfWork unitOfWork,
        ISignatureProvider signatureProvider,
        ILogger<WebhookSendingService> logger)
    {
        _webhookApiClient = webhookApiClient;
        _jsonSerializer = jsonSerializer;
        _unitOfWork = unitOfWork;
        _signatureProvider = signatureProvider;
        _logger = logger;
    }

    public async Task SendWebhook<T>(WebhookEntity webhook, T payload, CancellationToken cancellationToken)
        where T : class, IWebhookPayload
    {
        Guard.Against.Null(webhook);
        Guard.Against.Null(payload);

        var json = _jsonSerializer.Serialize(payload);
        var content = new CapturedJsonContent(json);
        var dateSent = DateTimeOffset.UtcNow;
        var timestamp = dateSent.ToUnixTimeSeconds();
        var signatureInput = $"{timestamp}.{json}";
        var signature = _signatureProvider.GetSignature(signatureInput);

        var request = new FlurlRequest(webhook.TargetUrl)
            .WithMethod(HttpMethod.Post)
            .WithContent(content)
            .WithTimeout(TimeSpan.FromSeconds(5))
            .WithHeader(TimestampHeaderName, timestamp)
            .WithHeader(PayloadSignatureHeaderName, signature)
            .AllowAnyHttpStatus();

        _logger.LogInformation("Sending {topic} webhook to {targetUrl}.", webhook.Topic, webhook.TargetUrl);

        var stopwatch = Stopwatch.StartNew();

        var httpResponse = await _webhookApiClient.SendAsync(request, cancellationToken);

        stopwatch.Stop();

        var webhookCall = new WebhookCallEntity
        {
            SentOn = dateSent.UtcDateTime,
            DurationInMiliseconds = (long)stopwatch.Elapsed.TotalMilliseconds,
            HasTimedOut = httpResponse.HasTimedOut,
            ResponseStatusCode = httpResponse.ResponseStatusCode,
            TargetUrl = webhook.TargetUrl,
            Topic = webhook.Topic,
            Webhook = webhook,
            WebhookId = webhook.Id,
        };
        webhookCall.HasFailedResponse = httpResponse.HasFailedResponse;
        var responseContent = httpResponse.Response?.ResponseMessage?.Content;
        webhookCall.ResponseLength = httpResponse.Response?.ResponseMessage.Content.Headers.ContentLength;
        webhookCall.Response = await ReadResponseContentAsync(responseContent, webhookCall.ResponseLength, cancellationToken);

        _unitOfWork.Add(webhookCall);

        await _unitOfWork.SaveChangesAsync(cancellationToken);
    }

    private async Task<string> ReadResponseContentAsync(HttpContent responseContent, long? responseLength, CancellationToken cancellationToken)
    {
        if (responseContent is null)
        {
            return null;
        }

        if (responseLength.Value >= 2_000_000)
        {
            _logger.LogError("Response content is too large, omitting saving.");

            return null;
        }

        return await responseContent.ReadAsStringAsync(cancellationToken);
    }
}
