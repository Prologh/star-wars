﻿using Ardalis.GuardClauses;
using Flurl.Http;
using StarWars.Infrastructure.Http;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace StarWars.Infrastructure.Webhooks.Services;

public class WebhookApiClient : IWebhookApiClient
{
    private readonly IHttpClientFactory _httpClientFactory;

    public WebhookApiClient(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = Guard.Against.Null(httpClientFactory);
    }

    public async Task<HttpResponse> SendAsync(IFlurlRequest request, CancellationToken cancellationToken)
    {
        Guard.Against.Null(request);

        var client = _httpClientFactory.CreateClient(request.Url.Root);
        var flurlClient = new FlurlClient(client);

        try
        {
            var response = await flurlClient.SendAsync(request, completionOption: default, cancellationToken);

            return new HttpResponse(response);
        }
        catch (FlurlHttpTimeoutException exception)
        {
            return new HttpResponse(exception);
        }
        catch (FlurlHttpException exception)
        {
            return new HttpResponse(exception);
        }
    }
}
