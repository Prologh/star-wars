﻿using Flurl.Http;
using StarWars.Infrastructure.Http;
using System.Threading;
using System.Threading.Tasks;

namespace StarWars.Infrastructure.Webhooks.Services;

public interface IWebhookApiClient
{
    Task<HttpResponse> SendAsync(IFlurlRequest request, CancellationToken cancellationToken);
}
