﻿using Ardalis.GuardClauses;
using MediatR;
using StarWars.Application.Characters.RequestModels;
using StarWars.Application.Webhooks.Payloads;
using StarWars.Application.Webhooks.Services;
using StarWars.Domain.Characters;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StarWars.Infrastructure.Webhooks.Services;

public class WebhookDispatcher : IWebhookDispatcher
{
    private readonly IMediator _mediator;

    public WebhookDispatcher(IMediator mediator)
    {
        _mediator = mediator;
    }

    public Task DispatchCharacterCreated(CharacterEntity character, CancellationToken cancellationToken)
    {
        Guard.Against.Null(character);

        var webhookModel = new CharacterCreatedWebhookRequestModel
        {
            Episodes = character.CharacterEpisodes.Select(x => x.Episode.Name).ToList(),
            Friends = character.Friends.Select(x => x.Name).ToList(),
            Name = character.Name,
            Planet = character.OriginPlanet?.Name,
        };

        return Dispatch(webhookModel, cancellationToken);
    }

    public Task DispatchCharacterUpdated(CharacterEntity character, CancellationToken cancellationToken)
    {
        Guard.Against.Null(character);

        var webhookModel = new CharacterUpdatedWebhookRequestModel
        {
            Episodes = character.CharacterEpisodes.Select(x => x.Episode.Name).ToList(),
            Friends = character.Friends.Select(x => x.Name).ToList(),
            Name = character.Name,
            Planet = character.OriginPlanet?.Name,
        };

        return Dispatch(webhookModel, cancellationToken);
    }

    private Task Dispatch<T>(T webhookPayload, CancellationToken cancellationToken)
        where T : class, IBaseRequest, IWebhookPayload
    {
        return _mediator.Send(webhookPayload, cancellationToken);
    }
}
