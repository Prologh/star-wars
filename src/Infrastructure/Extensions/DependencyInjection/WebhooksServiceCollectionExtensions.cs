﻿using Ardalis.GuardClauses;
using Microsoft.Extensions.DependencyInjection;
using StarWars.Application.Webhooks.Services;
using StarWars.Infrastructure.Cryptography;
using StarWars.Infrastructure.Webhooks.Services;

namespace StarWars.Infrastructure.Extensions.DependencyInjection;

public static class WebhooksServiceCollectionExtensions
{
    public static IServiceCollection AddWebhookServices(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddTransient<ISignatureProvider, HMACSHA256SignatureProvider>();
        services.AddScoped<IWebhookDispatcher, WebhookDispatcher>();
        services.AddScoped<IBackgroundJobWebhookDispatcher, BackgroundJobWebhookDispatcher>();
        services.AddScoped<IWebhookSendingService, WebhookSendingService>();
        services.AddScoped<IWebhookApiClient, WebhookApiClient>();

        return services;
    }
}
