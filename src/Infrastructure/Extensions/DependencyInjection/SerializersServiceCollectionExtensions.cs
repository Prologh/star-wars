﻿using Ardalis.GuardClauses;
using Microsoft.Extensions.DependencyInjection;
using StarWars.Infrastructure.Json;

namespace StarWars.Infrastructure.Extensions.DependencyInjection;

public static class SerializersServiceCollectionExtensions
{
    public static IServiceCollection AddSerializers(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddScoped<IJsonSerializer, NewtonsoftJsonSerializer>();

        return services;
    }
}
