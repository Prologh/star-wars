﻿using Ardalis.GuardClauses;
using Microsoft.Extensions.DependencyInjection;
using StarWars.Application.Scheduling;
using StarWars.Infrastructure.Scheduling;

namespace StarWars.Infrastructure.Extensions.DependencyInjection;

public static class HangfireServiceCollectionExtensions
{
    public static IServiceCollection AddHangfireJobScheduler(this IServiceCollection services)
    {
        Guard.Against.Null(services);

        services.AddScoped<IJobScheduler, HangfireJobScheduler>();

        return services;
    }
}
