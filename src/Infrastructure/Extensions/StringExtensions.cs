﻿namespace StarWars.Infrastructure.Extensions;

public static class StringExtensions
{
    public static string FirstToLower(this string source)
    {
        if (string.IsNullOrWhiteSpace(source))
        {
            return source;
        }

        return char.ToLower(source[0]) + source[1..];
    }
}
