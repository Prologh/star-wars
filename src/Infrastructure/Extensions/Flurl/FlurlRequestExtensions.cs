﻿using Ardalis.GuardClauses;
using Flurl.Http;
using System.Net.Http;

namespace StarWars.Infrastructure.Extensions.Flurl;

public static class FlurlRequestExtensions
{
    public static FlurlRequest WithContent(this FlurlRequest request, HttpContent httpContent)
    {
        Guard.Against.Null(request);
        Guard.Against.Null(httpContent);

        request.Content = httpContent;

        return request;
    }

    public static FlurlRequest WithMethod(this FlurlRequest request, HttpMethod httpMethod)
    {
        Guard.Against.Null(request);
        Guard.Against.Null(httpMethod);

        request.Verb = httpMethod;

        return request;
    }
}
