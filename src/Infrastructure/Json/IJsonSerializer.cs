﻿namespace StarWars.Infrastructure.Json;

public interface IJsonSerializer
{
    string Serialize<T>(T value);
}
