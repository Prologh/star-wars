﻿using Ardalis.GuardClauses;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace StarWars.Infrastructure.Json;

public class NewtonsoftJsonSerializer : IJsonSerializer
{
    private readonly JsonSerializerSettings _settings;

    public NewtonsoftJsonSerializer()
    {
        _settings = new JsonSerializerSettings();
        _settings.Converters.Add(new StringEnumConverter());
    }

    public string Serialize<T>(T value)
    {
        Guard.Against.Null(value);

        return JsonConvert.SerializeObject(value, _settings);
    }
}
