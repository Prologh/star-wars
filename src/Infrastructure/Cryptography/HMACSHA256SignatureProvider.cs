﻿using Ardalis.GuardClauses;
using Microsoft.Extensions.Options;
using StarWars.Infrastructure.Options;
using System;
using System.Security.Cryptography;
using System.Text;

namespace StarWars.Infrastructure.Cryptography;

public class HMACSHA256SignatureProvider : ISignatureProvider
{
    private readonly IOptions<CryptographyOptions> cryptographyOptions;

    public HMACSHA256SignatureProvider(IOptions<CryptographyOptions> cryptographyOptions)
    {
        this.cryptographyOptions = cryptographyOptions;
    }

    public string GetSignature(string input)
    {
        Guard.Against.Null(input);

        var encoding = Encoding.UTF8;
        var key = cryptographyOptions.Value.SignatureHashingKey;
        var keyBytes = encoding.GetBytes(key);
        var source = encoding.GetBytes(input);
        var hashBytes = HMACSHA256.HashData(keyBytes, source);

        return BitConverter.ToString(hashBytes).Replace("-", string.Empty).ToLower();
    }
}
