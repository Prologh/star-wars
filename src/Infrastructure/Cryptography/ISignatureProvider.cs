﻿namespace StarWars.Infrastructure.Cryptography;

public interface ISignatureProvider
{
    string GetSignature(string input);
}
