﻿using Ardalis.GuardClauses;
using StarWars.Domain.Converters;
using System.Text.RegularExpressions;

namespace StarWars.Infrastructure.Converters;

public class SimplePascalCaseToKebabCaseConverter : IPascalCaseToKebabCaseConverter
{
    public string Convert(string input)
    {
        Guard.Against.NullOrWhiteSpace(input);

        return Regex.Replace(
            input,
            "(?<!^)([A-Z][a-z]|(?<=[a-z])[A-Z0-9])",
            "-$1",
            RegexOptions.Compiled)
            .Trim()
            .ToLower();
    }
}
