﻿namespace StarWars.Infrastructure.Options;

public class CryptographyOptions
{
    public string SignatureHashingKey { get; set; }
}
