﻿using Ardalis.GuardClauses;
using Flurl.Http;

namespace StarWars.Infrastructure.Http;

public class HttpResponse
{
    public HttpResponse(IFlurlResponse response)
    {
        Response = Guard.Against.Null(response);
    }

    public HttpResponse(FlurlHttpException exception)
    {
        Response = Guard.Against.Null(exception).Call.Response;
    }

    public HttpResponse(FlurlHttpTimeoutException exception) : this(exception as FlurlHttpException)
    {
        HasTimedOut = true;
    }

    public bool HasFailedResponse => HasTimedOut || (Response?.StatusCode >= 200 && Response?.StatusCode <= 299);

    public bool HasTimedOut { get; }

    public IFlurlResponse Response { get; }

    public int? ResponseStatusCode => Response?.StatusCode;
}
