﻿using Ardalis.GuardClauses;
using Hangfire;
using StarWars.Application.Scheduling;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace StarWars.Infrastructure.Scheduling;

public class HangfireJobScheduler : IJobScheduler
{
    private readonly IBackgroundJobClient _backgroundJobClient;

    public HangfireJobScheduler(IBackgroundJobClient backgroundJobClient)
    {
        _backgroundJobClient = backgroundJobClient;
    }

    public string Schedule<TService>(Expression<Func<TService, Task>> expression)
        where TService : class
    {
        Guard.Against.Null(expression);

        return _backgroundJobClient.Enqueue(expression);
    }
}
