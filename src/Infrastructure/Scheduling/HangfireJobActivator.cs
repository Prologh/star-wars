﻿using Ardalis.GuardClauses;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace StarWars.Infrastructure.Scheduling;

public class HangfireJobActivator : JobActivator
{
    private readonly IServiceProvider _serviceProvider;

    public HangfireJobActivator(IServiceProvider serviceProvider)
    {
        _serviceProvider = Guard.Against.Null(serviceProvider);
    }

    public override object ActivateJob(Type type)
    {
        return _serviceProvider.GetRequiredService(type);
    }
}
